package net.novagamestudios.tools.playback

import android.support.v4.media.RatingCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import net.novagamestudios.tools.collections.move
import net.novagamestudios.tools.flatMapLatestState
import net.novagamestudios.tools.ifNull

@ExperimentalCoroutinesApi
class PlaybackQueueServiceController<Item : QueueItem>(
        scope: CoroutineScope,
        private val controllerProvider: suspend () -> Pair<PlaybackController, QueueController<Item>>
) : PlaybackController, QueueController<Item>, CoroutineScope by scope {

    private var playbackController = MutableStateFlow<PlaybackController?>(null)
    private var queueController = MutableStateFlow<QueueController<Item>?>(null)

    private val _queueState = MutableStateFlow<QueueState<Item>?>(QueueState())

    override val playbackState: StateFlow<PlaybackState?> = playbackController
            .flatMapLatestState(scope) { it?.playbackState ?: MutableStateFlow(null) }
    override val queueState: StateFlow<QueueState<Item>?> = _queueState.asStateFlow()


    init {
        scope.launch {
            queueController
                    .flatMapLatest { it?.queueState ?: emptyFlow() }
                    .collect { _queueState.value = it }
        }
    }


    override suspend fun play() {
        requirePlayback { play() }
    }

    override suspend fun pause() {
        requirePlayback { pause() }
    }

    override suspend fun stop() {
        requirePlayback { stop() }
    }

    override suspend fun seekTo(ms: Long) {
        requirePlayback { seekTo(ms) }
    }

    override suspend fun setShuffleMode(mode: ShuffleMode) {
        requirePlayback { setShuffleMode(mode) }
    }

    override suspend fun setRepeatMode(mode: RepeatMode) {
        requirePlayback { setRepeatMode(mode) }
    }

    override suspend fun setPlaybackParams(params: PlaybackParams) {
        requirePlayback { setPlaybackParams(params) }
    }

    override suspend fun setCaptionEnabled(enabled: Boolean) {
        requirePlayback { setCaptionEnabled(enabled) }
    }




    override suspend fun skipToIndex(index: Int) {
        optionalQueue(
                { modifyQueueIndex { index } },
                { skipToIndex(index) }
        )
    }

    override suspend fun skipToNext() {
        optionalQueue(
                { modifyQueueIndex { it + 1 } },
                { skipToNext() }
        )
    }

    override suspend fun skipToPrev() {
        optionalQueue(
                { modifyQueueIndex { it - 1 } },
                { skipToPrev() }
        )
    }

    override suspend fun set(item: Item) {
        optionalQueue(
                { modifyQueueList { clear(); add(item) } },
                { set(item) }
        )
    }

    override suspend fun setAll(items: Collection<Item>) {
        optionalQueue(
                { modifyQueueList { clear(); addAll(items) } },
                { setAll(items) }
        )
    }

    override suspend fun add(item: Item) {
        optionalQueue(
                { modifyQueueList { add(item) } },
                { add(item) }
        )
    }

    override suspend fun addAll(items: Collection<Item>) {
        optionalQueue(
                { modifyQueueList { addAll(items) } },
                { addAll(items) }
        )
    }

    override suspend fun insert(index: Int, item: Item) {
        optionalQueue(
                { modifyQueueList { add(index, item) } },
                { insert(index, item) }
        )
    }

    override suspend fun insertAll(index: Int, items: Collection<Item>) {
        optionalQueue(
                { modifyQueueList { addAll(index, items) } },
                { insertAll(index, items) }
        )
    }

    override suspend fun move(fromIndex: Int, toIndex: Int) {
        optionalQueue(
                { modifyQueueList { move(fromIndex, toIndex) } },
                { move(fromIndex, toIndex) }
        )
    }

    override suspend fun remove(index: Int) {
        optionalQueue(
                { modifyQueueList { removeAt(index) } },
                { remove(index) }
        )
    }

    override suspend fun clear() {
        optionalQueue(
                { modifyQueueList { clear() } },
                { clear() }
        )
    }

    override suspend fun setRating(rating: RatingCompat) {
        requireQueue { setRating(rating) }
    }




    private suspend fun <R> requirePlayback(block: suspend PlaybackController.() -> R): R {
        return playbackController.value.ifNull {
            val (p, q) = controllerProvider()
            syncQueueController(q)
            playbackController.value = p
            queueController.value = q
            p
        }.block()
    }

    private suspend fun <R> requireQueue(block: suspend QueueController<Item>.() -> R): R {
        return queueController.value.ifNull {
            val (p, q) = controllerProvider()
            syncQueueController(q)
            playbackController.value = p
            queueController.value = q
            q
        }.block()
    }


    private suspend fun <R> optionalQueue(noController: suspend () -> R, block: suspend QueueController<Item>.() -> R): R {
        return queueController.value.let {
            if (it != null) it.block()
            else noController()
        }
    }

    private suspend fun syncQueueController(controller: QueueController<Item>) {
        _queueState.value?.let {
            controller.setAll(it.queue)
            controller.skipToIndex(it.currentIndex)
        }
    }

    private fun modifyQueueList(modifier: MutableList<Item>.() -> Unit) {
        val (queue, index) = _queueState.value ?: QueueState()
        val mutable = queue.toMutableList()
        mutable.modifier()
        _queueState.value = QueueState(mutable, index.takeIf { it in queue.indices } ?: -1)
    }
    private fun modifyQueueIndex(modifier: (Int) -> Int) {
        val (queue, index) = _queueState.value ?: QueueState()
        _queueState.value = QueueState(queue, modifier(index).takeIf { it in queue.indices } ?: -1)
    }
}