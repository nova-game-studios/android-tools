package net.novagamestudios.tools.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.ContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


//@Preview
//@Composable
//fun BorderedSelectableBoxPreview() {
//    BorderedSelectableBox(
//            selected = true,
//            color = Color.Red
//    ) {
//        Text("BorderedSelectableBox", Modifier.padding(16.dp))
//    }
//}



@Composable
fun SelectableBox(
        selected: Boolean,
        modifier: Modifier = Modifier,
        notSelectedModifier: Modifier = Modifier,
        notSelectedOverlay: @Composable BoxScope.() -> Unit = {},
        selectedModifier: Modifier = Modifier,
        selectedOverlay: @Composable BoxScope.() -> Unit = {},
        content: @Composable BoxScope.() -> Unit
) {
    Box(modifier = modifier.then(if (selected) selectedModifier else notSelectedModifier)) {
        content()
        if (selected) selectedOverlay()
        else notSelectedOverlay()
    }
}

@Composable
fun BorderedSelectableBox(
        selected: Boolean,
        modifier: Modifier = Modifier,
        borderWidth: Dp = 1.dp,
        borderColor: Color = MaterialTheme.colors.primary.copy(alpha = ContentAlpha.high),
        borderShape: Shape = MaterialTheme.shapes.small,
        content: @Composable BoxScope.() -> Unit
) = SelectableBox(
        selected = selected,
        modifier = modifier,
        selectedOverlay = { Box(Modifier.matchParentSize().border(borderWidth, borderColor, borderShape)) },
        content = content
)