package net.novagamestudios.tools.composables

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp

private val MaterialListTitlePadding = PaddingValues(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
private val MaterialIconListTitlePadding = PaddingValues(start = 72.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)


@Composable
fun MaterialListGroup(
        modifier: Modifier = Modifier,
        title: (@Composable () -> Unit)? = null,
        content: @Composable () -> Unit
) {
    Column(modifier.padding(top = 16.dp)) {
        if (title != null) MaterialListTitle { title() }
        content()
    }
}

@Composable
fun MaterialIconListGroup(
        modifier: Modifier = Modifier,
        title: (@Composable () -> Unit)? = null,
        content: @Composable () -> Unit
) {
    Column(modifier.padding(top = 16.dp)) {
        if (title != null) MaterialIconListTitle { title() }
        content()
    }
}


@Composable
fun MaterialListTitle(
        style: TextStyle = MaterialTheme.typography.overline,
        content: @Composable () -> Unit
) {
    ProvideTextStyle(style) {
        Box(Modifier.padding(MaterialListTitlePadding)) {
            content()
        }
    }
}

@Composable
fun MaterialIconListTitle(
        style: TextStyle = MaterialTheme.typography.overline,
        content: @Composable () -> Unit
) {
    ProvideTextStyle(style) {
        Box(Modifier.padding(MaterialIconListTitlePadding)) {
            content()
        }
    }
}
