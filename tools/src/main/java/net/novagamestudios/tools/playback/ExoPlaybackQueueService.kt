package net.novagamestudios.tools.playback

import android.app.Notification
import android.app.PendingIntent
import android.graphics.Bitmap
import androidx.annotation.StringRes
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import androidx.media2.session.MediaSession
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ext.media2.SessionPlayerConnector
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import net.novagamestudios.tools.Logger
import kotlin.time.Duration
import kotlin.time.ExperimentalTime


@FlowPreview
@ExperimentalTime
@ExperimentalCoroutinesApi
abstract class ExoPlaybackQueueService<Item : QueueItem> : LifecycleService(), Logger {

    protected open val tag: String get() = javaClass.simpleName
    override val loggerTag: String get() = tag


//    protected abstract val repository: PlaybackQueueRepository<Item>

    protected abstract val playerPositionUpdateInterval: Duration

    protected abstract val wakeMode: WakeMode

    private lateinit var exoPlayer: SimpleExoPlayer
    private lateinit var _playbackController: SessionPlaybackController
    private lateinit var _queueController: ExoQueueController<Item>
    lateinit var player: SessionPlayerConnector
    private lateinit var session: MediaSession
    private lateinit var notificationManager: PlayerNotificationManager

    protected abstract val notificationAdapter: NotificationAdapter<Item>


    val playbackController: PlaybackController get() = _playbackController
    val queueController: QueueController<Item> get() = _queueController

    override fun onCreate() {
        super.onCreate()


        exoPlayer = SimpleExoPlayer.Builder(this)
                .setWakeMode(wakeMode.exoConstant)
                .build()

        // TODO customize notification-actions and session behaviour
        //  (incrementMs, decrementMs, etc.)

        player = SessionPlayerConnector(exoPlayer)

        session = MediaSession.Builder(this, player)
//                .setSessionCallback(Executors.newSingleThreadExecutor(), object : MediaSession.SessionCallback() {})
                .build()

//        val sessionController = MediaController.Builder(this)
//                .setSessionToken(session.token)
//                .setControllerCallback(Executors.newSingleThreadExecutor(), object : MediaController.ControllerCallback() {})
//                .build()

        /*
        notificationManager = PlayerNotificationManager.createWithNotificationChannel(
                this,
                notificationAdapter.channelId,
                notificationAdapter.channelName,
                notificationAdapter.channelDescription,
                notificationAdapter.notificationId,
                object : PlayerNotificationManager.MediaDescriptionAdapter {
                    val item get() = _queueController.queueState.value.currentItem
                    override fun getCurrentContentTitle(player: Player): CharSequence =
                            notificationAdapter.contentTitle(item)
                    override fun createCurrentContentIntent(player: Player): PendingIntent? =
                            notificationAdapter.createContentIntent(item)
                    override fun getCurrentContentText(player: Player): CharSequence? =
                            notificationAdapter.contentText(item)
                    override fun getCurrentLargeIcon(player: Player, callback: PlayerNotificationManager.BitmapCallback): Bitmap? =
                            notificationAdapter.largeIcon(item, { callback.onBitmap(it) })
                },
                object : PlayerNotificationManager.NotificationListener {
                    override fun onNotificationPosted(notificationId: Int, notification: Notification, ongoing: Boolean) {
                        if (ongoing) startForeground(notificationId, notification)
                        else stopForeground(false)
                    }
                    override fun onNotificationCancelled(notificationId: Int, dismissedByUser: Boolean) {
                        stopForeground(true)
                        stopSelf()
//                        if (dismissedByUser) stopSelf()
                    }
                }
        ).apply {
            setMediaSessionToken(session.sessionCompatToken)
            notificationAdapter.configure(this)
            setPlayer(exoPlayer)
        }
         */


        // IN commands OUT state
        // playback of SessionPlayer somehow overrides playback of ExoPlayer
        // but queue of ExoPlayer probably works
        _playbackController = SessionPlaybackController(player, lifecycleScope, playerPositionUpdateInterval)
        _queueController = ExoQueueController(exoPlayer)

    }

    override fun onDestroy() {
        super.onDestroy()
        notificationManager.setPlayer(null)
        session.close()
        player.close()
        _queueController.close()
        _playbackController.close()
        exoPlayer.release()
    }



    protected interface NotificationAdapter<Item : QueueItem> {
        val channelId: String
        val channelName: Int @StringRes get
        val channelDescription: Int @StringRes get
        val notificationId: Int

        fun configure(manager: PlayerNotificationManager)
        fun contentTitle(item: Item?): CharSequence = "${item?.media?.mediaItem?.mediaMetadata?.title}"
        fun createContentIntent(item: Item?): PendingIntent? = null
        fun contentText(item: Item?): CharSequence? = null
        fun largeIcon(item: Item?, callback: (Bitmap) -> Unit): Bitmap? = null
    }


}