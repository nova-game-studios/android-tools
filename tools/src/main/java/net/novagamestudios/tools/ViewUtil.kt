package net.novagamestudios.tools

import android.view.View
import android.view.View.*
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope


var View.visibleElseGone: Boolean
    get() = visibility == VISIBLE
    set(value) { visibility = if (value) VISIBLE else GONE }

var View.visibleElseInvisible: Boolean
    get() = visibility == VISIBLE
    set(value) { visibility = if (value) VISIBLE else INVISIBLE }


val View.lifecycleScope: LifecycleCoroutineScope? get() = findViewTreeLifecycleOwner()?.lifecycleScope
