package net.novagamestudios.tools.composables

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.material.RadioButtonColors
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier

@Composable
fun <K> rememberRadioButtonGroup(
        initialSelected: K? = null,
        onClick: (K) -> Boolean = { true },
        onChanged: (K) -> Unit = { }
): RadioButtonGroup<K> = remember {
    RadioButtonGroup(initialSelected, onClick, onChanged)
}

class RadioButtonGroup<K>(
        initialSelected: K? = null,
        val onClick: (K) -> Boolean = { true },
        val onChanged: (K) -> Unit = { }
) {
    var selected by mutableStateOf(initialSelected)
    fun click(key: K) {
        if (onClick(key)) {
            val old = selected
            selected = key
            if (old != key) onChanged(key)
        }
    }
}

@Composable
fun <K> RadioButton(
    group: RadioButtonGroup<K>,
    key: K,
    modifier: Modifier = Modifier,
    isInitialSelected: Boolean = false,
    enabled: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colors: RadioButtonColors = RadioButtonDefaults.colors()
) {
    if (group.selected == null && isInitialSelected) group.selected = key
    androidx.compose.material.RadioButton(
            onClick = { group.click(key) },
            selected = group.selected == key,
            modifier = modifier,
            enabled = enabled,
            interactionSource = interactionSource,
            colors = colors
    )
}

@Composable
fun <K> RadioButtonGroup(
        initialSelected: K? = null,
        onClick: (K) -> Boolean = { true },
        content: @Composable RadioButtonGroupScope<K>.() -> Unit
) {
    val group = rememberRadioButtonGroup(initialSelected, onClick)
    RadioButtonGroupScope(group).content()
}

class RadioButtonGroupScope<K>(
        private val group: RadioButtonGroup<K>
) {
    @Composable
    fun AddRadioButton(
            key: K,
            modifier: Modifier = Modifier,
            isInitialSelected: Boolean = false,
            enabled: Boolean = true,
            interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
            colors: RadioButtonColors = RadioButtonDefaults.colors()
    ) = RadioButton(
            group = group,
            key = key,
            modifier = modifier,
            isInitialSelected = isInitialSelected,
            enabled = enabled,
            interactionSource = interactionSource,
            colors = colors
    )
}