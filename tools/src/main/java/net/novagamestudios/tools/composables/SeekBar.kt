package net.novagamestudios.tools.composables

import androidx.compose.animation.core.animate
import androidx.compose.animation.core.tween
import androidx.compose.foundation.interaction.DragInteraction
import androidx.compose.foundation.interaction.Interaction
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.collect

@Preview
@Composable
fun SeekBarPreview() {
    Column(modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(16.dp)
    ) {

        var animated by rememberMutableState { 0f }

        LaunchedEffect(null) {
            net.novagamestudios.tools.repeat {
                animate(0f, 1f, animationSpec = tween(2000)) { v, _ -> animated = v }
                animate(1f, 0f, animationSpec = tween(2000)) { v, _ -> animated = v }
            }
        }

        var value by rememberMutableState { 0f }

        var seekedValue by rememberMutableState { 0f }



        Text("value: $value, animated: $animated, seekedValue: $seekedValue")
        Spacer(modifier = Modifier.height(8.dp))
        SeekBar(animated,
                value,
                modifier = Modifier.fillMaxWidth(),
                onSeek = { value = it },
                onSeekFinished = { seekedValue = it }
        )

    }
}

@Composable
fun SeekBar(
        progress: Float,
        seekValue: Float,
        modifier: Modifier = Modifier,
        onSeek: (Float) -> Unit = { },
        onSeekFinished: (Float) -> Unit = { },
        enabled: Boolean = true,
        interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
        colors: SliderColors = SliderDefaults.colors()
) {
    val interactions = remember { mutableStateListOf<Interaction>() }
    LaunchedEffect(interactionSource) {
        interactionSource.interactions.collect { interaction ->
            when (interaction) {
                is PressInteraction.Press -> interactions.add(interaction)
                is PressInteraction.Release -> interactions.remove(interaction.press)
                is PressInteraction.Cancel -> interactions.remove(interaction.press)
                is DragInteraction.Start -> interactions.add(interaction)
                is DragInteraction.Stop -> interactions.remove(interaction.start)
                is DragInteraction.Cancel -> interactions.remove(interaction.start)
            }
        }
    }
    Slider(value = if (interactions.isNotEmpty()) seekValue else progress,
           modifier = modifier,
           onValueChange = onSeek,
           onValueChangeFinished = {
               if (!(interactions.any { it is DragInteraction } &&
                     interactions.any { it is PressInteraction })) onSeekFinished(seekValue)
           },
           enabled = enabled,
           interactionSource = interactionSource,
           colors = colors
    )
}

