package net.novagamestudios.tools

import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import java.io.File
import java.io.IOException

interface JFile {

    val uri: Uri

    val parent: JFile?
    val children: List<JFile>

    /** Exists */
    val exists: Boolean
    /** Exists and is a directory */
    val isDirectory: Boolean
    /** Exists and is a file */
    val isFile: Boolean

    val canRead: Boolean
    val canWrite: Boolean
    val canExecute: Boolean

    val name: String?
    val size: Long


    fun child(name: String, mimeType: String? = null): JFile

    fun createDirectory(): Boolean
    fun createFile(): Boolean

    fun rename(name: String): Boolean
    fun delete(): Boolean



    val isTypeDocumentFile: Boolean
    val isTypeFile: Boolean

}

fun Uri.toJFile(context: Context?): JFile = when {
    isFileUri() -> toFile().toJFile()
    else -> {
        if (context == null) throw NullPointerException("Context is needed to convert $this to JFile")
        val file = toDocumentFile(context) ?: throw NullPointerException("Cannot convert $this to JFile")
        file.toJFile()
    }
}

fun File.toJFile(): JFile = FileJFile(this)

fun DocumentFile.toJFile(): JFile = DocumentJFile(this)


private fun combineNameAndMimeType(name: String, mimeType: String? = null): String {
    if (mimeType != null) {
        val ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType)
        if (ext != null) return "$name.$ext"
    }
    return name
}


@Throws(IOException::class)
fun JFile.copyTo(context: Context, dest: JFile, overwrite: Boolean = false, bufferSize: Int = 1024) {
    if (!exists) throw IOException("Source file $this does not exist")
    if (dest.exists) {
        if (!overwrite) throw IOException("$dest already exists")
    } else if (!dest.createFile()) throw IOException("$dest could not be created")
    context.contentResolver.openInputStream(uri).use { inStream ->
        context.contentResolver.openOutputStream(dest.uri).use { outStream ->
            if (inStream == null) throw IOException("Could not open input stream from $this")
            if (outStream == null) throw IOException("Could not open output stream to $dest")

            val buffer = ByteArray(bufferSize)
            var read: Int
            while (inStream.read(buffer).also { read = it } != -1) outStream.write(buffer, 0, read)
            outStream.flush()
        }
    }
}

@Throws(IOException::class)
fun JFile.moveTo(context: Context, dest: JFile, overwrite: Boolean = false, bufferSize: Int = 1024) {
    copyTo(context, dest, overwrite, bufferSize)
    if (!delete()) throw IOException("Could not delete source file $this after copy")
}



val JFile.nameWithoutExtension get() = name?.splitNameAndExtension()?.first


private class FileJFile(
        private val src: File
) : JFile {

    override val uri: Uri get() = src.toUri()

    override val parent: JFile? get() = src.parentFile?.toJFile()
    override val children: List<JFile> get() = src.listFiles()?.map { it.toJFile() }.orEmpty()

    override val exists: Boolean get() = src.exists()
    override val isDirectory: Boolean get() = src.isDirectory
    override val isFile: Boolean get() = src.isFile

    override val canRead: Boolean get() = src.canRead()
    override val canWrite: Boolean get() = src.canWrite()
    override val canExecute: Boolean get() = src.canExecute()

    override val name: String? get() = src.name
    override val size: Long get() = src.length()


    override fun child(name: String, mimeType: String?): JFile =
            File(src, combineNameAndMimeType(name, mimeType)).toJFile()

    override fun createDirectory(): Boolean = src.mkdirs()
    override fun createFile(): Boolean = src.createNewFile()

    override fun rename(name: String): Boolean {
        val path = src.path
        val oldName = src.name

        if (path.substring(path.length - oldName.length) != oldName) return false

        return src.renameTo(File(path.substring(0, path.length - oldName.length) + name))
    }

    override fun delete(): Boolean = src.delete()

    override val isTypeDocumentFile: Boolean = false
    override val isTypeFile: Boolean = true

    override fun toString(): String = uri.toString()

}


private class DocumentJFile(
        val src: DocumentFile
) : JFile {

    override val uri: Uri get() = src.uri

    override val parent: JFile? get() = src.parentFile?.toJFile()
    override val children: List<JFile> get() = src.listFiles().map { it.toJFile() }

    override val exists: Boolean get() = src.exists()
    override val isDirectory: Boolean get() = exists && src.isDirectory
    override val isFile: Boolean get() = exists && src.isFile

    override val canRead: Boolean get() = src.canRead()
    override val canWrite: Boolean get() = src.canWrite()
    override val canExecute: Boolean get() = throw UnsupportedOperationException()

    override val name: String? get() = src.name
    override val size: Long get() = src.length()


    override fun child(name: String, mimeType: String?): JFile {
        val nameWithExt = combineNameAndMimeType(name, mimeType)
        children.find { it.name == nameWithExt }?.let { return it }
        return NonexistentDocumentJFile(this, name, mimeType)
    }

    override fun createDirectory(): Boolean = false
    override fun createFile(): Boolean = false

    override fun rename(name: String): Boolean = src.renameTo(name)

    override fun delete(): Boolean = src.delete()

    override val isTypeDocumentFile: Boolean = true
    override val isTypeFile: Boolean = false

    override fun toString(): String = uri.toString()
}


private class NonexistentDocumentJFile(
    private val parentWhileNotCreated: JFile,
    private var nameWhileNotCreated: String,
    private var mimeTypeWhileNotCreated: String?
) : JFile {
    private var created: JFile? = null

    private fun <R> createdOrNot(prop: JFile.() -> R, def: () -> R): R = created.let {
        if (it != null) it.prop()
        else def()
    }

    override val uri: Uri get() = createdOrNot({ uri }) {
        throw UnsupportedOperationException("The uri is unknown for a NonexistentDocumentJFile")
    }

    override val parent: JFile? get() = createdOrNot({ parent }) { parentWhileNotCreated }
    override val children: List<JFile> get() = createdOrNot({ children }) { emptyList() }

    override val exists: Boolean get() = createdOrNot({ exists }) { false }
    override val isDirectory: Boolean get() = createdOrNot({ isDirectory }) { false }
    override val isFile: Boolean get() = createdOrNot({ isFile }) { false }

    override val canRead: Boolean get() = createdOrNot({ canRead }) { false }
    override val canWrite: Boolean get() = createdOrNot({ canWrite }) { false }
    override val canExecute: Boolean get() = createdOrNot({ canExecute }) { false }

    override val name: String? get() = createdOrNot({ name }) {
        combineNameAndMimeType(nameWhileNotCreated, mimeTypeWhileNotCreated)
    }
    override val size: Long get() = createdOrNot({ size }) { 0 }

    override fun child(name: String, mimeType: String?): JFile {
        val nameWithExt = combineNameAndMimeType(name, mimeType)
        children.find { it.name == nameWithExt }?.let { return it }
        return NonexistentDocumentJFile(this, name, mimeType)
    }


    private fun creatingDocumentFile(): DocumentFile = parent.let { when (it) {
        is DocumentJFile            -> it.src
        is NonexistentDocumentJFile -> (it.created as DocumentJFile).src
        else                        -> throw UnsupportedOperationException()
    } }


    override fun createDirectory(): Boolean {
        if (exists) return false
        if (!parent!!.exists) if (!parent!!.createDirectory()) return false

        return creatingDocumentFile()
                .createDirectory(combineNameAndMimeType(nameWhileNotCreated, mimeTypeWhileNotCreated))
                ?.toJFile()
                .also { created = it } != null
    }

    override fun createFile(): Boolean {
        if (exists) return false
        if (!parent!!.exists) if (!parent!!.createDirectory()) return false

        val name: String
        val mimeType: String

        when (val mt = mimeTypeWhileNotCreated) {
            null -> {
                val (tmpName, ext) = nameWhileNotCreated.splitNameAndExtension()
                name = tmpName
                mimeType = ext?.let {
                    MimeTypeMap.getSingleton().getMimeTypeFromExtension(it)
                }.letIfNull { "*/*" }
            }
            else -> {
                name = nameWhileNotCreated
                mimeType = mt
            }
        }

        return creatingDocumentFile()
                .createFile(mimeType, name)
                ?.toJFile()
                .also { created = it } != null
    }

    override fun rename(name: String): Boolean = when (val src = created) {
        null -> {
            nameWhileNotCreated = name
            mimeTypeWhileNotCreated = null
            true
        }
        else -> src.rename(name)
    }

    override fun delete(): Boolean = when (val src = created) {
        null -> false
        else -> src.delete()
    }

    override val isTypeDocumentFile: Boolean get() = createdOrNot({ isTypeDocumentFile }) { true }
    override val isTypeFile: Boolean get() = createdOrNot({ isTypeFile }) { false }

    override fun toString(): String = try {
        uri.toString()
    } catch(e: Throwable) {
        "NonexistentDocumentJFile($nameWhileNotCreated)"
    }
}


