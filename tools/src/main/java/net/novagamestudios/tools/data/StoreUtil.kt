package net.novagamestudios.tools.data

import com.dropbox.android.external.store4.SourceOfTruth
import com.dropbox.android.external.store4.Store
import com.dropbox.android.external.store4.StoreRequest
import com.dropbox.android.external.store4.StoreResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import net.novagamestudios.tools.*
import net.novagamestudios.tools.collections.MutableFlowingMap
import kotlin.collections.MutableMap
import kotlin.collections.mutableMapOf
import kotlin.collections.set

// see: https://github.com/dropbox/Store/issues/245
//suspend inline fun <K : Any, V : Any> Store<K, V>.tryGet(key: K): V? =
//        stream(StoreRequest.cached(key, refresh = false))
//                .filterNot { it is StoreResponse.Loading || it is StoreResponse.NoNewData }
//                .first()
//                .dataOrNull()


class MapSourceOfTruthMutable<K, V : Any>(
        map: MutableMap<K, MutableStateFlow<V?>> = mutableMapOf()
) : MutableFlowingMap<K, V>(map), SourceOfTruth<K, V, V> {
    override suspend fun delete(key: K) { remove(key) }
    override suspend fun deleteAll() = clear()
    override fun reader(key: K): Flow<V?> = getFlow(key)
    override suspend fun write(key: K, value: V) { this[key] = value }
}


@Suppress("FunctionName")
fun <K, I, O> SourceOfTruth(
        write: suspend (key: K, value: I) -> Unit,
        reader: (key: K) -> Flow<O?>,
        delete: suspend (key: K) -> Unit,
        deleteAll: suspend () -> Unit
): SourceOfTruth<K, I, O> = object : SourceOfTruth<K, I, O> {
    override suspend fun write(key: K, value: I) = write(key, value)
    override fun reader(key: K): Flow<O?> = reader(key)
    override suspend fun delete(key: K) = delete(key)
    override suspend fun deleteAll() = deleteAll()
}

fun <K, I, O> SourceOfTruth<K, I, O>.debugError(
        name: String = "SourceOfTruth"
): SourceOfTruth<K, I, O> = SourceOfTruth(
        { k, i -> debugError("$name.write($k, $i)") { write(k, i) } },
        { k -> debugError("$name.reader($k)") { reader(k).flowDebugError("$name.readerFlow($k)") } },
        { k -> debugError("$name.delete($k)") { delete(k) } },
        { debugError("$name.deleteAll") { deleteAll() } }
)


operator fun <K, I, O> SourceOfTruth<K, I, O>.plus(backup: SourceOfTruth<K, I, O>): SourceOfTruth<K, I, O> = SourceOfTruth(
        { key, value ->
            this.write(key, value)
            backup.write(key, value)
        },
        { key -> this.reader(key).combine(backup.reader(key)) { a: O?, b: O? -> a ?: b } },
        { key ->
            this.delete(key)
            backup.delete(key)
        },
        {
            this.deleteAll()
            backup.deleteAll()
        }
)



@Suppress("FunctionName")
fun <T> DataState(response: StoreResponse<T>?) : DataState<T> = when(response) {
    is StoreResponse.Loading -> DataState.loading()
    is StoreResponse.Data    -> DataState.data(response.value)
    is StoreResponse.Error   -> DataState.error(DataState.Error(response))
    else                     -> DataState.empty()
}
@Suppress("FunctionName")
fun DataState.Companion.Error(response: StoreResponse.Error): DataState.Error = when (response) {
    is StoreResponse.Error.Message   -> DataState.Error.Message(response.message)
    is StoreResponse.Error.Exception -> DataState.Error.Exception(response.error)
}


@ExperimentalCoroutinesApi
class StoreController<Key : Any, Output : Any>(
        val store: Store<Key, Output>,
        val scope: CoroutineScope,
        private val requestFlow: Flow<StoreRequest<Key>?>
) : Logger {
    private val refreshEvent = MutableEventFlow<Unit>()
    fun refresh() { refreshEvent.dispatch(Unit) }
    val stateFlow: StateFlow<DataState<Output>> = flow {
        var state: DataState<Output> = DataState.empty()

        fun combine(new: DataState<Output>): DataState<Output> {
            state += new
            return state
        }

        emitAll(requestFlow
                        .onEach { info { "received request: $it" } }
                        .replayLatestOnEvent(refreshEvent) { request, _ ->
                            request?.let { StoreRequest.fresh(it.key) }
                        }
                        .flatMapLatest { request ->
                            info { "has request: ${request != null}" }
                            val flow = if (request != null)
                                store.stream(request)
                                        .flowDump("StoreResponse")
                                        .map { response -> DataState(response) }
                                        // to ensure the outputs of this controller are responsive
                                        .prepend { DataState.loading() }
                            else flowOf(DataState.empty()) // when there is no request
                            flow.map { combine(it) }
                        })
    }.stateIn(scope, SharingStarted.WhileSubscribed(), DataState.empty())
}


