package net.novagamestudios.tools.composables

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Outline
import android.view.*
import androidx.appcompat.view.ContextThemeWrapper
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.AbstractComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.semantics.dialog
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.DialogWindowProvider
import androidx.compose.ui.window.SecureFlagPolicy
//import androidx.lifecycle.ViewTreeLifecycleOwner
//import androidx.lifecycle.ViewTreeViewModelStoreOwner
//import androidx.savedstate.ViewTreeSavedStateRegistryOwner
import net.novagamestudios.tools.R
import net.novagamestudios.tools.screenDimensions


@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun FullScreenDialog(onClose: () -> Unit,
                     title: @Composable () -> Unit,
                     topAppBarColor: Color = MaterialTheme.colors.primarySurface,
                     actions: @Composable RowScope.() -> Unit = {},
                     navigationButton: @Composable (doClose: () -> Unit) -> Unit = { doClose ->
                         IconButton(onClick = doClose) { Icon(Icons.Filled.Close, null) }
                     },
                     backgroundColor: Color = MaterialTheme.colors.background,
                     content: @Composable () -> Unit
) = FullScreenDialog(
        onClose = onClose,
        topAppBar = { doClose -> TopAppBar(
                title = title,
                navigationIcon = { navigationButton(doClose) },
                actions = actions,
                backgroundColor = topAppBarColor
        ) },
        backgroundColor = backgroundColor,
        content = content
)


@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun FullScreenDialog(
        onClose: () -> Unit,
        topAppBar: (@Composable (doClose: () -> Unit) -> Unit)? = null,
        backgroundColor: Color = MaterialTheme.colors.background,
        content: @Composable () -> Unit
) {
    val doClose = remember { { onClose() } }
    FullScreenDialog(onDismissRequest = doClose) {
        Surface(color = backgroundColor, modifier = Modifier.fillMaxSize()) {
            Column(modifier = Modifier.fillMaxSize()) {
                topAppBar?.invoke(doClose)
                content()
            }
        }
    }
}

@Composable
fun FullScreenDialog(
        onDismissRequest: () -> Unit,
        properties: DialogProperties = DialogProperties(),
        content: @Composable () -> Unit
) {
    val view = LocalView.current
    val density = LocalDensity.current
    val layoutDirection = LocalLayoutDirection.current
    val composition = rememberCompositionContext()
    val currentContent by rememberUpdatedState(content)
    val dialog = remember(view, density) {
        FullScreenDialogWrapper(
                onDismissRequest,
                properties,
                view,
                layoutDirection,
                density
        ).apply {
            setContent(composition) {
                FullScreenDialogLayout(Modifier.semantics { dialog() }) {
                    currentContent()
                }
            }
        }
    }

    DisposableEffect(dialog) {
        dialog.show()

        onDispose {
            dialog.dismiss()
            dialog.disposeComposition()
        }
    }

    SideEffect {
        dialog.updateParameters(
                onDismissRequest = onDismissRequest,
                properties = properties,
                layoutDirection = layoutDirection
        )
    }
}

@SuppressLint("ViewConstructor")
private class FullScreenDialogLayout(
        context: Context,
        override val window: Window
) : AbstractComposeView(context), DialogWindowProvider {

    private var content: @Composable () -> Unit by mutableStateOf({})

    override var shouldCreateCompositionOnAttachedToWindow: Boolean = false
        private set

    fun setContent(parent: CompositionContext, content: @Composable () -> Unit) {
        setParentCompositionContext(parent)
        this.content = content
        shouldCreateCompositionOnAttachedToWindow = true
        createComposition()
    }

    @Composable
    override fun Content() {
        content()
    }
}

@Composable
private fun FullScreenDialogLayout(
        modifier: Modifier = Modifier,
        content: @Composable () -> Unit
) {
    Layout(
            content = content,
            modifier = modifier
    ) { measurables, constraints ->
        val placeables = measurables.map { it.measure(constraints) }
        val width = placeables.maxByOrNull { it.width }?.width ?: constraints.minWidth
        val height = placeables.maxByOrNull { it.height }?.height ?: constraints.minHeight
        layout(width, height) {
            placeables.forEach { it.placeRelative(0, 0) }
        }
    }
}

private class FullScreenDialogWrapper(
        private var onDismissRequest: () -> Unit,
        private var properties: DialogProperties,
        private val composeView: View,
        layoutDirection: LayoutDirection,
        density: Density
) : Dialog(
        /**
         * [Window.setClipToOutline] is only available from 22+, but the style attribute exists on 21.
         * So use a wrapped context that sets this attribute for compatibility back to 21.
         */
        ContextThemeWrapper(composeView.context, androidx.compose.ui.R.style.DialogWindowTheme)
) {
    private val dialogLayout: FullScreenDialogLayout

    private val maxSupportedElevation = 30.dp

    init {
        val window = window ?: error("Dialog has no window")
        window.requestFeature(Window.FEATURE_NO_TITLE)
        window.setBackgroundDrawableResource(android.R.color.transparent)

        dialogLayout = FullScreenDialogLayout(context, window).apply {
            // Enable children to draw their shadow by not clipping them
            clipChildren = false
            // Allocate space for elevation
            with(density) { elevation = maxSupportedElevation.toPx() }
            // Simple outline to force window manager to allocate space for shadow.
            // Note that the outline affects clickable area for the dismiss listener. In case of
            // shapes like circle the area for dismiss might be to small (rectangular outline
            // consuming clicks outside of the circle).
            outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, result: Outline) {
                    result.setRect(0, 0, view.width, view.height)
                    // We set alpha to 0 to hide the view's shadow and let the composable to draw
                    // its own shadow. This still enables us to get the extra space needed in the
                    // surface.
                    result.alpha = 0f
                }
            }
        }

        /**
         * Disables clipping for [this] and all its descendant [ViewGroup]s until we reach a
         * [FullScreenDialogLayout] (the [ViewGroup] containing the Compose hierarchy).
         */
        fun ViewGroup.disableClipping() {
            clipChildren = false
            if (this is FullScreenDialogLayout) return
            for (i in 0 until childCount) {
                (getChildAt(i) as? ViewGroup)?.disableClipping()
            }
        }

        // Turn of all clipping so shadows can be drawn outside the window
        (window.decorView as? ViewGroup)?.disableClipping()
        setContentView(dialogLayout)
//        ViewTreeLifecycleOwner.set(dialogLayout, ViewTreeLifecycleOwner.get(composeView))
//        ViewTreeViewModelStoreOwner.set(dialogLayout, ViewTreeViewModelStoreOwner.get(composeView))
//        ViewTreeSavedStateRegistryOwner.set(
//                dialogLayout,
//                ViewTreeSavedStateRegistryOwner.get(composeView)
//        )

        // Initial setup
        updateParameters(onDismissRequest, properties, layoutDirection)
    }

    override fun onStart() {
        super.onStart()

        val window = window ?: error("Dialog has no window")


//        window.setWindowAnimations(R.style.FullScreenDialogAnimation) // TODO enable


        val dimensions = context.screenDimensions()
        val layoutParams = window.attributes
        layoutParams.width = dimensions.x
        layoutParams.height = dimensions.y
        window.attributes = layoutParams

    }

    private fun setLayoutDirection(layoutDirection: LayoutDirection) {
        dialogLayout.layoutDirection = when (layoutDirection) {
            LayoutDirection.Ltr -> android.util.LayoutDirection.LTR
            LayoutDirection.Rtl -> android.util.LayoutDirection.RTL
        }
    }

    // TODO(b/159900354): Make the Android Dialog full screen and the scrim fully transparent

    fun setContent(parentComposition: CompositionContext, children: @Composable () -> Unit) {
        dialogLayout.setContent(parentComposition, children)
    }

    private fun setSecurePolicy(securePolicy: SecureFlagPolicy) {
        val secureFlagEnabled =
                securePolicy.shouldApplySecureFlag(composeView.isFlagSecureEnabled())
        window!!.setFlags(
                if (secureFlagEnabled) {
                    WindowManager.LayoutParams.FLAG_SECURE
                } else {
                    WindowManager.LayoutParams.FLAG_SECURE.inv()
                },
                WindowManager.LayoutParams.FLAG_SECURE
        )
    }

    private fun SecureFlagPolicy.shouldApplySecureFlag(isSecureFlagSetOnParent: Boolean): Boolean {
        return when (this) {
            SecureFlagPolicy.SecureOff -> false
            SecureFlagPolicy.SecureOn  -> true
            SecureFlagPolicy.Inherit   -> isSecureFlagSetOnParent
        }
    }

    private fun View.isFlagSecureEnabled(): Boolean {
        val windowParams = rootView.layoutParams as? WindowManager.LayoutParams
        if (windowParams != null) {
            return (windowParams.flags and WindowManager.LayoutParams.FLAG_SECURE) != 0
        }
        return false
    }

    fun updateParameters(
            onDismissRequest: () -> Unit,
            properties: DialogProperties,
            layoutDirection: LayoutDirection
    ) {
        this.onDismissRequest = onDismissRequest
        this.properties = properties
        setSecurePolicy(properties.securePolicy)
        setLayoutDirection(layoutDirection)
    }

    fun disposeComposition() {
        dialogLayout.disposeComposition()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val result = super.onTouchEvent(event)
        if (result && properties.dismissOnClickOutside) {
            onDismissRequest()
        }

        return result
    }

    override fun cancel() {
        // Prevents the dialog from dismissing itself
        return
    }

    override fun onBackPressed() {
        if (properties.dismissOnBackPress) {
            onDismissRequest()
        }
    }
}