package net.novagamestudios.tools.composables

import android.content.Intent
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.annotation.ColorInt
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Indication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ParagraphStyle
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import net.novagamestudios.tools.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


val ctx @Composable get() = LocalContext.current





@Composable
fun rememberLoggerForComposable(level: Int = 0): Logger {
    val name = methodNameForLogger(1 + level)
    return remember(name) { Logger(name) }
}


@Composable fun verbose(thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { verbose(thr, message) }
@Composable fun debug  (thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { debug  (thr, message) }
@Composable fun info   (thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { info   (thr, message) }
@Composable fun warn   (thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { warn   (thr, message) }
@Composable fun error  (thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { error  (thr, message) }
@Composable fun wtf    (thr: Throwable? = null, message: () -> Any?) = rememberLoggerForComposable(1).apply { wtf    (thr, message) }



@Composable
fun <T> StateFlow<T>.collectValue(
        context: CoroutineContext = EmptyCoroutineContext
): T = collectAsState(context).value

@Composable
fun <T : R, R> Flow<T>.collectValue(
        initial: R,
        context: CoroutineContext = EmptyCoroutineContext
): R = collectAsState(initial, context).value

@Composable
fun <T> MutableStateFlow<T>.collectAsMutableState(
        context: CoroutineContext = EmptyCoroutineContext
): MutableState<T> = collectAsState(context).mutable { value = it }

@Composable
fun <T> MutableStateFlow<T>.collectAsMutableState(
        initial: T,
        context: CoroutineContext = EmptyCoroutineContext
): MutableState<T> = collectAsState(initial, context).mutable { value = it }


@Suppress("FunctionName")
fun <T> MutableState(get: () -> T, set: (T) -> Unit): MutableState<T> = object : MutableState<T> {
    override var value: T
        get() = get()
        set(value) = set(value)
    override fun component1(): T = value
    override fun component2(): (T) -> Unit = { value = it }
}

fun <T> State<T>.mutable(set: (T) -> Unit): MutableState<T> = MutableState({ value }, set)
fun <T> MutableState<T>.immutable(): State<T> = this

fun <T> MutableState<T>.alsoOnSet(f: (T) -> Unit): MutableState<T> = MutableState(
        { value },
        {
            value = it
            f(it)
        }
)



val Color.argb: Int @ColorInt get() = toArgb()





@Composable
fun <I, O> registerForActivityResultWithCallback(
        contract: ActivityResultContract<I, O>,
        onResult: (O) -> Unit = { }
): ActivityResultLauncherWithCallback<I, O> {

    val mainOnResult = rememberUpdatedState(onResult)
    val wrappedOnResult = remember { mutableStateOf<(O) -> Unit>({}) }
    val currentOnResult = remember { { result: O ->
            mainOnResult.value(result)
            wrappedOnResult.value(result)
    } }

    val launcher = rememberLauncherForActivityResult(contract, currentOnResult)
    return remember(launcher) {
        ActivityResultLauncherWithCallbackImpl<I, O>(launcher).also {
            wrappedOnResult.value = { result -> it.onResult(result) }
        }
    }
}

private class ActivityResultLauncherWithCallbackImpl<I, O>(
        val wrappedLauncher: ActivityResultLauncher<I>
) : ActivityResultLauncherWithCallback<I, O>() {
    private var currentCallback: ((O) -> Unit)? = null

    fun onResult(result: O) = currentCallback?.invoke(result).let { currentCallback = null }
    override fun launch(input: I, options: ActivityOptionsCompat?, onResult: (O) -> Unit) {
        if (currentCallback != null) throw IllegalStateException("Cannot launch multiple contracts at once")
        currentCallback = onResult
        wrappedLauncher.launch(input, options)
    }
    override fun unregister() {
        currentCallback = null
        wrappedLauncher.unregister()
    }
    override fun getContract(): ActivityResultContract<I, *> = wrappedLauncher.contract
}



@Composable
fun takeDirectoryAccessLauncher(
    logger: Logger? = null,
    onResult: (Uri?) -> Unit = { }
): ActivityResultLauncher<Uri?> {
    val ctx = ctx
    val contract = remember(logger, onResult) { selectDirectoryContract(logger) }
    return rememberLauncherForActivityResult(contract) {
        logger?.info { "Received uri is '$it'" }
        val rwFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        if (it != null) ctx.contentResolver.takePersistableUriPermission(it, rwFlags)
        onResult(it)
    }
}


inline fun Modifier.onlyIf(p: () -> Boolean, f: Modifier.() -> Modifier): Modifier = if (p()) f() else this
inline fun Modifier.onlyIf(p: Boolean, f: Modifier.() -> Modifier): Modifier = onlyIf({ p }, f)

inline fun <T> Modifier.onlyIfNotNull(t: T?, f: Modifier.(T) -> Modifier): Modifier = if (t != null) f(t) else this




val String.annotated: AnnotatedString get() = annotated()
fun String.annotated(
    spanStyles: List<AnnotatedString.Range<SpanStyle>> = listOf(),
    paragraphStyles: List<AnnotatedString.Range<ParagraphStyle>> = listOf()
): AnnotatedString = AnnotatedString(this, spanStyles, paragraphStyles)



@Composable
fun <T> rememberMutableState(value: () -> T): MutableState<T> = remember { mutableStateOf(value()) }
@Composable
fun <T> rememberMutableState(key1: Any?, value: () -> T): MutableState<T> = remember(key1) { mutableStateOf(value()) }
@Composable
fun <T> rememberMutableState(key1: Any?, key2: Any?, value: () -> T): MutableState<T> = remember(key1, key2) { mutableStateOf(value()) }

@Composable
fun <T> rememberWithPrevious(initial: T, calculation: (T) -> T): T {
    val previous = remember { mutableStateOf(initial) }
    return remember { calculation(previous.value).also { previous.value = it } }
}
@Composable
fun <T> rememberWithPrevious(key1: Any?, initial: T, calculation: (T) -> T): T {
    val previous = remember { mutableStateOf(initial) }
    return remember(key1) { calculation(previous.value).also { previous.value = it } }
}
@Composable
fun <T> rememberMutableStateWithPrevious(initial: () -> T, calculation: (T) -> T): MutableState<T> {
    val previous = remember { mutableStateOf(initial()) }
    return rememberMutableState { calculation(previous.value).also { previous.value = it } }
}
@Composable
fun <T> rememberMutableStateWithPrevious(key1: Any?, initial: () -> T, calculation: (T) -> T): MutableState<T> {
    val previous = remember { mutableStateOf(initial()) }
    return rememberMutableState(key1) { calculation(previous.value).also { previous.value = it } }
}




@Composable
fun OnLifecycleEventWithOwner(onEvent: (owner: LifecycleOwner, event: Lifecycle.Event) -> Unit) {
    val rememberedOnEvent = rememberUpdatedState(onEvent)
    val lifecycleOwner = rememberUpdatedState(LocalLifecycleOwner.current)
    DisposableEffect(lifecycleOwner.value) {
        val lifecycle = lifecycleOwner.value.lifecycle
        val observer = LifecycleEventObserver { owner, event ->
            rememberedOnEvent.value(owner, event)
        }
        lifecycle.addObserver(observer)
        onDispose {
            lifecycle.removeObserver(observer)
        }
    }
}

@Composable
fun OnLifecycleEvent(event: Lifecycle.Event, onEvent: (LifecycleOwner) -> Unit) = OnLifecycleEventWithOwner {
    owner, e -> if (event == e) onEvent(owner)
}

@Composable
fun OnLifecycleEvent(onEvent: (Lifecycle.Event) -> Unit) = OnLifecycleEventWithOwner {
    _, event -> onEvent(event)
}





@Composable
fun TextButton(
        text: AnnotatedString,
        onClick: () -> Unit,
        modifier: Modifier = Modifier,
        enabled: Boolean = true,
        interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
        elevation: ButtonElevation? = null,
        shape: Shape = MaterialTheme.shapes.small,
        border: BorderStroke? = null,
        colors: ButtonColors = ButtonDefaults.textButtonColors(),
        contentPadding: PaddingValues = ButtonDefaults.TextButtonContentPadding
) = TextButton(
        onClick = onClick,
        modifier = modifier,
        enabled = enabled,
        interactionSource = interactionSource,
        elevation = elevation,
        shape = shape,
        border = border,
        colors = colors,
        contentPadding = contentPadding
) {
    Text(text)
}


@Composable
fun ProvideTextStyleAndAlpha(style: TextStyle, alpha: Float, content: @Composable () -> Unit) {
    ProvideTextStyle(style) { ProvideAlpha(alpha) { content() } }
}

@Composable
fun ProvideAlpha(alpha: Float, content: @Composable () -> Unit) {
    CompositionLocalProvider(LocalContentAlpha provides alpha) { content() }
}




fun LazyListState.isItemVisible(index: Int): Boolean = layoutInfo.visibleItemsInfo.any { it.index == index }

suspend fun LazyListState.scrollToItemIfNotVisible(index: Int, scrollOffset: Int = 0) {
    if (!isItemVisible(index)) scrollToItem(index, scrollOffset)
}
suspend fun LazyListState.animateScrollToItemIfNotVisible(index: Int, scrollOffset: Int = 0) {
    if (!isItemVisible(index)) animateScrollToItem(index, scrollOffset)
}





sealed class Progress {
    data class Determinate(/** 0 to 1 */ val value: Float) : Progress()
    object Indeterminate : Progress()
}

/**
 * @param value 0 to 1
 */
@Suppress("FunctionName")
fun Progress(value: Float): Progress.Determinate = Progress.Determinate(value)

@Composable
fun LinearProgressIndicator(
    progress: Progress,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colors.primary,
    backgroundColor: Color = color.copy(alpha = ProgressIndicatorDefaults.IndicatorBackgroundOpacity)
) = when (progress) {
    is Progress.Indeterminate -> LinearProgressIndicator(
            modifier = modifier,
            color = color,
            backgroundColor = backgroundColor
    )
    is Progress.Determinate   -> LinearProgressIndicator(
            progress = progress.value,
            modifier = modifier,
            color = color,
            backgroundColor = backgroundColor
    )
}

@Composable
fun CircularProgressIndicator(
    progress: Progress,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colors.primary,
    strokeWidth: Dp = ProgressIndicatorDefaults.StrokeWidth
) = when (progress) {
    is Progress.Indeterminate -> CircularProgressIndicator(
            modifier = modifier,
            color = color,
            strokeWidth = strokeWidth
    )
    is Progress.Determinate   -> CircularProgressIndicator(
            progress = progress.value,
            modifier = modifier,
            color = color,
            strokeWidth = strokeWidth
    )
}



@Composable
fun SpacerHorizontal(size: Dp, modifier: Modifier = Modifier) = Spacer(modifier = modifier.width(size))
@Composable
fun SpacerVertical(size: Dp, modifier: Modifier = Modifier) = Spacer(modifier = modifier.height(size))


@Composable
fun SimpleButton(
        onClick: () -> Unit,
        modifier: Modifier = Modifier,
        enabled: Boolean = true,
        indication: Indication = rememberRipple(),
        interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
        content: @Composable BoxScope.() -> Unit
) {
    Box(modifier = modifier
            .clickable(
                    onClick = onClick,
                    enabled = enabled,
                    role = Role.Button,
                    interactionSource = interactionSource,
                    indication = indication
            ),
        contentAlignment = Alignment.Center
    ) {
        val contentAlpha = if (enabled) LocalContentAlpha.current else ContentAlpha.disabled
        CompositionLocalProvider(LocalContentAlpha provides contentAlpha) {
            content()
        }
    }
}


