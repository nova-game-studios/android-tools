package net.novagamestudios.tools.collections

class RedBlackTreeMap<K : Comparable<K>, V> : AbstractMutableSortedMap<K, V>() {
    private val set = RedBlackTreeSet<Entry<K, V>>()

    private fun K.find(): Entry<K, V>? = set.find(Entry(this))

    override val first: K get() = set.first.key
    override val last: K get() = set.last.key

    override val entries: MutableSortedSet<MutableSortedMapEntry<K, V>>
        get() = object : AbstractMutableSortedSet<MutableSortedMapEntry<K, V>>() {
        override val size: Int get() = set.size
        override val first: MutableSortedMapEntry<K, V> get() = set.first
        override val last: MutableSortedMapEntry<K, V> get() = set.last
        override fun iterator(): MutableSortedIterator<MutableSortedMapEntry<K, V>> = set.iterator()
        override fun add(element: MutableSortedMapEntry<K, V>): Boolean = throw UnsupportedOperationException()
        override fun clear() = set.clear()
    }
    override val keys: MutableSortedSet<K>
        get() = object : AbstractMutableSortedSet<K>() {
        override val size: Int get() = set.size
        override val first: K get() = set.first.key
        override val last: K get() = set.last.key
        override fun iterator(): MutableSortedIterator<K> = set.iterator().mappedMutable { it.key }
        override fun contains(element: K): Boolean = element.find() != null
        override fun add(element: K): Boolean = throw UnsupportedOperationException()
        override fun clear() = set.clear()
    }
    override val values: MutableSortedCollection<V>
        get() = object : AbstractMutableSortedCollection<V>() {
        override val size: Int get() = set.size
        override val first: V get() = set.first.value
        override val last: V get() = set.last.value
        override fun iterator(): MutableSortedIterator<V> = entries.iterator().mappedMutable { it.value }
        override fun add(element: V): Boolean = throw UnsupportedOperationException()
        override fun clear() = set.clear()
    }

    override fun containsKey(key: K): Boolean = keys.contains(key)
    override fun containsValue(value: V): Boolean = values.contains(value)
    override fun get(key: K): V? = key.find()?.value
    override fun put(key: K, value: V): V? = key.find()?.setValue(value) ?: set.add(Entry(key, value)).let { null }
    override fun remove(key: K): V? = key.find()?.also { set.remove(it) }?.value

    private class Entry<K : Comparable<K>, V>(
            override val key: K,
            private var _value: V? = null
    ) : MutableSortedMapEntry<K, V>, Comparable<Entry<K, V>> {
        override val value: V get() = _value ?: throw NoSuchElementException()
        override fun setValue(newValue: V): V = value.also { _value = newValue }
        override fun compareTo(other: Entry<K, V>): Int = key.compareTo(other.key)
    }

}