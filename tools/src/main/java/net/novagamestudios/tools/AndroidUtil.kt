package net.novagamestudios.tools

import android.app.Activity
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import androidx.activity.result.contract.ActivityResultContract
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment

import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import java.net.URL
import android.graphics.Point

import android.util.DisplayMetrics

import android.content.Context

import android.view.WindowManager
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.StringRes
import androidx.core.app.ActivityOptionsCompat


abstract class ActivityResultLauncherWithCallback<I, O> : ActivityResultLauncher<I>() {
    override fun launch(input: I, options: ActivityOptionsCompat?) {
        launch(input, options) { /* noop */ }
    }
    abstract fun launch(input: I, options: ActivityOptionsCompat? = null, onResult: (O) -> Unit)
}


fun selectDirectoryContract(logger: Logger? = null) = object : ActivityResultContract<Uri?, Uri?>() {
    override fun createIntent(context: Context, input: Uri?): Intent {
        val rwFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION
        val intentFlags = rwFlags or
                Intent.FLAG_GRANT_PREFIX_URI_PERMISSION or
                Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        return Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            addCategory(Intent.CATEGORY_DEFAULT)
            addFlags(intentFlags)
            if (input != null) input
                    .let {
                        trySetInitialTreeUri(context, it)
                                .alsoIfTrue { logger?.info { "Initial uri is '$it' (try #1)" } }
                    }
                    .letIfFalse {
                        input.toFile().guessPrimaryTreeUri()
                    }
                    ?.let {
                        trySetInitialTreeUri(context, it)
                                .alsoIfTrue { logger?.info { "Initial uri is '$it' (try #2)" } }
                    }
                    .letIfNull { false }
                    .letIfFalse { logger?.warn { "Could not set initial uri" } }
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Uri? =
            intent?.data?.takeIf { resultCode == Activity.RESULT_OK }

}


suspend fun takeDirectoryAccess(deferrable: DeferrableActivityResult, defaultDirectory: Uri?, logger: Logger? = null): Uri? {
    val rwFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
            Intent.FLAG_GRANT_WRITE_URI_PERMISSION
    val intentFlags = rwFlags or
            Intent.FLAG_GRANT_PREFIX_URI_PERMISSION or
            Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
    return deferrable.startActivity(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
        addCategory(Intent.CATEGORY_DEFAULT)
        addFlags(intentFlags)
        if (defaultDirectory != null) defaultDirectory
                .let {
                    trySetInitialTreeUri(deferrable.context, it)
                            .alsoIfTrue { logger?.info { "Initial uri is '$it' (try #1)" } }
                }
                .letIfFalse {
                    defaultDirectory.toFile().guessPrimaryTreeUri()
                }
                ?.let {
                    trySetInitialTreeUri(deferrable.context, it)
                            .alsoIfTrue { logger?.info { "Initial uri is '$it' (try #2)" } }
                }
                .letIfNull { false }
                .letIfFalse { logger?.warn { "Could not set initial uri" } }
    }, null)?.takeIf { it.resultCode == Activity.RESULT_OK }?.data?.data?.also {
        deferrable.context.contentResolver.takePersistableUriPermission(it, rwFlags)
    }
}









// Use BoundServiceConnection (see TaskService)
//inline fun <reified S, C : ServiceConnection> Context.connectToService(connectionSupplier: () -> C): C =
//        connectToService(S::class.java, connectionSupplier)
//
//inline fun <C : ServiceConnection> Context.connectToService(service: Class<*>, connectionSupplier: () -> C): C {
//    startService(Intent(this, service))
//    return connectionSupplier().also {
//        bindService(Intent(this, service), it, Context.BIND_AUTO_CREATE)
//    }
//}


val Fragment.ctx get() = requireContext()




typealias NotificationBuilder = NotificationCompat.Builder


fun Context.lazyNotificationChannel(
        id: String,
        @StringRes name: Int,
        @StringRes description: Int,
        importance: Int,
        initializer: NotificationChannel.() -> Unit
): Lazy<NotificationChannelCompat> = lazy {
    NotificationChannelCompat(this, id, name, description, importance, initializer)
}

data class NotificationChannelCompat(
        val id: String,
        @StringRes val name: Int,
        @StringRes val description: Int,
        val importance: Int,
        val channel: NotificationChannel?
) {
    companion object {
        operator fun invoke(
                context: Context,
                id: String,
                @StringRes name: Int,
                @StringRes description: Int,
                importance: Int,
                initializer: NotificationChannel.() -> Unit
        ): NotificationChannelCompat = NotificationChannelCompat(id, name, description, importance, apiLevelO26 {
            NotificationChannel(id, context.getString(name), importance)
                    .apply(initializer)
                    .also {
                        context.systemService<NotificationManager>(Context.NOTIFICATION_SERVICE)!!
                                .createNotificationChannel(it)
                    }
        })
    }
}




fun NotificationChannel?.builder(context: Context): NotificationCompat.Builder =
        if (this != null && Build.VERSION.SDK_INT >= apiLevelO26)
            NotificationCompat.Builder(context, id)
        else @Suppress("DEPRECATION") NotificationCompat.Builder(context)

fun NotificationBuilder.notify(context: Context, id: Int) = build().notify(context, id)

fun Notification.notify(context: Context, id: Int) =
        NotificationManagerCompat.from(context).notify(id, this)





@Suppress("UNCHECKED_CAST")
fun <S> Context.systemService(name: String): S? = this.getSystemService(name) as? S

fun Context.clipboardManager(): ClipboardManager? = systemService(Context.CLIPBOARD_SERVICE)

fun Context.clipboardText(): CharSequence? {
    val clipboardManager = clipboardManager()
    val primaryClip = clipboardManager?.primaryClip
    val firstItem = primaryClip?.takeIf { it.itemCount > 0 }?.getItemAt(0)
    return firstItem?.text
}


fun Context.openVideoInYoutube(videoID: String) = try {
    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$videoID")))
} catch (ex: ActivityNotFoundException) {
    openUrlInYoutube(videoID)
}

fun Context.openUrlInYoutube(idOrUrl: String): Unit = toYoutubeUrl(idOrUrl)?.let { openUrl(it) }.void()

fun Context.openUrl(url: Uri) = startActivity(Intent(Intent.ACTION_VIEW, url))

/**
 * @return Size in px
 */
fun Context.screenDimensions(): Point {
    val wm = systemService<WindowManager>(Context.WINDOW_SERVICE)!!
    val display = wm.defaultDisplay
    val metrics = DisplayMetrics()
    display.getMetrics(metrics)
    val screenWidth = metrics.widthPixels
    var screenHeight = metrics.heightPixels

    // find out if status bar has already been subtracted from screenHeight
    display.getRealMetrics(metrics)
    val physicalHeight = metrics.heightPixels
    val statusBarHeight: Int = statusBarHeight()
    val navigationBarHeight: Int = navigationBarHeight()
    val heightDelta = physicalHeight - screenHeight
    if (heightDelta == 0 || heightDelta == navigationBarHeight) screenHeight -= statusBarHeight
    return Point(screenWidth, screenHeight)
}

fun Context.statusBarHeight(): Int {
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
}

fun Context.navigationBarHeight(): Int {
    val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
}


suspend fun URL.loadDrawable(srcName: String = ""): Drawable = withContext(Dispatchers.IO) {
//    Drawable.createFromStream(content as InputStream, srcName)
    @Suppress("BlockingMethodInNonBlockingContext")
    openStream().use { Drawable.createFromStream(it, srcName)!! }
}

suspend fun URL.loadBitmap(): Bitmap = withContext(Dispatchers.IO) {
    @Suppress("BlockingMethodInNonBlockingContext")
    openStream().use { BitmapFactory.decodeStream(it) }
}











const val apiLevelB1  = Build.VERSION_CODES.BASE
const val apiLevelB2  = Build.VERSION_CODES.BASE_1_1
const val apiLevelC3  = Build.VERSION_CODES.CUPCAKE
const val apiLevelD4  = Build.VERSION_CODES.DONUT
const val apiLevelE5  = Build.VERSION_CODES.ECLAIR
const val apiLevelE6  = Build.VERSION_CODES.ECLAIR_0_1
const val apiLevelE7  = Build.VERSION_CODES.ECLAIR_MR1
const val apiLevelF8  = Build.VERSION_CODES.FROYO
const val apiLevelG9  = Build.VERSION_CODES.GINGERBREAD
const val apiLevelG10 = Build.VERSION_CODES.GINGERBREAD_MR1
const val apiLevelH11 = Build.VERSION_CODES.HONEYCOMB
const val apiLevelH12 = Build.VERSION_CODES.HONEYCOMB_MR1
const val apiLevelH13 = Build.VERSION_CODES.HONEYCOMB_MR2
const val apiLevelI14 = Build.VERSION_CODES.ICE_CREAM_SANDWICH
const val apiLevelI15 = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
const val apiLevelJ16 = Build.VERSION_CODES.JELLY_BEAN
const val apiLevelJ17 = Build.VERSION_CODES.JELLY_BEAN_MR1
const val apiLevelJ18 = Build.VERSION_CODES.JELLY_BEAN_MR2
const val apiLevelK19 = Build.VERSION_CODES.KITKAT
const val apiLevelK20 = Build.VERSION_CODES.KITKAT_WATCH
const val apiLevelL21 = Build.VERSION_CODES.LOLLIPOP
const val apiLevelL22 = Build.VERSION_CODES.LOLLIPOP_MR1
const val apiLevelM23 = Build.VERSION_CODES.M
const val apiLevelN24 = Build.VERSION_CODES.N
const val apiLevelN25 = Build.VERSION_CODES.N_MR1
const val apiLevelO26 = Build.VERSION_CODES.O
const val apiLevelO27 = Build.VERSION_CODES.O_MR1
const val apiLevelP28 = Build.VERSION_CODES.P
const val apiLevelQ29 = Build.VERSION_CODES.Q
const val apiLevelR30 = Build.VERSION_CODES.R

inline fun <R> apiLevelB1(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelB1 ) f() else null
inline fun <R> apiLevelB2(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelB2 ) f() else null
inline fun <R> apiLevelC3(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelC3 ) f() else null
inline fun <R> apiLevelD4(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelD4 ) f() else null
inline fun <R> apiLevelE5(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelE5 ) f() else null
inline fun <R> apiLevelE6(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelE6 ) f() else null
inline fun <R> apiLevelE7(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelE7 ) f() else null
inline fun <R> apiLevelF8(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelF8 ) f() else null
inline fun <R> apiLevelG9(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelG9 ) f() else null
inline fun <R> apiLevelG10(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelG10) f() else null
inline fun <R> apiLevelH11(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelH11) f() else null
inline fun <R> apiLevelH12(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelH12) f() else null
inline fun <R> apiLevelH13(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelH13) f() else null
inline fun <R> apiLevelI14(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelI14) f() else null
inline fun <R> apiLevelI15(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelI15) f() else null
inline fun <R> apiLevelJ16(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelJ16) f() else null
inline fun <R> apiLevelJ17(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelJ17) f() else null
inline fun <R> apiLevelJ18(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelJ18) f() else null
inline fun <R> apiLevelK19(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelK19) f() else null
inline fun <R> apiLevelK20(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelK20) f() else null
inline fun <R> apiLevelL21(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelL21) f() else null
inline fun <R> apiLevelL22(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelL22) f() else null
inline fun <R> apiLevelM23(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelM23) f() else null
inline fun <R> apiLevelN24(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelN24) f() else null
inline fun <R> apiLevelN25(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelN25) f() else null
inline fun <R> apiLevelO26(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelO26) f() else null
inline fun <R> apiLevelO27(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelO27) f() else null
inline fun <R> apiLevelP28(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelP28) f() else null
inline fun <R> apiLevelQ29(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelQ29) f() else null
inline fun <R> apiLevelR30(f: () -> R): R? = if (Build.VERSION.SDK_INT >= apiLevelR30) f() else null





