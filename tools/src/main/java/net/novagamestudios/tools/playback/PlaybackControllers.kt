package net.novagamestudios.tools.playback

import android.support.v4.media.RatingCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.media.AudioAttributesCompat
import androidx.media2.common.*
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.MediaSource
import com.google.common.util.concurrent.ListenableFuture
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.suspendCancellableCoroutine
import net.novagamestudios.tools.*
import net.novagamestudios.tools.playback.PlayerEvent.Companion.asPlayerEvents
import net.novagamestudios.tools.collections.copy
import net.novagamestudios.tools.collections.move
import net.novagamestudios.tools.collections.removeIndices
import java.util.*
import kotlin.NoSuchElementException
import kotlin.coroutines.resume
import kotlin.time.Duration
import kotlin.time.ExperimentalTime


interface PlaybackController {

    companion object {
        const val DefaultFastForwardIncrement: Long = 5000
        const val DefaultRewindIncrement: Long = 5000
    }


    val playbackState: StateFlow<PlaybackState?>


    suspend fun play()
    suspend fun pause()
    suspend fun togglePlay() { if (playbackState.value?.isPlaying == true) pause() else play() }
    suspend fun stop()
    suspend fun seekTo(ms: Long)
    suspend fun fastForward() { seekToClamped((playbackState.value?.position ?: 0) + DefaultFastForwardIncrement) }
    suspend fun rewind() { seekToClamped((playbackState.value?.position ?: 0) - DefaultRewindIncrement) }

    suspend fun setShuffleMode(mode: ShuffleMode)
    suspend fun setRepeatMode(mode: RepeatMode)
    suspend fun setPlaybackParams(params: PlaybackParams)
    suspend fun setCaptionEnabled(enabled: Boolean)

    private suspend fun seekToClamped(ms: Long) {
        val duration = playbackState.value?.duration ?: C.TIME_UNSET
        seekTo(ms.min(0).letIf(duration != C.TIME_UNSET) { it.max(duration) })
    }
}

interface QueueController<Item : QueueItem> {

    val queueState: StateFlow<QueueState<Item>?>

    suspend fun skipToIndex(index: Int)
    suspend fun skipToNext()
    suspend fun skipToPrev()
    suspend fun skipToItem(item: Item) { skipToIndex(item.toIndex) }

    suspend fun set(item: Item)
    suspend fun setAll(items: Collection<Item>)
    suspend fun add(item: Item)
    suspend fun addAll(items: Collection<Item>)
    suspend fun insert(index: Int, item: Item)
    suspend fun insertAfter(after: Item, item: Item) { insert(after.toIndex + 1, item) }
    suspend fun insertAll(index: Int, items: Collection<Item>)
    suspend fun insertAllAfter(item: Item, items: Collection<Item>) { insertAll(item.toIndex + 1, items) }
    suspend fun move(fromIndex: Int, toIndex: Int)
    suspend fun remove(index: Int)
    suspend fun remove(item: Item) { remove(item.toIndex) }
    suspend fun clear()

    suspend fun setRating(rating: RatingCompat)

    private val Item.toIndex get() = queueState.value?.queue?.indexOf(this) ?: -1

}



@ExperimentalCoroutinesApi
@ExperimentalTime
class ExoPlaybackController constructor(
        private val player: ExoPlayer,
        scope: CoroutineScope,
        private val positionUpdateInterval: Duration
) : PlaybackController, Logger {

    private val playerEvents = MutableEventFlow<PlayerEvents>()

    override val playbackState: StateFlow<PlaybackState> = playerEvents.flatMapLatest { event ->
        event.use(Unit) {
            if (player.isPlaying) {
                flow {
                    net.novagamestudios.tools.repeat {
                        emit(PlaybackState(player))
                        delay(positionUpdateInterval)
                    }
                }
            } else flowOf(PlaybackState(player))
        } ?: emptyFlow()
    }.stateIn(scope, SharingStarted.Eagerly, PlaybackState(player))

    private val listener = DefaultPlayerEventListener { events->
        info { events.joinToString() }
        playerEvents.dispatch(events)
    }

    init {
        player.addListener(listener)
    }
    fun close() {
        player.removeListener(listener)
    }

    override suspend fun play() {
        if (player.playbackState == Player.STATE_ENDED) player.seekToDefaultPosition()
        player.play()
    }
    override suspend fun pause() { player.pause() }
    override suspend fun stop() { player.stop() }
    override suspend fun seekTo(ms: Long) { player.seekTo(ms) }
    override suspend fun setShuffleMode(mode: ShuffleMode) {
        player.shuffleModeEnabled = mode != ShuffleMode.OFF
    }
    override suspend fun setRepeatMode(mode: RepeatMode) {
        player.repeatMode = mode.exoConstant
    }
    override suspend fun setPlaybackParams(params: PlaybackParams) {
        player.setPlaybackParameters(params.asPlaybackParameters)
    }
    override suspend fun setCaptionEnabled(enabled: Boolean) {
        warn { "setCaptionEnabled is not supported" }
    }
}


class ExoQueueController<Item : QueueItem>(
        private val player: ExoPlayer
) : QueueController<Item>, Logger {

    private val playlist = ExoPlayerAsPlaylist<Item>(player) { it.media }

    private val _queueState = MutableStateFlow(QueueState<Item>(player, emptyList()))
    override val queueState: StateFlow<QueueState<Item>> get() = _queueState

    private val listener = DefaultPlayerEventListener { events ->
        info { events.joinToString() }
        val queue = playlist.copy()
        info { "queue: $queue" }
        _queueState.value = QueueState(player, queue)
    }

    init {
        player.addListener(listener)
    }

    fun close() {
        player.removeListener(listener)
    }


    override suspend fun skipToNext() { player.next() }
    override suspend fun skipToPrev() { player.previous() }
    override suspend fun skipToIndex(index: Int) { seekToWindow(index.clamp(-1, queueState.value.queue.size)) }

    private fun seekToWindow(index: Int) {
        val queue = queueState.value.queue
        if (queue.isNotEmpty()) player.seekToDefaultPosition(index.clampIndex(queue))
    }

    override suspend fun set(item: Item) { info { "set($item)" }; playlist.set(item) }
    override suspend fun setAll(items: Collection<Item>) { info { "setAll(${items.joinToString()})" }; playlist.setAll(items) }
    override suspend fun add(item: Item) { info { "add($item)" }; playlist.add(item) }
    override suspend fun addAll(items: Collection<Item>) { info { "addAll(${items.joinToString()})" }; playlist.addAll(items) }
    override suspend fun insert(index: Int, item: Item) { playlist.add(index, item) }
    override suspend fun insertAll(index: Int, items: Collection<Item>) { playlist.addAll(index, items) }
    override suspend fun move(fromIndex: Int, toIndex: Int) { playlist.move(fromIndex, toIndex) }
    override suspend fun remove(index: Int) { playlist.removeAt(index) }
    override suspend fun clear() { info { "clear()" }; playlist.clear() }

    override suspend fun setRating(rating: RatingCompat) { warn { "setRating is not supported" } }
}



@ExperimentalTime
@ExperimentalCoroutinesApi
class SessionPlaybackController(
        private val player: SessionPlayer,
        scope: CoroutineScope,
        private val positionUpdateInterval: Duration
) : PlaybackController {

    private val playerEvents = MutableEventFlow<Unit>()

    override val playbackState: StateFlow<PlaybackState> = playerEvents.flatMapLatest { event ->
        event.use(Unit) {
            if (player.isPlaying) {
                flow {
                    net.novagamestudios.tools.repeat {
                        emit(PlaybackState(player))
                        delay(positionUpdateInterval)
                    }
                }
            } else flowOf(PlaybackState(player))
        } ?: emptyFlow()
    }.stateIn(scope, SharingStarted.Eagerly, PlaybackState(player))


    private val callback = DefaultSessionPlayerCallback {
        playerEvents.dispatch()
    }

    init {
        player.registerPlayerCallback(scope.coroutineContext.asExecutor, callback)
    }
    fun close() {
        player.unregisterPlayerCallback(callback)
    }


    override suspend fun play() {
        if (playbackState.value.isIdle) player.prepare().await()
        player.play().await()
//        playerEvents.dispatch()
    }
    override suspend fun pause() {
        player.pause().await()
//        playerEvents.dispatch()
    }
    override suspend fun stop() {
        player.stop().await()
//        playerEvents.dispatch()
    }
    override suspend fun seekTo(ms: Long) {
        player.seekTo(ms).await()
//        playerEvents.dispatch()
    }
    override suspend fun setShuffleMode(mode: ShuffleMode) {
        player.setShuffleMode(mode.sessionConstant).await()
//        playerEvents.dispatch()
    }
    override suspend fun setRepeatMode(mode: RepeatMode) {
        player.setRepeatMode(mode.sessionConstant).await()
//        playerEvents.dispatch()
    }
    override suspend fun setPlaybackParams(params: PlaybackParams) {
        player.setPlaybackSpeed(params.speed).await()
//        playerEvents.dispatch()
    }
    override suspend fun setCaptionEnabled(enabled: Boolean) {
        throw UnsupportedOperationException()
//        playerEvents.dispatch()
    }

}

interface Playlist<Item> : List<Item> {
    fun set(item: Item)
    fun setAll(items: Collection<Item>)
    fun add(item: Item): Boolean
    fun addAll(items: Collection<Item>): Boolean
    fun add(index: Int, item: Item)
    fun addAll(index: Int, items: Collection<Item>): Boolean
    fun move(fromIndex: Int, toIndex: Int)
    fun removeAt(index: Int): Item
    fun removeRange(fromIndex: Int, toIndex: Int): List<Item>
    fun clear()
}


class ExoPlayerAsPlaylist<Item>(
        private val player: ExoPlayer,
        private val baseList: MutableList<Item> = mutableListOf(),
        private val mediaProvider: (Item) -> MediaSource
) : Playlist<Item>, List<Item> by baseList {
    override fun set(item: Item) {
        player.setMediaSource(item.convert)
        baseList.clear()
        baseList.add(item)
    }
    override fun setAll(items: Collection<Item>) {
        player.setMediaSources(items.convert)
        baseList.clear()
        baseList.addAll(items)
    }
    override fun add(item: Item): Boolean {
        player.addMediaSource(item.convert)
        return baseList.add(item)
    }
    override fun addAll(items: Collection<Item>): Boolean {
        player.addMediaSources(items.convert)
        return baseList.addAll(items)
    }
    override fun add(index: Int, item: Item) {
        player.addMediaSource(index, item.convert)
        return baseList.add(index, item)
    }
    override fun addAll(index: Int, items: Collection<Item>): Boolean {
        player.addMediaSources(index, items.convert)
        return baseList.addAll(index, items)
    }
    override fun move(fromIndex: Int, toIndex: Int) {
        player.moveMediaItem(fromIndex, toIndex) // TODO maybe bug
        return baseList.move(fromIndex, toIndex)
    }
    override fun removeAt(index: Int): Item {
        player.removeMediaItem(index) // TODO maybe bug
        return baseList.removeAt(index)
    }
    override fun removeRange(fromIndex: Int, toIndex: Int): List<Item> {
        player.removeMediaItems(fromIndex, toIndex) // TODO maybe bug
        return baseList.removeIndices(fromIndex until toIndex)
    }
    override fun clear() {
        player.clearMediaItems() // TODO maybe bug
        baseList.clear()
    }

    private val Item.convert get() = mediaProvider(this)
    private val Collection<Item>.convert get() = map { it.convert }
}



//class DynamicListMediaSource<Item>(
//        private val baseSource: DynamicConcatenatingMediaSource = DynamicConcatenatingMediaSource(false),
//        private val baseList: MutableList<Item> = mutableListOf(),
//        private val sourceProvider: (Item) -> MediaSource
//) : List<Item> by baseList, MediaSource by baseSource {
//
//    fun add(item: Item): Boolean {
//        baseSource.addMediaSource(sourceProvider(item))
//        return baseList.add(item)
//    }
//    fun addAll(items: Collection<Item>): Boolean {
//        baseSource.addMediaSources(items.map { sourceProvider(it) })
//        return baseList.addAll(items)
//    }
//    fun add(index: Int, item: Item) {
//        baseSource.addMediaSource(index, sourceProvider(item))
//        return baseList.add(index, item)
//    }
//    fun addAll(index: Int, items: Collection<Item>): Boolean {
//        baseSource.addMediaSources(index, items.map { sourceProvider(it) })
//        return baseList.addAll(index, items)
//    }
//    fun move(fromIndex: Int, toIndex: Int) {
//        baseSource.moveMediaSource(fromIndex, toIndex)
//        return baseList.move(fromIndex, toIndex)
//    }
//    fun removeAt(index: Int): Item {
//        baseSource.removeMediaSource(index)
//        return baseList.removeAt(index)
//    }
//
//}


private class DefaultPlayerEventListener(
        val onEvent: (PlayerEvents) -> Unit
) : Player.Listener {
    override fun onEvents(player: Player, events: Player.Events) = onEvent(events.asPlayerEvents)
}

private class DefaultSessionPlayerCallback(
        val onEvent: () -> Unit
) : SessionPlayer.PlayerCallback() {
    override fun onPlayerStateChanged(player: SessionPlayer, playerState: Int) = onEvent()
    override fun onBufferingStateChanged(player: SessionPlayer, item: MediaItem?, buffState: Int) = onEvent()
    override fun onPlaybackSpeedChanged(player: SessionPlayer, playbackSpeed: Float) = onEvent()
    override fun onSeekCompleted(player: SessionPlayer, position: Long) = onEvent()
    override fun onPlaylistChanged(player: SessionPlayer, list: MutableList<MediaItem>?, metadata: MediaMetadata?) = onEvent()
    override fun onPlaylistMetadataChanged(player: SessionPlayer, metadata: MediaMetadata?) = onEvent()
    override fun onShuffleModeChanged(player: SessionPlayer, shuffleMode: Int) = onEvent()
    override fun onRepeatModeChanged(player: SessionPlayer, repeatMode: Int) = onEvent()
    override fun onCurrentMediaItemChanged(player: SessionPlayer, item: MediaItem?) = onEvent()
    override fun onPlaybackCompleted(player: SessionPlayer) = onEvent()
    override fun onAudioAttributesChanged(player: SessionPlayer, attributes: AudioAttributesCompat?) = onEvent()
    override fun onVideoSizeChanged(player: SessionPlayer, size: VideoSize) = onEvent()
    override fun onSubtitleData(player: SessionPlayer, item: MediaItem, track: SessionPlayer.TrackInfo, data: SubtitleData) = onEvent()
    override fun onTracksChanged(player: SessionPlayer, tracks: MutableList<SessionPlayer.TrackInfo>) = onEvent()
    override fun onTrackSelected(player: SessionPlayer, trackInfo: SessionPlayer.TrackInfo) = onEvent()
    override fun onTrackDeselected(player: SessionPlayer, trackInfo: SessionPlayer.TrackInfo) = onEvent()
}



typealias PlayerEvents = EnumSet<PlayerEvent>
enum class PlayerEvent(
        val constant: Int
) {
    UNKNOWN                            (C.INDEX_UNSET                                   ),
    TIMELINE_CHANGED                   (Player.EVENT_TIMELINE_CHANGED                   ),
    MEDIA_ITEM_TRANSITION              (Player.EVENT_MEDIA_ITEM_TRANSITION              ),
    TRACKS_CHANGED                     (Player.EVENT_TRACKS_CHANGED                     ),
    IS_LOADING_CHANGED                 (Player.EVENT_IS_LOADING_CHANGED                 ),
    PLAYBACK_STATE_CHANGED             (Player.EVENT_PLAYBACK_STATE_CHANGED             ),
    PLAY_WHEN_READY_CHANGED            (Player.EVENT_PLAY_WHEN_READY_CHANGED            ),
    PLAYBACK_SUPPRESSION_REASON_CHANGED(Player.EVENT_PLAYBACK_SUPPRESSION_REASON_CHANGED),
    IS_PLAYING_CHANGED                 (Player.EVENT_IS_PLAYING_CHANGED                 ),
    REPEAT_MODE_CHANGED                (Player.EVENT_REPEAT_MODE_CHANGED                ),
    SHUFFLE_MODE_ENABLED_CHANGED       (Player.EVENT_SHUFFLE_MODE_ENABLED_CHANGED       ),
    PLAYER_ERROR                       (Player.EVENT_PLAYER_ERROR                       ),
    POSITION_DISCONTINUITY             (Player.EVENT_POSITION_DISCONTINUITY             ),
    PLAYBACK_PARAMETERS_CHANGED        (Player.EVENT_PLAYBACK_PARAMETERS_CHANGED        );

    companion object {
        fun from(constant: Int): PlayerEvent = when (constant) {
            Player.EVENT_TIMELINE_CHANGED                    -> TIMELINE_CHANGED
            Player.EVENT_MEDIA_ITEM_TRANSITION               -> MEDIA_ITEM_TRANSITION
            Player.EVENT_TRACKS_CHANGED                      -> TRACKS_CHANGED
            Player.EVENT_IS_LOADING_CHANGED                  -> IS_LOADING_CHANGED
            Player.EVENT_PLAYBACK_STATE_CHANGED              -> PLAYBACK_STATE_CHANGED
            Player.EVENT_PLAY_WHEN_READY_CHANGED             -> PLAY_WHEN_READY_CHANGED
            Player.EVENT_PLAYBACK_SUPPRESSION_REASON_CHANGED -> PLAYBACK_SUPPRESSION_REASON_CHANGED
            Player.EVENT_IS_PLAYING_CHANGED                  -> IS_PLAYING_CHANGED
            Player.EVENT_REPEAT_MODE_CHANGED                 -> REPEAT_MODE_CHANGED
            Player.EVENT_SHUFFLE_MODE_ENABLED_CHANGED        -> SHUFFLE_MODE_ENABLED_CHANGED
            Player.EVENT_PLAYER_ERROR                        -> PLAYER_ERROR
            Player.EVENT_POSITION_DISCONTINUITY              -> POSITION_DISCONTINUITY
            Player.EVENT_PLAYBACK_PARAMETERS_CHANGED         -> PLAYBACK_PARAMETERS_CHANGED
            else                                             -> UNKNOWN
        }
        val Player.Events.asPlayerEvents: PlayerEvents
            get() = sequence {
            var i = 0
            while (i < size()) yield(from(get(i++)))
        }.toEnumSet()
    }
}




//class SessionConnectorPlaybackCallbacks(
//        private val target: PlaybackController
//) : MediaSessionConnector.PlaybackController {
//    override fun getCommands(): Array<String>? = null
//    override fun onCommand(player: Player, command: String, extras: Bundle, cb: ResultReceiver) = noop
//    override fun getSupportedPlaybackActions(player: Player?): Long =
//            target.supportedPlaybackActions.map { it.constant }.fold(0L) { l, r -> l or r }
//    override fun onPlay(player: Player) = target.play()
//    override fun onPause(player: Player) = target.pause()
//    override fun onSeekTo(player: Player, position: Long) = target.seekTo(position)
//    override fun onFastForward(player: Player) = target.fastForward()
//    override fun onRewind(player: Player) = target.rewind()
//    override fun onStop(player: Player) = target.stop()
//    override fun onSetShuffleMode(player: Player, shuffleMode: Int) =
//            target.setShuffleMode(ShuffleMode.fromPlaybackStateCompat(shuffleMode))
//    override fun onSetRepeatMode(player: Player, repeatMode: Int) =
//            target.setRepeatMode(RepeatMode.fromPlaybackStateCompat(repeatMode))
//}
//
//class SessionConnectorQueueNavigator<ItemId>(
//        private val target: QueueController<ItemId>,
//        private val longIdToItemId: (Long) -> ItemId,
//        private val itemIdToLongId: (ItemId) -> Long
//) : MediaSessionConnector.QueueNavigator {
//    override fun getCommands(): Array<String>? = null
//    override fun onCommand(player: Player?, command: String?, extras: Bundle?, cb: ResultReceiver?) = noop
//    override fun getSupportedQueueNavigatorActions(player: Player?): Long =
//            target.supportedQueueActions.map { it.constant }.fold(0L) { l, r -> l or r }
//    override fun onTimelineChanged(player: Player?) = warn { "onTimelineChanged is not supported" }
//    override fun onCurrentWindowIndexChanged(player: Player?) =
//            target.skipToIndex(player?.currentWindowIndex ?: C.INDEX_UNSET)
//    override fun getActiveQueueItemId(player: Player?): Long =
//            itemIdToLongId(target.queueState.value.currentItem.id)
//    override fun onSkipToNext(player: Player?) = target.skipToNext()
//    override fun onSkipToPrevious(player: Player?) = target.skipToPrev()
//    override fun onSkipToQueueItem(player: Player?, id: Long) =
//            target.skipToItem(longIdToItemId(id))
//}
//
//class SessionConnectorQueueEditor<ItemId>(
//        private val target: QueueController<ItemId>,
//        private val descriptionToItem: (MediaDescriptionCompat) -> QueueItem<ItemId>?
//) : MediaSessionConnector.QueueEditor {
//    override fun getSupportedQueueEditorActions(player: Player?): Long =
//            target.supportedQueueActions.map { it.constant }.fold(0L) { l, r -> l or r }
//    override fun getCommands(): Array<String>? = null
//    override fun onCommand(player: Player?, command: String?, extras: Bundle?, cb: ResultReceiver?) = noop
//    override fun onSetRating(player: Player?, rating: RatingCompat?) =
//            rating?.let { target.setRating(it) }.void()
//    override fun onAddQueueItem(player: Player?, description: MediaDescriptionCompat?) =
//            description?.let { descriptionToItem(it) }?.let { target.add(it) }.void()
//    override fun onAddQueueItem(player: Player?, description: MediaDescriptionCompat?, index: Int) =
//            description?.let { descriptionToItem(it) }?.let { target.insert(index, it) }.void()
//    override fun onRemoveQueueItem(player: Player?, description: MediaDescriptionCompat?) =
//            description?.let { descriptionToItem(it) }?.let { target.remove(it.id) }.void()
//    override fun onRemoveQueueItemAt(player: Player?, index: Int) = target.remove(index)
//}




interface PlaybackCompatActionConstantHolder {
    @PlaybackStateCompat.Actions val constant: Long
}

typealias PlaybackActions = EnumSet<PlaybackAction>
enum class PlaybackAction(
        @PlaybackStateCompat.Actions override val constant: Long
) : PlaybackCompatActionConstantHolder {
    PLAY                  (PlaybackStateCompat.ACTION_PLAY                  ),
    PAUSE                 (PlaybackStateCompat.ACTION_PAUSE                 ),
    PLAY_TOGGLE           (PlaybackStateCompat.ACTION_PLAY_PAUSE            ),
    STOP                  (PlaybackStateCompat.ACTION_STOP                  ),
    SET_SHUFFLE_MODE      (PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE      ),
    SET_REPEAT_MODE       (PlaybackStateCompat.ACTION_SET_REPEAT_MODE       ),
    SET_CAPTIONING_ENABLED(PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED),
    FAST_FORWARD          (PlaybackStateCompat.ACTION_FAST_FORWARD          ),
    REWIND                (PlaybackStateCompat.ACTION_REWIND                ),
    SEEK_TO               (PlaybackStateCompat.ACTION_SEEK_TO               );

    companion object {
        fun from(@PlaybackStateCompat.Actions constant: Long): PlaybackAction = values()
                .find { it.constant == constant }
                .ifNull { throw NoSuchElementException("$constant") }
    }
}

typealias QueueActions = EnumSet<QueueAction>
enum class QueueAction(
        @PlaybackStateCompat.Actions override val constant: Long
) : PlaybackCompatActionConstantHolder {
    SKIP_TO_NEXT          (PlaybackStateCompat.ACTION_SKIP_TO_NEXT          ),
    SKIP_TO_PREVIOUS      (PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS      ),
    SKIP_TO_QUEUE_ITEM    (PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM    ),
    /** QueueEditor only */
    SET_RATING            (PlaybackStateCompat.ACTION_SET_RATING            );

    companion object {
        fun from(@PlaybackStateCompat.Actions constant: Long): QueueAction = values()
                .find { it.constant == constant }
                .ifNull { throw NoSuchElementException("$constant") }
    }
}

data class PlaybackParams(
        val speed: Float = PlaybackParameters.DEFAULT.speed,
        val pitch: Float = PlaybackParameters.DEFAULT.pitch
) {
    companion object {
        fun fromSession(player: SessionPlayer) = PlaybackParams(speed = player.playbackSpeed)
    }
}

val PlaybackParameters.asPlaybackParams: PlaybackParams get() = PlaybackParams(speed, pitch)
val PlaybackParams.asPlaybackParameters: PlaybackParameters get() = PlaybackParameters(speed, pitch)


enum class ShuffleMode(
        @SessionPlayer.ShuffleMode val sessionConstant: Int,
        @PlaybackStateCompat.ShuffleMode val compatConstant: Int
) {
    OFF  (SessionPlayer.SHUFFLE_MODE_NONE , PlaybackStateCompat.SHUFFLE_MODE_NONE ),
    GROUP(SessionPlayer.SHUFFLE_MODE_GROUP, PlaybackStateCompat.SHUFFLE_MODE_GROUP),
    ALL  (SessionPlayer.SHUFFLE_MODE_ALL  , PlaybackStateCompat.SHUFFLE_MODE_ALL  );

    companion object {
        fun fromExo(isEnabled: Boolean): ShuffleMode = if (isEnabled) ALL else OFF
        fun fromSession(@SessionPlayer.ShuffleMode constant: Int): ShuffleMode = values()
                .find { it.sessionConstant == constant }
                .ifNull { OFF }
        fun fromCompat(@PlaybackStateCompat.ShuffleMode constant: Int): ShuffleMode = values()
                .find { it.compatConstant == constant }
                .ifNull { OFF }
    }
}


enum class RepeatMode(
        @Player.RepeatMode val exoConstant: Int,
        @SessionPlayer.RepeatMode val sessionConstant: Int,
        @PlaybackStateCompat.RepeatMode val compatConstant: Int
) {
    OFF  (Player.REPEAT_MODE_OFF, SessionPlayer.REPEAT_MODE_NONE , PlaybackStateCompat.REPEAT_MODE_NONE ),
    ONE  (Player.REPEAT_MODE_ONE, SessionPlayer.REPEAT_MODE_ONE  , PlaybackStateCompat.REPEAT_MODE_ONE  ),
    GROUP(                    -1, SessionPlayer.REPEAT_MODE_GROUP, PlaybackStateCompat.REPEAT_MODE_GROUP),
    ALL  (Player.REPEAT_MODE_ALL, SessionPlayer.REPEAT_MODE_ALL  , PlaybackStateCompat.REPEAT_MODE_ALL  );

    companion object {
        fun fromExo(@Player.RepeatMode constant: Int): RepeatMode = values()
                .find { it.exoConstant == constant }
                .ifNull { OFF }
        fun fromSession(@SessionPlayer.RepeatMode constant: Int): RepeatMode = values()
                .find { it.sessionConstant == constant }
                .ifNull { OFF }
        fun fromCompat(@PlaybackStateCompat.RepeatMode constant: Int): RepeatMode = values()
                .find { it.compatConstant == constant }
                .ifNull { OFF }
    }
}


enum class WakeMode(
        @C.WakeMode val exoConstant: Int
) {
    NONE   (C.WAKE_MODE_NONE   ),
    LOCAL  (C.WAKE_MODE_LOCAL  ),
    NETWORK(C.WAKE_MODE_NETWORK);

    companion object {
        fun fromExo(@C.WakeMode constant: Int): WakeMode = values()
                .find { it.exoConstant == constant }
                .ifNull { NONE }
    }
}


data class PlaybackState(
    /** position with ads */
        val position: Long = 0,
    /** position ignoring ads */
        val contentPosition: Long = position,
    val bufferedPosition: Long = 0,
    val duration: Long = 0,

    val isPlaying: Boolean = false,
    val isPlayingAd: Boolean = false,
    val isLoading: Boolean = false,
    val isLoadingAndStarved: Boolean = false,
    val isIdle: Boolean = false,
    val error: Throwable? = null,

    val repeatMode: RepeatMode = RepeatMode.OFF,
    val shuffleMode: ShuffleMode = ShuffleMode.OFF,
    val params: PlaybackParams = PlaybackParams()
) {
    val percent: Float get() = percentFrom(position)
    val contentPercent: Float get() = percentFrom(contentPosition)
    val bufferedPercent: Float get() = percentFrom(bufferedPosition)
    val hasError get() = error != null

    private fun percentFrom(position: Long) =
            if (duration == C.TIME_UNSET) 0f
            else duration.toFloat().let { if (it == 0f) 1f else position / it }
}

fun PlaybackState(player: ExoPlayer): PlaybackState = PlaybackState(
    position = player.currentPosition,
    contentPosition = player.contentPosition,
    bufferedPosition = player.bufferedPosition,
    duration = player.duration,

    isPlaying = player.isPlaying,
    isPlayingAd = player.isPlayingAd,
    isLoading = player.isLoading,
    isLoadingAndStarved = player.playbackState == Player.STATE_BUFFERING,
    isIdle = player.playbackState == Player.STATE_IDLE,
    error = player.playerError,

    repeatMode = RepeatMode.fromExo(player.repeatMode),
    shuffleMode = ShuffleMode.fromExo(player.shuffleModeEnabled),
    params = player.playbackParameters.asPlaybackParams
)

fun PlaybackState(player: SessionPlayer): PlaybackState = PlaybackState(
    position = player.currentPosition,
    contentPosition = player.currentPosition,
    bufferedPosition = player.bufferedPosition,
    duration = player.duration,

    isPlaying = player.isPlaying,
    isPlayingAd = false,
    isLoading = player.bufferingState == SessionPlayer.BUFFERING_STATE_BUFFERING_AND_PLAYABLE
                    || player.bufferingState == SessionPlayer.BUFFERING_STATE_BUFFERING_AND_STARVED,
    isLoadingAndStarved = player.bufferingState == SessionPlayer.BUFFERING_STATE_BUFFERING_AND_STARVED,
    isIdle = !player.isPrepared,
    error = if (player.playerState == SessionPlayer.PLAYER_STATE_ERROR) Throwable("Unknown error")
        else null,

    repeatMode = RepeatMode.fromSession(player.repeatMode),
    shuffleMode = ShuffleMode.fromSession(player.shuffleMode),
    params = PlaybackParams.fromSession(player)
)


val SessionPlayer.isPrepared get() = playerState == SessionPlayer.PLAYER_STATE_PAUSED || isPlaying
val SessionPlayer.isPlaying get() = playerState == SessionPlayer.PLAYER_STATE_PLAYING
fun SessionPlayer.stop() = pause()





interface QueueItem {
    val media: MediaSource
}

data class QueueState<Item : QueueItem>(
        val queue: List<Item> = emptyList(),
        val currentIndex: Int = -1,
        val hasNext: Boolean = currentIndex < queue.size - 1,
        val hasPrev: Boolean = currentIndex > 0
) {
    val currentItem get() = queue.getOrNull(currentIndex)
}

fun <Item : QueueItem> QueueState(player: ExoPlayer, queue: List<Item>): QueueState<Item> = QueueState(
        queue = queue,
        currentIndex = player.currentWindowIndex,
        hasNext = player.hasNext(),
        hasPrev = player.hasPrevious()
)





suspend fun <T> ListenableFuture<T>.await(): T = suspendCancellableCoroutine {
    addListener({ it.resume(get()) }, it.context.asExecutor)
    it.invokeOnCancellation { cancel(true) }
}



