package net.novagamestudios.tools.data

import kotlin.reflect.KProperty

data class DataState<T>(
    val isLoading: Boolean = false,
    val error: Error? = null,
    val data: T? = null
) {
    val hasError get() = error != null
    val hasData get() = data != null

    val errorMessage get() = error?.message
    val exception get() = (error as? Error.Exception)?.exception

    sealed class Error {
        abstract val message: String?
        data class Message(
                override val message: String
        ) : Error()
        data class Exception(
                val exception: Throwable
        ) : Error() {
            override val message: String? get() = exception.message
        }
    }
    companion object {
        fun <T> empty(): DataState<T> = DataState()
        fun <T> loading(): DataState<T> = DataState(isLoading = true)
        fun <T> data(data: T): DataState<T> = DataState(data = data)
        fun <T> error(error: Error): DataState<T> = DataState(error = error)
        fun <T> error(message: String): DataState<T> = error(Error.Message(message))
        fun <T> error(exception: Throwable): DataState<T> = error(Error.Exception(exception))
    }

    fun then(next: DataState<T>): DataState<T> = next.copy()
    operator fun plus(next: DataState<T>) = then(next)

    operator fun getValue(thisRef: Any?, property: KProperty<*>) = data


//    operator fun plus(response: StoreResponse<T>?) : StoreState<T> = when(response) {
//        is StoreResponse.Loading -> copy(isLoading = true,
//                                         error = null)
//        is StoreResponse.Data    -> copy(isLoading = false,
//                                         data = response.value,
//                                         error = null)
//        is StoreResponse.Error   -> copy(isLoading = false,
//                                         error = Error(response))
//        else                     -> copy(isLoading = false)
//    }
}