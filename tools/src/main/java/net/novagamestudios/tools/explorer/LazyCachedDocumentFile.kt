package net.novagamestudios.tools.explorer

import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import net.novagamestudios.tools.LazyCached


class LazyCachedDocumentFile(
        val original: DocumentFile
) {

    var uri         : Uri?                by LazyCached { original.uri                  }; private set
    var name        : String?             by LazyCached { original.name                 }; private set
    var type        : String?             by LazyCached { original.type                 }; private set
    var parentFile  : DocumentFile?       by LazyCached { original.parentFile           }; private set
    var isDirector  : Boolean?            by LazyCached { original.isDirectory          }; private set
    var isFile      : Boolean?            by LazyCached { original.isFile               }; private set
    var isVirtual   : Boolean?            by LazyCached { original.isVirtual            }; private set
    var lastModified: Long?               by LazyCached { original.lastModified()       }; private set
    var length      : Long?               by LazyCached { original.length()             }; private set
    var canRead     : Boolean?            by LazyCached { original.canRead()            }; private set
    var canWrite    : Boolean?            by LazyCached { original.canWrite()           }; private set
    var exists      : Boolean?            by LazyCached { original.exists()             }; private set
    var listFiles   : List<DocumentFile>? by LazyCached { original.listFiles().toList() }; private set

    companion object {
        fun DocumentFile.lazyCached(): LazyCachedDocumentFile = LazyCachedDocumentFile(this)
    }

    fun invalidate() {
        uri          = null
        name         = null
        type         = null
        parentFile   = null
        isDirector   = null
        isFile       = null
        isVirtual    = null
        lastModified = null
        length       = null
        canRead      = null
        canWrite     = null
        exists       = null
        listFiles    = null
    }

    fun findFile(displayName: String): DocumentFile? = original.findFile(displayName)

    fun createFile(mimeType: String, displayName: String): DocumentFile? =
            original.createFile(mimeType, displayName).also { invalidate() }

    fun createDirectory(displayName: String): DocumentFile? =
            original.createDirectory(displayName).also { invalidate() }

    fun renameTo(displayName: String): Boolean = original.renameTo(displayName).also { invalidate() }

    fun delete(): Boolean = original.delete().also { invalidate() }


}

