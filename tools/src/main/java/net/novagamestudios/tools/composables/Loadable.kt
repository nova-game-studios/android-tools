package net.novagamestudios.tools.composables

import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun Loadable(
        isLoading: Boolean,
        modifier: Modifier = Modifier,
        content: @Composable () -> Unit
) {
    if (isLoading) {
        CircularProgressIndicator(modifier = modifier)
    } else {
        content()
    }
}