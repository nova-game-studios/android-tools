package net.novagamestudios.tools.collections

import net.novagamestudios.tools.collections.*
import kotlin.math.min



// region FUNCTIONS

fun <T, R> Iterator<T>.mapped(f: (T) -> R): Iterator<R> =
        MappedIterator(this, f)
fun <T, R> MutableIterator<T>.mappedMutable(f: (T) -> R): MutableIterator<R> =
        MappedMutableIterator(this, f)

fun <T, R> SortedIterator<T>.mapped(f: (T) -> R): SortedIterator<R> =
        MappedSortedIterator(this, f)
fun <T, R> MutableSortedIterator<T>.mappedMutable(f: (T) -> R): MutableSortedIterator<R> =
        MappedMutableSortedIterator(this, f)

fun <T, R> ListIterator<T>.mapped(f: (Int, T) -> R): ListIterator<R> =
        MappedListIterator(this, f)
fun <T, R> MutableListIterator<T>.mappedMutable(fOut: (Int, T) -> R, fIn: (R) -> T): MutableListIterator<R> =
        MappedMutableListIterator(this, fOut, fIn)

fun <T, R> List<T>.mapped(f: (Int, T) -> R): List<R> = MappedList(this, f)
fun <T, R> MutableList<T>.mappedMutable(fOut: (Int, T) -> R, fIn: (R) -> T): MutableList<R> = MappedMutableList(this, fOut, fIn)

fun <T, R> Set<T>.mapped(f: (T) -> R): Set<R> = MappedSet(this, f)
fun <T, R> MutableSet<T>.mappedMutable(fOut: (T) -> R, fIn: (R) -> T): MutableSet<R> = MappedMutableSet(this, fOut, fIn)

fun <T, R> SortedSet<T>.mapped(f: (T) -> R): SortedSet<R> = MappedSortedSet(this, f)
fun <T, R> MutableSortedSet<T>.mappedMutable(fOut: (T) -> R, fIn: (R) -> T): MutableSortedSet<R> = MappedMutableSortedSet(this, fOut, fIn)

fun <T> Iterator<T>.asUnsupportedMutable(): MutableIterator<T> = object : MutableIterator<T> {
    override fun hasNext(): Boolean = this@asUnsupportedMutable.hasNext()
    override fun next(): T = this@asUnsupportedMutable.next()
    override fun remove() = throw UnsupportedOperationException()
}
fun <T> SortedIterator<T>.asUnsupportedMutable(): MutableSortedIterator<T> = object :
    MutableSortedIterator<T> {
    override fun hasNext(): Boolean = this@asUnsupportedMutable.hasNext()
    override fun hasPrevious(): Boolean = this@asUnsupportedMutable.hasPrevious()
    override fun next(): T = this@asUnsupportedMutable.next()
    override fun previous(): T  = this@asUnsupportedMutable.previous()
    override fun remove() = throw UnsupportedOperationException()
}
fun <T> ListIterator<T>.asUnsupportedMutable(): MutableListIterator<T> = object : MutableListIterator<T> {
    override fun hasNext(): Boolean = this@asUnsupportedMutable.hasNext()
    override fun hasPrevious(): Boolean = this@asUnsupportedMutable.hasPrevious()
    override fun next(): T = this@asUnsupportedMutable.next()
    override fun previous(): T  = this@asUnsupportedMutable.previous()
    override fun nextIndex(): Int = this@asUnsupportedMutable.nextIndex()
    override fun previousIndex(): Int  = this@asUnsupportedMutable.previousIndex()
    override fun add(element: T) = throw UnsupportedOperationException()
    override fun remove() = throw UnsupportedOperationException()
    override fun set(element: T) = throw UnsupportedOperationException()
}

fun <T> List<T>.range(range: IntRange): List<T> {
    val indices = range.toList()
    return mapIndices({ min(size, indices.size) }, { indices[it] })
}
fun <T> MutableList<T>.rangeMutable(range: IntRange): MutableList<T> {
    val indices = range.toList()
    return mapIndicesMutable({ min(size, indices.size) }, { indices[it] })
}

fun <T> List<T>.mapIndices(usedSize: () -> Int, f: (Int) -> Int): List<T> =
        IndexMappedList(this, usedSize, f)
fun <T> MutableList<T>.mapIndicesMutable(usedSize: () -> Int, f: (Int) -> Int): MutableList<T> =
        MutableIndexMappedList(this, usedSize, f)


fun <K, V> MapEntry(key: K, value: V) = object : MapEntry<K, V> {
    override val key: K = key
    override val value: V = value
}
fun <K, V> SortedMapEntry(key: K, value: V) = object : SortedMapEntry<K, V> {
    override val key: K = key
    override val value: V = value
}

fun <K, V> MutableMapEntry(key: K, value: V) = object : MutableMapEntry<K, V> {
    override val key: K = key
    override var value: V = value
    override fun setValue(newValue: V): V = this.value.also { this.value = newValue }
}
fun <K, V> MutableSortedMapEntry(key: K, value: V) = object : MutableSortedMapEntry<K, V> {
    override val key: K = key
    override var value: V = value
    override fun setValue(newValue: V): V = this.value.also { this.value = newValue }
}


fun <T> List<T>.asSet(): Set<T> = object : AbstractSet<T>() {
    override val size: Int get() = this@asSet.size
    override fun iterator(): Iterator<T> = this@asSet.iterator()
}
fun <K, V> Map<K, V>.asSet(): Set<MapEntry<K, V>> = object : AbstractSet<MapEntry<K, V>>() {
    override val size: Int get() = this@asSet.size
    override fun iterator(): Iterator<MapEntry<K, V>> = this@asSet.iterator()
}
fun <T> MutableList<T>.asMutableSet(): MutableSet<T> = object : AbstractMutableSet<T>() {
    override val size: Int get() = this@asMutableSet.size
    override fun add(element: T): Boolean = this@asMutableSet.add(element)
    override fun iterator(): MutableIterator<T> = this@asMutableSet.iterator()
}
fun <K, V> MutableMap<K, V>.asMutableSet(): MutableSet<MutableMapEntry<K, V>> = object : AbstractMutableSet<MutableMapEntry<K, V>>() {
    override val size: Int get() = this@asMutableSet.size
    override fun add(element: MutableMapEntry<K, V>): Boolean = this@asMutableSet.add(element)
    override fun iterator(): MutableIterator<MutableMapEntry<K, V>> = this@asMutableSet.iterator()
}





fun <T> ListIterator<T>.asSortedIterator(): SortedIterator<T> =
        ListIteratorAsSortedIterator(this)
fun <T> MutableListIterator<T>.asMutableSortedIterator(): MutableSortedIterator<T> =
        MutableListIteratorAsMutableSortedIterator(this)

fun <T> List<T>.asSortedIterable(): SortedIterable<T> =
        ListAsSortedIterable(this)
fun <T> MutableList<T>.asMutableSortedIterable(): MutableSortedIterable<T> =
        MutableListAsMutableSortedIterable(this)

fun <T> List<T>.asSortedCollection(): SortedCollection<T> =
        ListAsSortedCollection(this)
fun <T> MutableList<T>.asMutableSortedCollection(): MutableSortedCollection<T> =
        MutableListAsMutableSortedCollection(this)

fun <T> List<T>.asSortedSet(): SortedSet<IndexedValue<T>> =
        ListAsSortedSet(this)
fun <T> MutableList<T>.asMutableSortedSet(): MutableSortedSet<IndexedValue<T>> =
        MutableListAsMutableSortedSet(this)

fun <T> List<T>.asSparseList(): SparseList<T> =
        ListAsSparseList(this)
fun <T> MutableList<T>.asMutableSparseList(): MutableSparseList<T> =
        MutableListAsMutableSparseList(this)

// endregion


// region CLASSES

typealias MapEntry<K, V> = Map.Entry<K, V>
typealias MutableMapEntry<K, V> = MutableMap.MutableEntry<K, V>

open class MappedIterator<T, R>(
        protected open val self: Iterator<T>,
        protected val f: (T) -> R
) : Iterator<R> {
    override fun hasNext(): Boolean = self.hasNext()
    override fun next(): R = f(self.next())
}
open class MappedMutableIterator<T, R>(
        override val self: MutableIterator<T>,
        f: (T) -> R
) : MappedIterator<T, R>(self, f), MutableIterator<R> {
    override fun remove() = self.remove()
}
private open class MappedSortedIterator<T, R>(
    override val self: SortedIterator<T>,
    f: (T) -> R
) : MappedIterator<T, R>(self, f), SortedIterator<R> {
    override fun hasPrevious(): Boolean = self.hasNext()
    override fun previous(): R = f(self.next())
}
private open class MappedMutableSortedIterator<T, R>(
    override val self: MutableSortedIterator<T>,
    f: (T) -> R
) : MappedSortedIterator<T, R>(self, f), MutableSortedIterator<R> {
    override fun remove() = self.remove()
}

private open class MappedListIterator<T, R>(
        protected open val self: ListIterator<T>,
        protected val f: (Int, T) -> R
) : ListIterator<R> {
    override fun hasNext(): Boolean = self.hasNext()
    override fun next(): R = f(nextIndex(), self.next())
    override fun nextIndex(): Int = self.nextIndex()
    override fun hasPrevious(): Boolean = self.hasPrevious()
    override fun previous(): R = f(previousIndex(), self.previous())
    override fun previousIndex(): Int = self.previousIndex()
}
private open class MappedMutableListIterator<T, R>(
        override val self: MutableListIterator<T>,
        fOut: (Int, T) -> R,
        protected val fIn: (R) -> T
) : MappedListIterator<T, R>(self, fOut), MutableListIterator<R> {
    override fun remove() = self.remove()
    override fun add(element: R) = self.add(fIn(element))
    override fun set(element: R) = self.set(fIn(element))
}

private open class MappedSet<T, R>(
        protected open val self: Set<T>,
        protected val f: (T) -> R
) : AbstractSet<R>() {
    override val size: Int get() = self.size
    override fun iterator(): Iterator<R> = self.iterator().mapped(f)
}
private open class MappedSortedSet<T, R>(
    override val self: SortedSet<T>,
    f: (T) -> R
) : MappedSet<T, R>(self, f), SortedSet<R> {
    override fun iterator(): SortedIterator<R> = self.iterator().mapped(f)
    override val first: R get() = f(self.first)
    override val last: R get() = f(self.last)
}

private open class MappedMutableSet<T, R>(
        protected open val self: MutableSet<T>,
        protected val fOut: (T) -> R,
        protected val fIn: (R) -> T
) : AbstractMutableSet<R>() {
    override val size: Int get() = self.size
    override fun iterator(): MutableIterator<R> = self.iterator().mappedMutable(fOut)
    override fun add(element: R): Boolean = self.add(fIn(element))
}
private open class MappedMutableSortedSet<T, R>(
    override val self: MutableSortedSet<T>,
    fOut: (T) -> R,
    fIn: (R) -> T
) : MappedMutableSet<T, R>(self, fOut, fIn), MutableSortedSet<R> {
    override fun iterator(): MutableSortedIterator<R> = self.iterator().mappedMutable(fOut)
    override val first: R get() = fOut(self.first)
    override val last: R get() = fOut(self.last)
}

private open class MappedList<T, R>(
        protected open val self: List<T>,
        protected val f: (Int, T) -> R
) : AbstractList<R>() {
    override val size: Int get() = self.size
    override fun get(index: Int): R = f(index, self[index])
}
private open class MappedMutableList<T, R>(
        protected open val self: MutableList<T>,
        protected val fOut: (Int, T) -> R,
        protected val fIn: (R) -> T
) : AbstractMutableList<R>() {
    override val size: Int get() = self.size
    override fun get(index: Int): R = fOut(index, self[index])
    override fun add(index: Int, element: R) = self.add(index, fIn(element))
    override fun set(index: Int, element: R): R = fOut(index, self.set(index, fIn(element)))
    override fun removeAt(index: Int): R = fOut(index, self.removeAt(index))
}

private open class IndexMappedList<T>(
        protected open val self: List<T>,
        protected val usedSize: () -> Int,
        protected val f: (Int) -> Int
) : AbstractList<T>() {
    protected fun Int.map(): Int = if (this >= usedSize()) throw IndexOutOfBoundsException() else f(this)
    override val size: Int get() = usedSize()
    override fun get(index: Int): T = self[index.map()]
}
private open class MutableIndexMappedList<T>(
        protected open val self: MutableList<T>,
        protected val usedSize: () -> Int,
        protected val f: (Int) -> Int
) : AbstractMutableList<T>() {
    protected fun Int.map(): Int = if (this >= usedSize()) throw IndexOutOfBoundsException() else f(this)
    override val size: Int get() = usedSize()
    override fun get(index: Int): T = self[index.map()]
    override fun add(index: Int, element: T) = self.add(index.map(), element)
    override fun removeAt(index: Int): T = self.removeAt(index.map())
    override fun set(index: Int, element: T): T = self.set(index.map(), element)
}



private open class ListIteratorAsSortedIterator<T>(
        protected open val self: ListIterator<T>
) : SortedIterator<T> {
    override fun hasPrevious(): Boolean = self.hasPrevious()
    override fun previous(): T = self.previous()
    override fun hasNext(): Boolean = self.hasNext()
    override fun next(): T = self.next()
}
private open class MutableListIteratorAsMutableSortedIterator<T>(
        override val self: MutableListIterator<T>
) : ListIteratorAsSortedIterator<T>(self), MutableSortedIterator<T> {
    override fun remove() = self.remove()
}

private open class ListAsSortedIterable<T>(
        protected open val self: List<T>
) : SortedIterable<T> {
    override fun iterator(): SortedIterator<T> = self.listIterator().asSortedIterator()
}
private open class MutableListAsMutableSortedIterable<T>(
        override val self: MutableList<T>
) : ListAsSortedIterable<T>(self), MutableSortedIterable<T> {
    override fun iterator(): MutableSortedIterator<T> = self.listIterator().asMutableSortedIterator()
}

private open class ListAsSortedCollection<T>(
        override val self: List<T>
) : ListAsSortedIterable<T>(self), SortedCollection<T> {
    override val first: T get() = self.first()
    override val last: T get() = self.last()
    override val size: Int get() = self.size
    override fun contains(element: T): Boolean = self.contains(element)
    override fun containsAll(elements: Collection<T>): Boolean = self.containsAll(elements)
    override fun isEmpty(): Boolean = self.isEmpty()
}
private open class MutableListAsMutableSortedCollection<T>(
        override val self: MutableList<T>
) : ListAsSortedCollection<T>(self), MutableSortedCollection<T> {
    override fun iterator(): MutableSortedIterator<T> = self.listIterator().asMutableSortedIterator()
    override fun add(element: T): Boolean = self.add(element)
    override fun addAll(elements: Collection<T>): Boolean = self.addAll(elements)
    override fun clear() = self.clear()
    override fun remove(element: T): Boolean = self.remove(element)
    override fun removeAll(elements: Collection<T>): Boolean = self.removeAll(elements)
    override fun retainAll(elements: Collection<T>): Boolean = self.retainAll(elements)
}

private open class ListAsSortedSet<T>(
        protected open val list: List<T>
) : ListAsSortedCollection<IndexedValue<T>>(
        list.mapped { i, v -> IndexedValue(i, v) }
), SortedSet<IndexedValue<T>>
private open class MutableListAsMutableSortedSet<T>(
        protected open val list: MutableList<T>
) : MutableListAsMutableSortedCollection<IndexedValue<T>>(
        list.mappedMutable({ i, v -> IndexedValue(i, v) }, { v -> v.value })
), MutableSortedSet<IndexedValue<T>>



private open class ListAsSparseList<T>(
        protected open val self: List<T>
) : AbstractSortedMap<Int, T>(), SparseList<T> {
    override val first: Int get() = 0
    override val last: Int get() = size - 1
    override val entries: SortedSet<SortedMapEntry<Int, T>>
        get() = self.asSortedSet().mapped { (i, v) -> SortedMapEntry(i, v) }
    override fun containsKey(key: Int): Boolean = key in first..last
    override fun get(key: Int): T? = self.getOrNull(key)

}
private open class MutableListAsMutableSparseList<T>(
        protected open val self: MutableList<T>
) : AbstractMutableSortedMap<Int, T>(), MutableSparseList<T> {
    override val first: Int get() = 0
    override val last: Int get() = size - 1
    override val entries: MutableSortedSet<MutableSortedMapEntry<Int, T>>
        get() = self.asMutableSortedSet().mappedMutable(
                { (i, v) -> MutableSortedMapEntry(i, v) },
                { (i, v) -> IndexedValue(i, v) })
    override fun containsKey(key: Int): Boolean = key in first..last
    override fun get(key: Int): T? = self.getOrNull(key)
    override fun put(key: Int, value: T): T? = self.set(key, value)
}


// endregion











