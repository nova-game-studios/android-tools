package net.novagamestudios.tools.collections

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface FlowingMap<K, V : Any> : Map<K, V> {
    fun getFlow(key: K): StateFlow<V?>
    fun tryGetFlow(key: K): StateFlow<V?>?
}

open class MutableFlowingMap<K, V : Any> (
    private val map: MutableMap<K, MutableStateFlow<V?>>
) : AbstractMutableMap<K, V>(), FlowingMap<K, V> {
    override val entries: MutableSet<MutableMapEntry<K, V>> = object : AbstractMutableSet<MutableMapEntry<K, V>>() {
        override val size: Int get() = map.size
        override fun add(element: MutableMapEntry<K, V>): Boolean = put(element.key, element.value) != element.value
        override fun iterator(): MutableIterator<MutableMapEntry<K, V>> = object : MutableIterator<MutableMapEntry<K, V>> {
            val iterator = map.entries
                    .filter { (_, f) -> f.value != null }
                    .iterator()
            var last: MutableMapEntry<K, MutableStateFlow<V?>>? = null

            override fun hasNext(): Boolean = iterator.hasNext()
            override fun next(): MutableMapEntry<K, V> = iterator.next().also { last = it }.let {
                object : MutableMapEntry<K, V> {
                    override val key: K get() = it.key
                    override val value: V get() = it.value.value!!
                    override fun setValue(newValue: V): V = value.also { _ -> it.value.value = newValue }
                }
            }

            override fun remove() {
                last?.value?.value = null
            }
        }
    }

    override fun getFlow(key: K) = map.getOrPut(key) { MutableStateFlow(null) }
    override fun tryGetFlow(key: K) = map.getOrDefault(key, null)

    override fun put(key: K, value: V): V? {
        return getFlow(key).let {
            val old = it.value
            it.value = value
            old
        }
    }

    override fun remove(key: K): V? {
        return tryGetFlow(key)?.let {
            val old = it.value
            it.value = null
            old
        }
    }
}