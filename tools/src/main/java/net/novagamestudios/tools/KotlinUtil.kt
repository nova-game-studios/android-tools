package net.novagamestudios.tools

import kotlinx.coroutines.*
import java.lang.Runnable
import java.text.DecimalFormat
import java.util.*
import kotlin.math.absoluteValue
import kotlin.math.log10
import kotlin.properties.ObservableProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.text.repeat as nativeRepeat


/*
                | uses it | uses this |
 returns this   | also    | apply     |
 returns result | let     | run       |
 */



inline fun <T> T.alsoIf(p: (T) -> Boolean, block: (T) -> Unit): T = if (p(this)) also(block) else this
inline fun <T, R> T.letIf(p: (T) -> Boolean, block: (T) -> R): R where T : R = if (p(this)) block(this) else this
inline fun <T> T.applyIf(p: (T) -> Boolean, block: T.() -> Unit): T = alsoIf(p, block)
inline fun <T, R> T.runIf(p: (T) -> Boolean, block: T.() -> R): R where T : R = letIf(p, block)

inline fun <T> T.alsoIf(p: Boolean, block: (T) -> Unit): T = alsoIf({ p }, block)
inline fun <T, R> T.letIf(p: Boolean, block: (T) -> R): R where T : R = letIf({ p }, block)
inline fun <T> T.applyIf(p: Boolean, block: T.() -> Unit): T = applyIf({ p }, block)
inline fun <T, R> T.runIf(p: Boolean, block: T.() -> R): R where T : R = runIf({ p }, block)

inline fun Boolean.alsoIfTrue(f: () -> Unit): Boolean = alsoIf(this) { f() }
inline fun Boolean.alsoIfFalse(f: () -> Unit): Boolean = alsoIf(!this) { f() }
inline fun <R> Boolean.letIfTrue(f: () -> R?): R? = this.let { if (it) f() else null }
inline fun <R> Boolean.letIfFalse(f: () -> R?): R? = this.let { if (!it) f() else null }

inline fun Boolean.applyIfTrue(f: () -> Unit): Boolean = alsoIfTrue(f)
inline fun Boolean.applyIfFalse(f: () -> Unit): Boolean = alsoIfFalse(f)
inline fun <R> Boolean.runIfTrue(f: () -> R?): R? = letIfTrue(f)
inline fun <R> Boolean.runIfFalse(f: () -> R?): R? = letIfFalse(f)


inline fun <T> T?.alsoIfNull(f: () -> Unit): T? = alsoIf(this == null) { f() }
inline fun <T> T?.letIfNull(f: () -> T): T = this ?: f()

inline fun <T> T?.applyIfNull(f: () -> Unit): T? = alsoIfNull(f)
inline fun <T> T?.runIfNull(f: () -> T): T = letIfNull(f)

inline fun <T> T?.ifNull(f: () -> T): T = letIfNull(f)





//inline fun <T, R> T.ifThen(p: (T) -> Boolean, f: (T) -> R): R where T : R = if (p(this)) f(this) else this

inline fun <T : CharSequence?> T?.ifEmpty(f: () -> T): T = if (this == null || isEmpty()) f() else this
inline fun <T : Collection<*>?> T?.ifEmpty(f: () -> T): T = if (this == null || isEmpty()) f() else this






inline fun <T, R> T.tryCatchAll(handler: T.(Throwable) -> R, block: T.() -> R): R  = try {
    block()
} catch (t: Throwable) {
    handler(t)
}

inline fun <T, R> T.tryCatchAll(block: T.() -> R?): R? = tryCatchAll({ null }, block)


inline fun <R> tryCatchAll(crossinline handler: (Throwable) -> R, crossinline block: () -> R): R =
        Unit.tryCatchAll({ handler(it) }) { block() }

inline fun <R> tryCatchAll(crossinline block: () -> R?): R? =
        Unit.tryCatchAll { block() }


inline fun <T> T.throwIf(p: (T) -> Boolean, e: (T) -> Throwable): T = when {
    p(this) -> throw e(this)
    else    -> this
}
inline fun <T> T?.throwIfNull(e: () -> Throwable): T = this ?: throw e()

inline fun <T> T.throwIfNot(f: (T) -> Boolean, e: (T) -> Throwable) = throwIf({ !f(it) }, e)
inline fun <T> T.require(f: (T) -> Boolean, e: (T) -> Throwable) = throwIfNot(f, e)
inline fun <T> T.requireIs(v: T, e: (T) -> Throwable) = require({ it == v }, e)
inline fun <T, reified C> T.requireIs(e: (T) -> Throwable) = require({ it is C }, e)
inline fun <T> T.requireInstance(c: KClass<*>, e: (T) -> Throwable) = require({ c.isInstance(it) }, e)
inline fun <T> T.requireIn(c: Collection<T>, e: (T) -> Throwable) = require({ c.contains(it) }, e)
inline fun <T> T.requireNotIn(c: Collection<T>, e: (T) -> Throwable) = require({ !c.contains(it) }, e)
inline fun <T : Comparable<T>> T.requireIn(r: ClosedRange<T>, e: (T) -> Throwable) = require({ r.contains(it) }, e)
inline fun <T : Comparable<T>> T.requireNotIn(r: ClosedRange<T>, e: (T) -> Throwable) = require({ !r.contains(it) }, e)
inline fun <T> T.requireOneOf(vararg vs: T, e: (T) -> Throwable) = require({ vs.contains(it) }, e)
inline fun <T> T.requireNoneOf(vararg vs: T, e: (T) -> Throwable) = require({ !vs.contains(it) }, e)
inline fun <T> T?.requireNotNull(e: (T?) -> Throwable) = require({ it != null }, e)!!




infix fun <T> T.instanceof(c: KClass<*>): Boolean = c.isInstance(this)



fun <A, B, R> Pair<A, B>.sequence(): Sequence<R> where A : R, B : R = sequenceOf(first, second)

fun <A, B, R> Pair<A?, B?>.notNull(): R? where A : R, B : R = sequence().findNotNull()




infix fun Int.to(end: Int): IntRange = IntRange(this, end)

infix fun <A, B> A.with(second: B): Pair<A, B> = Pair(this, second)

infix fun <A, B, C> Pair<A, B>.and(third: C): Triple<A, B, C> = Triple(first, second, third)
infix fun <A, B, C> A.and(pair: Pair<B, C>): Triple<A, B, C> = Triple(this, pair.first, pair.second)


infix fun CharSequence.repeat(n: Int) = nativeRepeat(n)


fun Number.format(format: String): String = DecimalFormat(format).format(toDouble())



fun lt(v: Int): IntRange = Int.MIN_VALUE until v
fun gt(v: Int): IntRange = (v + 1)..Int.MAX_VALUE
fun lte(v: Int): IntRange = Int.MIN_VALUE..v
fun gte(v: Int): IntRange = v..Int.MAX_VALUE

fun lt(v: Long): LongRange = Long.MIN_VALUE until v
fun gt(v: Long): LongRange = (v + 1)..Long.MAX_VALUE
fun lte(v: Long): LongRange = Long.MIN_VALUE..v
fun gte(v: Long): LongRange = v..Long.MAX_VALUE


fun lte(v: Float): ClosedFloatingPointRange<Float> = Float.NEGATIVE_INFINITY..v
fun gte(v: Float): ClosedFloatingPointRange<Float> = v..Float.POSITIVE_INFINITY

fun lte(v: Double): ClosedFloatingPointRange<Double> = Double.NEGATIVE_INFINITY..v
fun gte(v: Double): ClosedFloatingPointRange<Double> = v..Double.POSITIVE_INFINITY


fun <T : Comparable<T>> T.isBetween(min: T, max: T) = min < this && this < max
fun <T : Comparable<T>> T.isIn(min: T, max: T) = this in min..max

fun <T : Comparable<T>> T.min(min: T) = coerceAtLeast(min)
fun <T : Comparable<T>> T.max(max: T) = coerceAtMost(max)
/** if max < min then returned == max */
fun <T : Comparable<T>> T.clamp(min: T, max: T) = min(min).max(max)
fun Int.clampIndex(collection: Collection<*>) = clamp(0, collection.size)

val IntProgression.size: Int get() = if (step == 0) 0 else ((last - first + 1) / step.toFloat()).absoluteValue.ceilToInt()

val IntRange.Companion.ALL: IntRange by lazy { Int.MIN_VALUE to Int.MAX_VALUE }




fun Float.ceil() = kotlin.math.ceil(this)
fun Double.ceil() = kotlin.math.ceil(this)
fun Float.floor() = kotlin.math.floor(this)
fun Double.floor() = kotlin.math.floor(this)
fun Float.ceilToInt() = ceil().toInt()
fun Double.ceilToInt() = ceil().toInt()
fun Float.floorToInt() = floor().toInt()
fun Double.floorToInt() = floor().toInt()
fun Float.ceilToLong() = ceil().toLong()
fun Double.ceilToLong() = ceil().toLong()
fun Float.floorToLong() = floor().toLong()
fun Double.floorToLong() = floor().toLong()


fun Float.lerpInv(min: Float, max: Float): Float = (this - min) / (max - min)

fun Float.Companion.lerp(min: Float, max: Float, t: Float): Float = t * (max - min) + min

fun Float.map(
        fromMin: Float,
        fromMax: Float,
        toMin: Float,
        toMax: Float
): Float = Float.lerp(toMin, toMax, lerpInv(fromMin, fromMax))

fun Float.mapClamped(
        fromMin: Float,
        fromMax: Float,
        toMin: Float,
        toMax: Float
): Float = clamp(fromMin, fromMax).map(fromMin, fromMax, toMin, toMax)




val Int.abs get() = absoluteValue
val Long.abs get() = absoluteValue
val Float.abs get() = absoluteValue
val Double.abs get() = absoluteValue

fun <T> Sequence<T>.findNotNull() = find { it != null }
fun <T> Iterable<T>.findNotNull() = find { it != null }


//fun <T> it(): (T) -> T = { it }
//fun <T> self(): T.() -> T = { this }


inline fun repeat(action: (Long) -> Unit) {
    var i = 0L
    while (true) action(i++)
}



val noop: Unit = Unit

fun Any?.void(): Unit { }

val <T> T.exhaustive get() = this

operator fun Runnable.invoke() = run()


@Suppress("DeferredIsResult")
fun <T> T.completedDeferred(): Deferred<T> = CompletableDeferred(value = this)




@ExperimentalTime
fun Duration.formatLikeYT(): String {
    if (isInfinite()) return toString()
    val components = listOf(
            1 with false,
            24 with false,
            60 with true,
            60 with true
    )

    var divisor = components.fold(1, { i, (mod, _) -> i * mod })
    var total = inWholeSeconds.abs.toInt()

    val strings = mutableListOf<String>()
    loop@ for ((mod, required) in components) {
        divisor /= mod
        val value = total / divisor
        total %= divisor
        strings += when {
            value == 0 && !required && strings.isEmpty() -> continue@loop
            strings.isEmpty() -> value.toString()
            else -> value.format("0" repeat (log10(mod.toDouble()).toInt() + 1))
        }
    }
    return (if (isNegative()) "-" else "") + strings.joinToString(":") { it }
}




//operator fun <R> (() -> R)?.invoke(): R? = this?.invoke()
//operator fun <T1, R> ((T1) -> R)?.invoke(t1: T1): R? = this?.invoke(t1)
//operator fun <T1, T2, R> ((T1, T2) -> R)?.invoke(t1: T1, t2: T2): R? = this?.invoke(t1, t2)
//operator fun <T1, T2, T3, R> ((T1, T2, T3) -> R)?.invoke(t1: T1, t2: T2, t3: T3): R? = this?.invoke(t1, t2, t3)
//operator fun <T1, T2, T3, T4, R> ((T1, T2, T3, T4) -> R)?.invoke(t1: T1, t2: T2, t3: T3, t4: T4): R? = this?.invoke(t1, t2, t3, t4)




class LazyCached<T>(
        private val generator: () -> T
) {
    private var value: T? = null
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T =
            value.letIfNull(generator).also { value = it }
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?): T? =
            value.also { this.value = it }
}





fun <V> whenChanged(
        initialValue: V,
        onlyIf: ((V) -> Boolean)? = null,
        onInit: Boolean = false,
        f: (V) -> Unit
): ObservableProperty<V> {
    val checkedF = { v: V -> if (onlyIf == null || onlyIf(v)) f(v) }
    if (onInit) checkedF(initialValue)
    return object : ObservableProperty<V>(initialValue) {
        override fun afterChange(property: KProperty<*>, oldValue: V, newValue: V) {
            super.afterChange(property, oldValue, newValue)
            checkedF(newValue)
        }
    }
}







class CompositeException(
        val exceptions: List<Throwable>
) : Throwable() {
    override val message: String get() = exceptions.map { it.message }.joinToString("\n")
}



// region EITHER

typealias Either<A, B> = Pair<A?, B?>
@Suppress("FunctionName") fun <A, B> EitherFirst(first: A): Either<A, B> = first with null
@Suppress("FunctionName") fun <A, B> EitherSecond(second: B): Either<A, B> = null with second
val Either<*, *>.isFirst get() = first != null
val Either<*, *>.isSecond get() = second != null

// endregion




// region ENUM_SET

infix fun <E : Enum<E>> E.or(e: E) = enumSetOf(this, e)
infix fun <E : Enum<E>> E.or(set: EnumSet<E>) = set or this
infix fun <E : Enum<E>> EnumSet<E>.or(e: E) = copy().apply { add(e) }
infix fun <E : Enum<E>> EnumSet<E>.or(set: EnumSet<E>) = copy().apply { addAll(set) }

infix fun <E : Enum<E>> E.and(e: E) = enumSetOf(this).alsoIf(this != e) { it.remove(this) }
infix fun <E : Enum<E>> E.and(set: EnumSet<E>) = enumSetOf(this).alsoIf(this !in set) { it.remove(this) }
infix fun <E : Enum<E>> EnumSet<E>.and(e: E) = e and this
infix fun <E : Enum<E>> EnumSet<E>.and(set: EnumSet<E>) = copy().apply { retainAll(set) }

fun <E : Enum<E>> E.not() = complement()
fun <E : Enum<E>> EnumSet<E>.not() = complement()
fun <E : Enum<E>> E.complement() = enumSetOf(this).complement()
fun <E : Enum<E>> EnumSet<E>.complement(): EnumSet<E> = EnumSet.complementOf(this)

inline fun <reified E : Enum<E>> emptyEnumSet(): EnumSet<E> = EnumSet.noneOf(E::class.java)
inline fun <reified E : Enum<E>> enumSetOf() = emptyEnumSet<E>()
fun <E : Enum<E>> enumSetOf(e: E): EnumSet<E>  = EnumSet.of(e)
fun <E : Enum<E>> enumSetOf(e: E, vararg es: E): EnumSet<E>  = EnumSet.of(e, *es)
inline fun <reified E : Enum<E>> enumSetOf(es: Iterable<E>) = enumSetOf<E>().apply { addAll(es) }
fun <E : Enum<E>> enumSetOf(e: E, es: Iterable<E>) = enumSetOf(e).apply { addAll(es) }
inline fun <reified E : Enum<E>> enumSetOf(es: Sequence<E>) = enumSetOf<E>().apply { addAll(es) }
fun <E : Enum<E>> enumSetOf(e: E, es: Sequence<E>) = enumSetOf(e).apply { addAll(es) }
inline fun <reified E : Enum<E>> Iterable<E>.toEnumSet() = enumSetOf(this)
inline fun <reified E : Enum<E>> Sequence<E>.toEnumSet() = enumSetOf(this)
fun <E : Enum<E>> EnumSet<E>.copy(): EnumSet<E> = EnumSet.copyOf(this)

operator fun <E : Enum<E>> EnumSet<E>.contains(es: Collection<E>): Boolean = containsAll(es)

// endregion





