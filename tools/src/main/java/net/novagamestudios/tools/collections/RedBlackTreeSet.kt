package net.novagamestudios.tools.collections

import net.novagamestudios.tools.throwIfNull

class RedBlackTreeSet<T : Comparable<T>> : AbstractMutableSortedSet<T>() {
    private var root: Node<T>? = null

    override var size: Int = 0
        private set

    private var firstNode: Node<T>? = null
    private var lastNode: Node<T>? = null

    override val first: T get() = firstNode.throwIfNull { NoSuchElementException() }.value
    override val last: T get() = lastNode.throwIfNull { NoSuchElementException() }.value



    override fun add(element: T): Boolean {
        var parent: Node<T>? = null
        var current: Node<T>? = root

        while (current != null) {
            parent = current
            current = when {
                element < current.value -> current.left
                element > current.value -> current.right
                else                    -> return false
            }
        }

        if (parent == null) {
            root = Node(element).apply {
                isBlack = true
            }
            firstNode = root
            lastNode = root
            size = 1
            return true
        }

        Node(element).also {
            size++
            it.parent = parent
            if (element < parent.value) {
                parent.left = it
                if (element < firstNode!!.value) firstNode = it
            } else {
                parent.right = it
                if (element > lastNode!!.value) lastNode = it
            }
            fixInsertViolations(it)
        }
        return true
    }

    override fun contains(element: T): Boolean = find(element) != null

    fun find(element: T): T? = findNode(element)?.value

    override fun remove(element: T): Boolean {
        deleteNode(findNode(element) ?: return false)
        return true
    }


    override fun clear() {
        root = null
        firstNode = null
        lastNode = null
        size = 0
    }

    private fun fixInsertViolations(node: Node<T>?) {
        var uncle: Node<T>?
        var current: Node<T>? = node

        while (current?.parent?.isBlack == false) {
            if (current.parent == current.parent?.parent?.left) {
                uncle = current.parent?.parent?.right

                when {
                    uncle?.isBlack == false -> {
                        current.parent?.isBlack = true
                        uncle.isBlack = true
                        current.parent?.parent?.isBlack = false
                        current = current.parent?.parent
                    }
                    current == current.parent?.right -> {
                        current = current.parent
                        if (current!!.parent?.parent == null) root = current.parent
                        current.rotateLeft()
                    }
                    current == current.parent?.left  -> {
                        if (current.parent?.parent?.parent == null) root = current.parent
                        current.parent?.parent?.rotateRight()
                    }
                }
            } else {
                uncle = current.parent?.parent?.left

                when {
                    uncle?.isBlack == false -> {
                        current.parent?.isBlack = true
                        uncle.isBlack = true
                        current.parent?.parent?.isBlack = false
                        current = current.parent?.parent
                    }
                    current == current.parent?.left  -> {
                        current = current.parent
                        if (current!!.parent?.parent == null) root = current.parent
                        current.rotateRight()
                    }
                    current == current.parent?.right -> {
                        if (current.parent?.parent?.parent == null) root = current.parent
                        current.parent?.parent?.rotateLeft()
                    }
                }
            }
        }
        root?.isBlack = true
    }

    private fun findNode(value: T): Node<T>? {
        var current = root

        while (current != null) {
            if (value == current.value) return current
            current = if (value < current.value) current.left else current.right
        }
        return null
    }

    private fun deleteNode(node: Node<T>) {
        size--
        when {
            (node.right != null && node.left != null) -> {
                val successor = node.successor()
                node.value = successor!!.value
                if (successor == lastNode) lastNode = node
                deleteNode(successor)
                return
            }
            (node == root && node.isLeaf()) -> {
                clear()
                return
            }
            (!node.isBlack && node.isLeaf()) -> { /* noop */ }
            (node.isBlack && node.left != null && !node.left!!.isBlack) -> {
                if (node.left!! == firstNode) firstNode = node
                node.value = node.left!!.value
                node.left = null
                return
            }
            (node.isBlack && node.right != null && !node.right!!.isBlack) -> {
                if (node.right!! == lastNode) lastNode = node
                node.value = node.right!!.value
                node.right = null
                return
            }
            else -> deleteCase1(node)
        }

        if (node == node.parent!!.left) node.parent!!.left = null
        else node.parent!!.right = null

        firstNode = root.min()
        lastNode = root.max()
    }

    private fun deleteCase1(node: Node<T>) {
        if (node.parent != null) deleteCase2(node)
    }

    private fun deleteCase2(node: Node<T>) {
        val brother = node.brother()

        if (!brother!!.isBlack) {
            if (node == node.parent!!.left)
                node.parent!!.rotateLeft()
            else if (node == node.parent!!.right)
                node.parent!!.rotateRight()

            if (root == node.parent)
                root = node.parent!!.parent
        }
        deleteCase3(node)
    }

    private fun deleteCase3(node: Node<T>) {
        val brother = node.brother()

        val a: Boolean = brother!!.left == null || brother.left!!.isBlack
        val b: Boolean = brother.right == null || brother.right!!.isBlack

        if (a && b && brother.isBlack && node.parent!!.isBlack) {
            brother.isBlack = false
            deleteCase1(node.parent!!)
        } else
            deleteCase4(node)
    }

    private fun deleteCase4(node: Node<T>) {
        val brother = node.brother()

        val a: Boolean = brother!!.left == null || brother.left!!.isBlack
        val b: Boolean = brother.right == null || brother.right!!.isBlack

        if (a && b && brother.isBlack && !node.parent!!.isBlack) {
            brother.isBlack = false
            node.parent!!.isBlack = true
        } else
            deleteCase5(node)
    }

    private fun deleteCase5(node: Node<T>) {
        val brother = node.brother()

        val a: Boolean = brother!!.left == null || brother.left!!.isBlack
        val b: Boolean = brother.right == null || brother.right!!.isBlack

        if (brother.isBlack) {
            if (brother.left?.isBlack == false && b && node == node.parent?.left)
                brother.rotateRight()
            else if (brother.right?.isBlack == false && a && node == node.parent?.right)
                brother.rotateLeft()
        }
        deleteCase6(node)
    }

    private fun deleteCase6(node: Node<T>) {
        val brother = node.brother()

        if (node == node.parent!!.left) {
            brother?.right?.isBlack = true
            node.parent?.rotateLeft()
        } else {
            brother?.left?.isBlack = true
            node.parent?.rotateRight()
        }

        if (root == node.parent)
            root = node.parent!!.parent
    }

    override fun iterator(): MutableSortedIterator<T> {
        return (object : MutableSortedIterator<T> {
            var initial = true
            var current: Node<T>? = null
            var lastReturned: Node<T>? = null

            override fun hasNext(): Boolean = current != lastNode
            override fun hasPrevious(): Boolean = !initial && current != firstNode

            override fun next(): T {
                if (initial) current = firstNode
                if (!hasNext()) throw NoSuchElementException()
                current = current.successor()
                return current.also { lastReturned = it }!!.value
            }
            override fun previous(): T {
                if (!hasPrevious()) throw NoSuchElementException()
                current = current.predecessor()
                return current.also { lastReturned = it }!!.value
            }
            override fun remove() {
                deleteNode(lastReturned!!)
                lastReturned = null
            }
        })
    }

    private fun Node<T>?.predecessor(): Node<T>? {
        var node = this ?: return null
        if (node.left != null) return node.left!!.max()
        while (node == node.parent?.left) node = node.parent!!
        return node.parent
    }

    private fun Node<T>?.successor(): Node<T>? {
        var node = this ?: return null
        if (node.right != null) return node.right!!.min()
        while (node == node.parent?.right) node = node.parent!!
        return node.parent
    }

    private fun Node<T>?.min(): Node<T>? = if (this?.left == null) this else left.min()

    private fun Node<T>?.max(): Node<T>? = if (this?.right == null) this else right.max()


    private class Node<T : Comparable<T>>(
            var value: T
    ) {
        var parent: Node<T>? = null
        var isBlack: Boolean = false

        var left: Node<T>? = null
        var right: Node<T>? = null

        fun isLeaf(): Boolean = (left == null) && (right == null)

        fun brother(): Node<T>? = if (this == parent?.left) parent!!.right else parent?.left

        fun rotateLeft() {
            val rightChild = right ?: return
            val father = parent

            swapColors(rightChild)
            rightChild.left?.parent = this
            right = rightChild.left
            rightChild.left = this

            when (this) {
                father?.left  -> father.left = rightChild
                father?.right -> father.right = rightChild
            }

            parent = rightChild
            rightChild.parent = father
        }

        fun rotateRight() {
            val leftChild = left ?: return
            val father = parent

            swapColors(leftChild)
            leftChild.right?.parent = this
            left = leftChild.right
            leftChild.right = this

            when (this) {
                father?.left  -> father.left = leftChild
                father?.right -> father.right = leftChild
            }

            parent = leftChild
            leftChild.parent = father
        }

        private fun swapColors(other: Node<T>?) {
            val tmp = isBlack
            other?.let {
                isBlack = it.isBlack
                it.isBlack = tmp
            }
        }

//    override fun equals(other: Any?): Boolean {
//        if (this === other) return true
//        if (other?.javaClass != javaClass) return false
//
//        other as RBNode<*>
//
//        if (value != other.value) return false
//        if (parent != other.parent) return false
//        if (isBlack != other.isBlack) return false
//        if (left != other.left) return false
//        if (right != other.right) return false
//
//        return true
//    }

//    override fun hashCode(): Int {
//        var result = value.hashCode()
//        result = 31 * result + (parent?.hashCode() ?: 0)
//        result = 31 * result + isBlack.hashCode()
//        result = 31 * result + (left?.hashCode() ?: 0)
//        result = 31 * result + (right?.hashCode() ?: 0)
//        return result
//    }
    }
}





