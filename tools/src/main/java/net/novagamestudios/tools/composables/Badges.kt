package net.novagamestudios.tools.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import net.novagamestudios.tools.formatLikeYT
import kotlin.time.Duration
import kotlin.time.ExperimentalTime



@Composable
fun Badge(
        modifier: Modifier = Modifier,
        backgroundColor: Color = Color.Unspecified,
        shape: Shape = RoundedCornerShape(2.dp),
        borderStroke: BorderStroke? = null,
        content: @Composable BoxScope.(Modifier) -> Unit
) {
    Box(modifier = modifier
            .onlyIfNotNull(borderStroke) { border(it, shape) }
            .clip(shape)
            .background(backgroundColor)
            .padding(horizontal = 4.dp)
    ) {
        content(Modifier.align(Alignment.Center))
    }
}

@Composable
fun TextBadge(
        text: AnnotatedString,
        modifier: Modifier = Modifier,
        color: Color = Color.Unspecified,
        backgroundColor: Color = Color.Unspecified,
        shape: Shape = RoundedCornerShape(2.dp),
        borderStroke: BorderStroke? = null
) {
    Badge(modifier = modifier,
          backgroundColor = backgroundColor,
          shape = shape,
          borderStroke = borderStroke
    ) {
        Text(text,
             modifier = it,
             fontSize = 12.sp,
             color = color)
    }
}


@ExperimentalTime
@Composable
fun DurationBadge(
        duration: Duration,
        modifier: Modifier = Modifier,
        backgroundColor: Color = Color.Unspecified
) {
    TextBadge(duration.formatLikeYT().annotated,
              modifier = modifier,
              backgroundColor = backgroundColor)
}


@Composable
fun LiveBadge(
        modifier: Modifier = Modifier,
        color: Color = Color.Red,
        inverted: Boolean = false,
) {
    // TODO string resource
    TextBadge("Live".annotated,
              modifier = modifier,
              color = if (inverted) Color.White else color,
              borderStroke = BorderStroke(1.dp, color),
              backgroundColor = if (inverted) color else Color.Transparent)
}