package net.novagamestudios.tools.collections

import net.novagamestudios.tools.void


fun <K, V> MutableMap<K, V>.add(element: MapEntry<K, V>): Boolean = put(element.key, element.value) != element.value

fun <K, V> MutableMap<K, V>.addAll(elements: Collection<MapEntry<K, V>>): Boolean {
    var changed = false
    for (element in elements) if (add(element)) changed = true
    return changed
}


fun <K, V> Map<K, V>.containsAllKeys(keys: Collection<K>): Boolean = keys.all(::containsKey)
fun <K, V> Map<K, V>.containsAllValues(values: Collection<V>): Boolean = values.all(::containsValue)


fun <T> Iterable<MapEntry<Int, T>>.indexed(): Iterable<IndexedValue<T>> = Iterable { iterator().mapped { IndexedValue(it.key, it.value) } }

fun <T> SparseList<T>.indexed(): Iterable<IndexedValue<T>> = entries.indexed()

inline fun <T> SparseList<T>.toFilledList(minSize: Int, maxSize: Int = minSize, def: (Int) -> T): List<T> {
    val list = mutableListOf<T>()
    for (index in 0 until minSize) list[index] = this[index] ?: def(index)
    for (index in minSize until maxSize) list[index] = this[index] ?: break
    return list
}

fun <T> SparseList<T>.toListOrNull(minSize: Int, maxSize: Int = minSize): List<T>? = toFilledList(minSize, maxSize) { return null }


fun <T> MutableList<T>.move(fromIndex: Int, toIndex: Int) = add(toIndex, removeAt(fromIndex))


fun <T> MutableList<T>.removeIndices(indices: IntRange): List<T> = removeIndices(indices.asIterable())
fun <T> MutableList<T>.removeIndices(indices: Iterable<Int>): List<T> {
    val removed = mutableListOf<T>()
    indices.withIndex().forEach { (offset, i) -> removed += removeAt(i - offset) }
    return removed
}





inline fun <T> MutableIterable<T>.consume(crossinline consumer: (T) -> Unit) = removeAll {
    consumer(it)
    true
}.void()


fun <T> MutableIterable<T>.removeFirst(p: (T) -> Boolean): T? {
    val iterator = iterator()
    while (iterator.hasNext()) {
        val v = iterator.next()
        if (!p(v)) continue
        iterator.remove()
        return v
    }
    return null
}


fun <T> Iterable<T>.filterIndexIn(range: IntRange) = filterIndexed { i, _ -> i in range }

fun <T> Iterable<T>.forEachIndexIn(range: IntRange, action: (T) -> Unit) = filterIndexIn(range).forEach(action)




fun <T> Sequence<T>.prepend(vararg elements: T): Sequence<T> = sequenceOf(*elements) + this
fun <T> Sequence<T>.append(vararg elements: T): Sequence<T> = this + sequenceOf(*elements)







fun <T> List<T>.copy() = toList()



