package net.novagamestudios.tools.collections


interface LinkedMap<K, V> : Map<K, V> {
    fun entryAt(index: Int): Pair<K, V>
    fun keyAt(index: Int): K = entryAt(index).first
    fun valueAt(index: Int): V = entryAt(index).second
}

interface MutableLinkedMap<K, V> : LinkedMap<K, V>, MutableMap<K, V>


fun <K, V> linkedMapOf(): LinkedMap<K, V> =
        LinkedMapImpl(mapOf(), listOf())
fun <K, V> linkedMapOf(vararg pairs: Pair<K, V>): LinkedMap<K, V> =
        LinkedMapImpl(mapOf(*pairs), listOf(*pairs))

fun <K, V> mutableLinkedMapOf(): MutableLinkedMap<K, V> =
        MutableLinkedMapImpl(mutableMapOf(), mutableListOf())
fun <K, V> mutableLinkedMapOf(vararg pairs: Pair<K, V>): MutableLinkedMap<K, V> =
        MutableLinkedMapImpl(mutableMapOf(*pairs), mutableListOf(*pairs))




private open class LinkedMapImpl<K, V>(
        protected open val map: Map<K, V>,
        protected open val link: List<Pair<K, V>>
) : LinkedMap<K, V>, Map<K, V> by map, List<Pair<K, V>> by link {
    override val size: Int get() = map.size
    override fun isEmpty(): Boolean = map.isEmpty()

    override fun entryAt(index: Int): Pair<K, V> = link[index]
}

private open class MutableLinkedMapImpl<K, V>(
        protected open val map: MutableMap<K, V>,
        protected open val link: MutableList<Pair<K, V>>
) : MutableLinkedMap<K, V>, MutableMap<K, V> by map, List<Pair<K, V>> by link {
    override val size: Int get() = map.size
    override fun isEmpty(): Boolean = map.isEmpty()

    override fun entryAt(index: Int): Pair<K, V> = link[index]

    override fun clear() {
        link.clear()
        map.clear()
    }
    override fun put(key: K, value: V): V? = map.put(key, value).also { link.add(key to value) }
    override fun putAll(from: Map<out K, V>) = from.forEach { (k, v) -> put(k, v) }
    override fun remove(key: K): V? = map.remove(key)?.also { link.removeFirst { (_, v) -> it == v } }
}