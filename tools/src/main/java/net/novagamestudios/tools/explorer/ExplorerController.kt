package net.novagamestudios.tools.explorer

import android.content.Context
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import net.novagamestudios.tools.explorer.LazyCachedDocumentFile.Companion.lazyCached
import net.novagamestudios.tools.letIfNull
import net.novagamestudios.tools.realPath
import net.novagamestudios.tools.toBreadcrumbs


class ExplorerController(
        context: Context?,
        coroutineScope: CoroutineScope
) {
    private val _current = MutableStateFlow<LazyCachedDocumentFile?>(null)

    val current: StateFlow<LazyCachedDocumentFile?> = _current

    val children: StateFlow<List<LazyCachedDocumentFile>> = current
            .map { it?.listFiles?.map(::LazyCachedDocumentFile).orEmpty() }
            .stateIn(coroutineScope, SharingStarted.WhileSubscribed(), emptyList())

    val breadcrumbs: StateFlow<List<String>> = current
            .map { it?.let { it.uri?.toBreadcrumbs(context, true) }.orEmpty() }
            .stateIn(coroutineScope, SharingStarted.WhileSubscribed(), emptyList())

    val path: StateFlow<String> = current
            .map { it?.let { it.uri?.realPath(context) }.letIfNull { "." } }
            .stateIn(coroutineScope, SharingStarted.WhileSubscribed(), ".")


    fun navigateTo(context: Context, file: Uri) {
        navigateTo(DocumentFile.fromTreeUri(context, file)?.lazyCached())
    }

    fun navigateTo(file: LazyCachedDocumentFile?) {
        _current.value = file
    }

    fun navigateUp() {
        current.value?.parentFile?.run { navigateTo(lazyCached()) }
    }


}