package net.novagamestudios.tools

import android.content.Context
import androidx.annotation.Dimension
import net.novagamestudios.tools.DimensionUnit.*


enum class DimensionUnit(
        val label: String
) {
    UNDEFINED("?"),
    PX("px"),
    IN("in"),
    MM("mm"),
    PT("pt"),
    DP("dp"),
    SP("sp");
}

private fun getUnitFromString(str: String): DimensionUnit {
    return DimensionUnit.values()
            .find { it.label.equals(str, ignoreCase = true) }
            .letIfNull { UNDEFINED }
}

@Dimension
fun Context.toPX(str: String): Int {
    val (_, v, u) = "(\\d+)\\s*(\\D+)".toRegex().find(str)?.groupValues
                    ?: throw IllegalArgumentException("String does not look like a dimension")
    val value = v.toFloat()
    return when (getUnitFromString(u)) {
        PX -> value.toInt()
        IN -> inToPX(value)
        MM -> mmToPX(value)
        PT -> ptToPX(value)
        DP -> dpToPX(value)
        SP -> spToPX(value)
        UNDEFINED -> throw IllegalArgumentException()
    }
}

@Dimension fun Context.toIN(str: String): Float = pxToIN(toPX(str).toFloat())
@Dimension fun Context.toMM(str: String): Float = pxToMM(toPX(str).toFloat())
@Dimension fun Context.toPT(str: String): Float = pxToPT(toPX(str).toFloat())
@Dimension fun Context.toDP(str: String): Float = pxToDP(toPX(str).toFloat())
@Dimension fun Context.toSP(str: String): Float = pxToSP(toPX(str).toFloat())


private val Context.inToPX get() = resources.displayMetrics.xdpi
private val Context.mmToPX get() = resources.displayMetrics.xdpi * (1.0f / 25.4f)
private val Context.ptToPX get() = resources.displayMetrics.xdpi * (1.0f / 72f)
private val Context.dpToPX get() = resources.displayMetrics.density
private val Context.spToPX get() = resources.displayMetrics.scaledDensity

@Dimension fun Context.inToPX(iu: Float): Int   = (iu * inToPX).toInt()
@Dimension fun Context.mmToPX(mm: Float): Int   = (mm * mmToPX).toInt()
@Dimension fun Context.ptToPX(pt: Float): Int   = (pt * ptToPX).toInt()
@Dimension fun Context.dpToPX(dp: Float): Int   = (dp * dpToPX).toInt()
@Dimension fun Context.spToPX(sp: Float): Int   = (sp * spToPX).toInt()
@Dimension fun Context.pxToIN(px: Float): Float =  px / inToPX
@Dimension fun Context.pxToMM(px: Float): Float =  px / mmToPX
@Dimension fun Context.pxToPT(px: Float): Float =  px / ptToPX
@Dimension fun Context.pxToDP(px: Float): Float =  px / dpToPX
@Dimension fun Context.pxToSP(px: Float): Float =  px / spToPX

//fun convert(@Nullable point: Point?, context: Context, converter: Getter.TwoParams<Int?, Context?, Int?>) {
//    if (point == null) return
//    point.x = converter.onGet(context, point.x)
//    point.y = converter.onGet(context, point.y)
//}
