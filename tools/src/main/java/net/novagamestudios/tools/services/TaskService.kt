package net.novagamestudios.tools.services

import android.app.Notification
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import net.novagamestudios.tools.Logger
import net.novagamestudios.tools.info
import net.novagamestudios.tools.services.BoundServiceConnection.Companion.connectToBoundService
import net.novagamestudios.tools.services.BoundServiceConnection.Companion.runOnBoundServiceAsync
import net.novagamestudios.tools.waitForAll
import net.novagamestudios.tools.*
import net.novagamestudios.tools.collections.consume
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds
import kotlin.time.ExperimentalTime

@ExperimentalTime
open class TaskService : LifecycleService(), Logger {

    protected open val useAutoShutdown: Boolean = true
    protected open val autoShutdownTimeout: Duration = 4.seconds

    private val jobChannel = Channel<Job>(Channel.UNLIMITED)


    init {
        lifecycleScope.launchWhenCreated { if (useAutoShutdown) startAutoShutdown() }
    }

    private suspend fun startAutoShutdown() {
        // require at least one
        jobChannel.receive().join()
        // wait for all currently running tasks before stopping
        jobChannel.waitForAll(autoShutdownTimeout)
        info { "stopping" }
        stopSelf()
    }

    fun startTask(task: Task): Job = lifecycleScope.launchWhenCreated {
        jobChannel.send(launchTask(task))
    }

    private fun CoroutineScope.launchTask(task: Task) = launch {
        info { "starting Task" }
        createTaskContext().use { it.task() }
        info { "done Task" }
    }


    override fun onBind(intent: Intent): IBinder {
        super.onBind(intent)
        return BoundServiceConnection.DefaultBinder(this)
    }

    protected open fun createTaskContext(): TaskContext = DefaultTaskContext()


    interface TaskContext : AutoCloseable {
        val androidContext: Context
        fun showForegroundNotification(id: Int, notification: Notification)
        fun cancelNotification(id: Int)
    }

    protected inner class DefaultTaskContext : TaskContext {
        private val notificationManager by lazy { NotificationManagerCompat.from(androidContext) }
        private val notifications = mutableSetOf<Int>()

        override val androidContext: Context = this@TaskService
        override fun showForegroundNotification(id: Int, notification: Notification) {
            startForeground(id, notification)
            notifications += id
        }
        override fun cancelNotification(id: Int) {
            notificationManager.cancel(id)
            notifications -= id
        }
        override fun close() {
            notifications.consume { notificationManager.cancel(it) }
        }
    }
}

@ExperimentalTime
typealias Task = suspend TaskService.TaskContext.() -> Unit


@ExperimentalTime
inline fun <reified S : TaskService> Context.connectToTaskService(): BoundServiceConnection<S> =
        connectToBoundService()
@ExperimentalTime
inline fun <reified S : TaskService> Context.startTaskAsync(noinline task: Task) =
        runOnBoundServiceAsync<S, Job>(false) { startTask(task) }





