package net.novagamestudios.tools

import kotlinx.serialization.json.*


fun String.parseAsJson() = Json.parseToJsonElement(this)


val JsonElement.isNull get() = this is JsonNull
val JsonElement.asString get() = tryCatchAll { jsonPrimitive.contentOrNull }
val JsonElement.asBoolean get() = tryCatchAll { jsonPrimitive.booleanOrNull }
val JsonElement.asInt get() = tryCatchAll { jsonPrimitive.intOrNull }
val JsonElement.asLong get() = tryCatchAll { jsonPrimitive.longOrNull }
val JsonElement.asFloat get() = tryCatchAll { jsonPrimitive.floatOrNull }
val JsonElement.asDouble get() = tryCatchAll { jsonPrimitive.doubleOrNull }
val JsonElement.asObject get() = tryCatchAll { jsonObject }
val JsonElement.asArray get() = tryCatchAll { jsonArray }

fun JsonArray.toStrings(): List<String?> = map { it.asString }
fun JsonArray.toJsonObjects(): List<JsonObject?> = map { it.asObject }