package net.novagamestudios.tools

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import java.util.concurrent.Executor
import kotlin.coroutines.CoroutineContext
import kotlin.time.Duration
import kotlin.time.ExperimentalTime





//fun CoroutineContext.getExecutor(): Executor = (get(ContinuationInterceptor) as CoroutineDispatcher).asExecutor()
val CoroutineContext.asExecutor: Executor get() = Executor { CoroutineScope(this).launch { it() } }

@ExperimentalTime
suspend fun Channel<Job>.waitForAll(timeout: Duration = Duration.ZERO) {
    val logger = LoggerForFun()
    for (job in generateSequence { tryReceive().getOrNull() }) {
        logger.info { "job joining..." }
        job.join()
        logger.info { "job joined" }
        delay(timeout)
    }
}




class CoroutineQueue(
        val parent: Job? = null,
        capacity: Int = Channel.UNLIMITED
) {

    private val scope = CoroutineScope(SupervisorJob(parent))
    private val queue = Channel<Job>(capacity)

    init {
        scope.launch { for (job in queue) job.join() }
    }

    suspend fun enqueue(block: suspend () -> Unit): Job {
        val job = scope.launch(start = CoroutineStart.LAZY) { block() }
        queue.send(job)
        return job
    }

    fun tryEnqueue(block: suspend () -> Unit): Job? {
        val job = scope.launch(start = CoroutineStart.LAZY) { block() }
        return if (queue.trySend(job).isSuccess) job else null
    }
}






//class Coroutine () {
////    private var lastRequestTime: LocalDateTime = LocalDateTime.now()
//    private val requestChannel: Channel<Deferred<*>> = Channel()
//    //SupervisorJob is used because we want to have continuous processing of requestChannel
//    //even if one of the requests fails
//    private val coroutineScope = CoroutineScope(SupervisorJob())
//
//    override suspend fun <T> request(block: () -> T): T {
//        val deferred = coroutineScope.async(start = CoroutineStart.LAZY) { block() }
//        requestChannel.send(deferred)
//        return deferred.await()
//    }
//
//    @PostConstruct
//    fun startProcessing() = coroutineScope.launch {
//        for (request in requestChannel) {
//            val now = LocalDateTime.now()
//            val diff = ChronoUnit.MILLIS.between(lastRequestTime, now)
//            if (diff < interval) {
//                delay(interval - diff)
//            }
//            lastRequestTime = LocalDateTime.now()
//            request.start()
//        }
//    }
//}


