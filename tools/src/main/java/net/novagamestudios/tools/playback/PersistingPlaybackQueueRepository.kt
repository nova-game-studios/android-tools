package net.novagamestudios.tools.playback

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import net.novagamestudios.tools.Logger
import net.novagamestudios.tools.info


@ExperimentalCoroutinesApi
abstract class PersistingPlaybackQueueRepository<Item : QueueItem>(
        scope: CoroutineScope
) : PlaybackQueueRepository<Item>(scope), Logger {

    private val pps get() = persistedPlaybackState.value
    private val pqs get() = persistedQueueState.value

    init {
        launch { persistedPlaybackState.collect {
            playbackController.valueOrNull?.let { c -> applyPersistedPlaybackStateIfDifferent(c, it) }
        } }
        launch { playbackState.collect { persistPlaybackState(it) } }

        launch { persistedQueueState.collect {
            queueController.valueOrNull?.let { c -> applyPersistedQueueStateIfDifferent(c, it) }
        } }
        launch { queueState.collect { s -> persistQueueState(s) } }
    }


    protected abstract val persistedPlaybackState: StateFlow<PlaybackState>
    protected abstract val persistedQueueState: StateFlow<QueueState<Item>>

    protected abstract suspend fun persistPlaybackState(state: PlaybackState?)
    protected abstract suspend fun persistQueueState(state: QueueState<Item>?)

    override suspend fun onPlaybackControllerChanged(controller: PlaybackController) {
        super.onPlaybackControllerChanged(controller)
        applyPersistedPlaybackStateIfDifferent(controller, pps)
    }

    override suspend fun onQueueControllerChanged(controller: QueueController<Item>) {
        super.onQueueControllerChanged(controller)
        applyPersistedQueueStateIfDifferent(controller, pqs)
    }

    protected open suspend fun applyPersistedPlaybackStateIfDifferent(controller: PlaybackController, persistedState: PlaybackState) {
        val state = controller.playbackState.value ?: return
        if (persistedState.repeatMode != state.repeatMode) controller.setRepeatMode(persistedState.repeatMode)
        if (persistedState.shuffleMode != state.shuffleMode) controller.setShuffleMode(persistedState.shuffleMode)
    }
    protected open suspend fun applyPersistedQueueStateIfDifferent(controller: QueueController<Item>, persistedState: QueueState<Item>) {
        info { "apply persisted QS: $persistedState" }
        val state = controller.queueState.value ?: return
//        if (persistedState.queue.size != state.queue.size
//            || persistedState.queue.withIndex().any { (i, v) -> v != state.queue[i] }) {
        if (persistedState.queue != state.queue) {
            info { "queue different" }
            controller.setAll(persistedState.queue)
        }
        if (persistedState.currentIndex != state.currentIndex) {
            controller.skipToIndex(persistedState.currentIndex)
        }
    }

}
