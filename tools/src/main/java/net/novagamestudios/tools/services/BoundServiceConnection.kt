package net.novagamestudios.tools.services

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import net.novagamestudios.tools.alsoIf
import net.novagamestudios.tools.collections.consume
import net.novagamestudios.tools.completedDeferred

class BoundServiceConnection<S : Service> : ServiceConnection {
    var service: S? = null
    private var unbindContext: Context? = null
    private val queue = mutableListOf<S.() -> Unit>()


    @Suppress("UNCHECKED_CAST")
    override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
        service = (binder as? Binder<S>)?.service
        service?.let { s -> queue.consume { s.it() } }
        unbindContext?.unbindService(this)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        service = null
        unbindContext = null
    }

    fun unbindAfterRun(context: Context?) {
        unbindContext = context
    }

    fun <R> runAsync(block: S.() -> R): Deferred<R> =
            tryRun(block)?.completedDeferred()
            ?: CompletableDeferred<R>().also { queue += { it.complete(block()) } }

    fun <R> tryRun(block: S.() -> R): R? = service?.block()


    interface Binder<S : Service> : IBinder {
        val service: S
    }

    data class DefaultBinder<S : Service>(
            override val service: S
    ) : android.os.Binder(), Binder<S>

    companion object {
        inline fun <reified S : Service> Context.connectToBoundService(): BoundServiceConnection<S> {
            return connectToBoundService(S::class.java)
        }

        fun <S : Service> Context.connectToBoundService(service: Class<S>): BoundServiceConnection<S> {
            startService(Intent(this, service))
            return BoundServiceConnection<S>().also {
                bindService(Intent(this, service), it, Context.BIND_AUTO_CREATE)
            }
        }
        inline fun <reified S : Service, R> Context.runOnBoundServiceAsync(
                stopServiceAfterRun: Boolean,
                crossinline block: S.() -> R
        ): Deferred<R> {
            return connectToBoundService<S>().run {
                runAndUnbindAsync(this@runOnBoundServiceAsync) {
                    block().alsoIf(stopServiceAfterRun) { stopSelf() }
                }
            }
        }
    }
}

fun <S : Service, R> BoundServiceConnection<S>.runAndUnbindAsync(context: Context?, block: S.() -> R): Deferred<R> {
    return runAsync(block).also { unbindAfterRun(context) }
}