package net.novagamestudios.tools.composables

import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Image
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.coil.CoilImage
import com.google.accompanist.imageloading.ImageLoadState


@Preview("None")
@Composable
private fun NetworkImagePreviewNone() {
    Box(modifier = Modifier
            .width(160.dp)
            .height(90.dp)) {
        RemoteImage(null, null, modifier = Modifier.fillMaxSize())
    }
}


@Composable
fun RemoteImage(
        url: String?,
        contentDescription: String?,
        modifier: Modifier = Modifier,
        onState: (ImageLoadState) -> Unit = {}
) {
    if (url != null) CoilImage(
            data = url,
            contentDescription = contentDescription,
            fadeIn = true,
            contentScale = ContentScale.Fit,
            onRequestCompleted = onState,
            loading = {
                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
            },
            error = {
                Icon(Icons.Filled.Warning, null, modifier = Modifier.align(Alignment.Center))
            },
            modifier = modifier
    ) else Box(modifier = modifier) {
        Icon(Icons.Filled.Image, null, modifier = Modifier.align(Alignment.Center))
        SideEffect {
            onState(ImageLoadState.Empty)
        }
    }
}
