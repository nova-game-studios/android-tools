package net.novagamestudios.tools.collections


interface SortedIterable<out T> : Iterable<T> {
    override fun iterator(): SortedIterator<T>
}

interface MutableSortedIterable<T> : SortedIterable<T>, MutableIterable<T> {
    override fun iterator(): MutableSortedIterator<T>
}


interface SortedCollection<out T> : SortedIterable<T>, Collection<T> {
    val first: T
    val last: T
}

interface MutableSortedCollection<T> : SortedCollection<T>, MutableSortedIterable<T>, MutableCollection<T>


interface SortedIterator<out T> : Iterator<T> {

    /**
     * Returns `true` if there are elements in the iteration before the current element.
     */
    fun hasPrevious(): Boolean

    /**
     * Returns the previous element in the iteration and moves the cursor position backwards.
     */
    fun previous(): T
}

interface MutableSortedIterator<out T> : SortedIterator<T>, MutableIterator<T>





interface SortedSet<out T> : SortedCollection<T>, Set<T>

interface MutableSortedSet<T> : SortedSet<T>, MutableSortedCollection<T>, MutableSet<T>


interface SortedMap<K, out V> : Map<K, V> {
    val first: K
    val last: K

    override val entries: SortedSet<SortedMapEntry<K, V>>
    override val keys: SortedSet<K>
    override val values: SortedCollection<V>
}

interface MutableSortedMap<K, V> : SortedMap<K, V>, MutableMap<K, V> {
    override val entries: MutableSortedSet<MutableSortedMapEntry<K, V>>
    override val keys: MutableSortedSet<K>
    override val values: MutableSortedCollection<V>
}

typealias SortedMapEntry<K, V> = MapEntry<K, V>
typealias MutableSortedMapEntry<K, V> = MutableMapEntry<K, V>


typealias SparseList<T> = SortedMap<Int, T>
typealias MutableSparseList<T> = MutableSortedMap<Int, T>



// region BUILDERS


fun <T : Comparable<T>> sortedSetOf(): SortedSet<T> =
        RedBlackTreeSet()
fun <T : Comparable<T>> sortedSetOf(vararg values: T): SortedSet<T> =
        RedBlackTreeSet<T>().apply { addAll(values) }

fun <T : Comparable<T>> mutableSortedSetOf(): MutableSortedSet<T> =
        RedBlackTreeSet()
fun <T : Comparable<T>> mutableSortedSetOf(vararg values: T): MutableSortedSet<T> =
        RedBlackTreeSet<T>().apply { addAll(values) }

fun <K : Comparable<K>, V> sortedMapOf(): SortedMap<K, V> =
        RedBlackTreeMap()
fun <K : Comparable<K>, V> sortedMapOf(vararg pairs: Pair<K, V>): SortedMap<K, V> =
        RedBlackTreeMap<K, V>().apply { putAll(pairs) }

fun <K : Comparable<K>, V> mutableSortedMapOf(): MutableSortedMap<K, V> =
        RedBlackTreeMap()
fun <K : Comparable<K>, V> mutableSortedMapOf(vararg pairs: Pair<K, V>): MutableSortedMap<K, V> =
        RedBlackTreeMap<K, V>().apply { putAll(pairs) }

fun <T> sparseListOf(): SparseList<T> =
        RedBlackTreeMap()
fun <T> sparseListOf(vararg values: IndexedValue<T>): SparseList<T> =
        RedBlackTreeMap<Int, T>().apply { values.forEach { put(it.index, it.value) } }

fun <T> mutableSparseListOf(): MutableSparseList<T> =
        RedBlackTreeMap()
fun <T> mutableSparseListOf(vararg values: IndexedValue<T>): MutableSparseList<T> =
        RedBlackTreeMap<Int, T>().apply { values.forEach { put(it.index, it.value) } }


// endregion



// region ABSTRACTS

abstract class AbstractSortedCollection<T> : AbstractCollection<T>(), SortedCollection<T>
abstract class AbstractMutableSortedCollection<T> : AbstractMutableCollection<T>(),
    MutableSortedCollection<T>

abstract class AbstractSortedSet<T> : AbstractSet<T>(), SortedSet<T>
abstract class AbstractMutableSortedSet<T> : AbstractMutableSet<T>(), MutableSortedSet<T>

abstract class AbstractSortedMap<K, out V> : SortedMap<K, V>, AbstractMap<K, V>() {
    override val keys: SortedSet<K> get() = entries.mapped { it.key }
    override val values: SortedCollection<V> get() = entries.mapped { it.value }
}
abstract class AbstractMutableSortedMap<K, V> : MutableSortedMap<K, V>, AbstractMutableMap<K, V>() {
    override val keys: MutableSortedSet<K> get() = entries.mappedMutable({ it.key }, { throw UnsupportedOperationException() })
    override val values: MutableSortedCollection<V> get() = entries.mappedMutable({ it.value }, { throw UnsupportedOperationException() })

    override fun clear() = entries.clear()
    override fun putAll(from: Map<out K, V>) = from.forEach { put(it.key, it.value) }
    override fun remove(key: K): V? = entries.removeFirst { it.key == key }?.value
}

// endregion



