package net.novagamestudios.tools

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach


inline fun <T, R> T.debugError(
        marker: String? = null,
        block: T.() -> R
): R = tryCatchAll({ throw DebugException(marker, it) }, block)

inline fun <R> debugError(
        marker: String? = null,
        crossinline block: () -> R
): R = Unit.debugError(marker) { block() }


fun <T> Flow<T>.flowDebugError(marker: String? = null) = catch { throw DebugException(marker, it) }



inline fun <T> T.dump(
        marker: String? = null,
        crossinline what: T.() -> Any? = { this },
        logger: Logger = LoggerForFun(1)
): T = also { logger.info { if (marker != null) "$marker: ${what()}" else what() } }

inline fun <T> Flow<T>.flowDump(
        marker: String? = null,
        crossinline what: (T) -> Any? = { it },
        logger: Logger = LoggerForFun(1)
): Flow<T> = onEach { value -> value.dump(marker, what, logger) }





class DebugException(
        marker: String? = null,
        cause: Throwable? = null
) : Throwable(marker, cause)








