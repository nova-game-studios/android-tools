package net.novagamestudios.tools

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.internal.*
import net.novagamestudios.tools.collections.LinkedMap
import net.novagamestudios.tools.collections.mutableLinkedMapOf
import net.novagamestudios.tools.collections.*


inline fun <T> LifecycleCoroutineScope.collectWhenCreated(
        crossinline flow: () -> Flow<T>,
        noinline action: suspend (value: T) -> Unit
): Job = launchWhenCreated { flow().collect(action) }

inline fun <T> LifecycleCoroutineScope.collectWhenStarted(
        crossinline flow: () -> Flow<T>,
        noinline action: suspend (value: T) -> Unit
): Job = launchWhenStarted { flow().collect(action) }

inline fun <T> LifecycleCoroutineScope.collectWhenResumed(
        crossinline flow: () -> Flow<T>,
        noinline action: suspend (value: T) -> Unit
): Job = launchWhenResumed { flow().collect(action) }


inline fun <T> LifecycleCoroutineScope.observeWhenCreated(
    crossinline flow: () -> EventFlow<T>,
    crossinline action: suspend (value: T) -> Unit
): Job = launchWhenCreated { flow().observe(action) }

inline fun <T> LifecycleCoroutineScope.observeWhenStarted(
    crossinline flow: () -> EventFlow<T>,
    crossinline action: suspend (value: T) -> Unit
): Job = launchWhenStarted { flow().observe(action) }

inline fun <T> LifecycleCoroutineScope.observeWhenResumed(
    crossinline flow: () -> EventFlow<T>,
    crossinline action: suspend (value: T) -> Unit
): Job = launchWhenResumed { flow().observe(action) }






typealias EventFlow<T> = StateFlow<Event<T>>

typealias MutableEventFlow<T> = MutableStateFlow<Event<T>>

@Suppress("FunctionName")
fun <T> MutableEventFlow(): MutableEventFlow<T> = MutableStateFlow(Event.invalid())

typealias Collector<T> = suspend (T) -> Unit

/**
 * The [user] gets the Event only once
 */
suspend inline fun <T> EventFlow<T>.observeLocally(user: Any, crossinline collector: Collector<T>): Nothing =
        collect { it.use(user)?.let { data -> collector(data) } }

/**
 * Only one collector gets the Event
 */
suspend inline fun <T> EventFlow<T>.observe(crossinline collector: Collector<T>): Nothing =
        collect { it.use()?.let { data -> collector(data) } }

@Composable
inline fun <T> EventFlow<T>.observeWithState(crossinline collector: @Composable (T) -> Unit): Event<T> {
    val event by collectAsState()
    event.use()?.let { collector(it) }
    return event
}

fun <T> EventFlow<T>.filterValid() = filter { it.isValid }
fun <T> EventFlow<T>.filterUsable() = filter { it.isUsable() }

fun <T> MutableEventFlow<T>.dispatch(data: T) {
    value = Event(data)
}
fun MutableEventFlow<Unit>.dispatch() = dispatch(Unit)


@ExperimentalCoroutinesApi
fun <T, E, R> Flow<T>.replayLatestOnEvent(
    eventFlow: EventFlow<E>,
    combineWithEvent: (request: T, event: E) -> R
): Flow<R> where T : R = channelFlow {
    val collector = this
    coroutineScope {
        var latest: T? = null
        var init = false
        launch {
            this@replayLatestOnEvent.collect {
                latest = it
                init = true
                collector.send(it)
            }
        }
        launch {
            eventFlow.collect { event ->
                if (init) {
                    event.use()?.let { value ->
                        @Suppress("UNCHECKED_CAST")
                        collector.send(combineWithEvent(latest as T, value))
                    }
                }
            }
        }
    }
}




data class Event<out T>(
        private val data: T
) {
    companion object {
        private val INVALID: Event<*> = Event<Any?>(null).apply { isValid = false }

        /** @return an invalid Content
         */
        @Suppress("UNCHECKED_CAST")
        fun <T> invalid(): Event<T> = INVALID as Event<T>
    }

    var isValid = true
        private set
    private val usedBy: MutableSet<Any?> = mutableSetOf()


    fun isUsable(user: Any = this): Boolean = isValid && user !in usedBy && this !in usedBy

    /**
     * Uses this content. After this, [isUsable(user)][isUsable] will always return false.
     * @return the content or null if not usable
     */
    fun use(user: Any = this): T? = user
            .takeIf { isUsable(user) }
            ?.also { usedBy += it }
            ?.let { data }

    /**
     * Gets this content without using it, if it is valid.
     * @return the content or null if invalid
     */
    fun get(): T? = if (isValid) data else null

    fun invalidate() {
        isValid = false
    }

    override fun equals(other: Any?): Boolean = this === other
    override fun hashCode(): Int = data?.hashCode() ?: 0
}

fun <T, R> Event<T>.use(user: Any = this, block: (T) -> R?): R? = use(user)?.let(block)



@Deprecated("just don't")
fun <T, R> StateFlow<T>.mapNonSuspending(f: (T) -> R): StateFlow<R> = NonSuspendingMappedStateFlow(this, f)

fun <T, R> FlowCollector<T>.mapIn(f: suspend (R) -> T): FlowCollector<R> = FlowCollector { emit(f(it)) }

@Deprecated("just don't")
class NonSuspendingMappedStateFlow<T, R>(
        private val original: StateFlow<T>,
        private val f: (T) -> R
) : StateFlow<R> {
    override val replayCache: List<R> get() = original.replayCache.map(f)
    override val value: R get() = f(original.value)
    @InternalCoroutinesApi
    override suspend fun collect(collector: FlowCollector<R>) = original.collect(collector.mapIn { f(it) })
}


@Suppress("FunctionName")
fun <T> FlowCollector(emit: suspend (T) -> Unit): FlowCollector<T> = object : FlowCollector<T> {
    override suspend fun emit(value: T) = emit(value)
}







//fun <T> List<Flow<T>>.combineFlows(): Flow<SparseList<T>> {
//    val values = mutableSparseListOf<T>()
//    return  flow combined@{ coroutineScope {
//        for ((index, flow) in withIndex()) launch { flow.collect { value ->
//            values[index] = value
//            this@combined.emit(values)
//        } }
//    } }
//}

fun <K, T> List<Pair<K, Flow<T>>>.combineFlows(): Flow<LinkedMap<K, T>> {
    val values = mutableLinkedMapOf<K, T>()
    return  flow combined@{ coroutineScope {
        for ((key, flow) in this@combineFlows) launch { flow.collect { value ->
            values[key] = value
            this@combined.emit(values)
        } }
    } }
}



fun <T> Flow<T>.prepend(f: suspend () -> T) = onStart { emit(f()) }






fun <T> StateFlow<T>.stateToShared() {

}


fun <T> StateFlow<T>.onEachState(
        scope: CoroutineScope,
        action: suspend (T) -> Unit
): StateFlow<T> = transformState(scope, value, TransformStateMode.IGNORE_FIRST_EMIT) { _, it ->
    action(it)
    emit(it)
}

inline fun <T, R> StateFlow<T>.mapState(
        scope: CoroutineScope,
        crossinline transform: (T) -> R
): StateFlow<R> = transformState(scope, transform(value),
                                 TransformStateMode.DROP_FIRST_TRANSFORM
) { _, it ->
    emit(transform(it))
}


inline fun <T, R> StateFlow<T>.transformState(
    scope: CoroutineScope,
    initial: R,
    mode: TransformStateMode = TransformStateMode.IGNORE_FIRST_EMIT,
    crossinline transform: suspend FlowCollector<R>.(CoroutineScope, T) -> Unit
): StateFlow<R> {
    val mutable = MutableStateFlow(initial)
    var dropTransform = mode == TransformStateMode.DROP_FIRST_TRANSFORM
    var ignoreEmit = mode == TransformStateMode.IGNORE_FIRST_EMIT
    val collector = FlowCollector<R> {
        if (ignoreEmit) ignoreEmit = false
        else mutable.emit(it)
    }
    scope.launch {
        collect {
            if (dropTransform) dropTransform = false
            else collector.transform(this, it)
        }
    }
    return mutable
}

enum class TransformStateMode {
    ALL,
    IGNORE_FIRST_EMIT,
    DROP_FIRST_TRANSFORM
}

fun <T> StateFlow<StateFlow<T>>.flattenLatestState(
        scope: CoroutineScope
): StateFlow<T> {
    var previous: Job? = null
    return transformState(scope, value.value, TransformStateMode.IGNORE_FIRST_EMIT) { s, flow ->
        previous?.cancelAndJoin()
        previous = s.launch {
            flow.collect { emit(it) }
        }
    }
}

inline fun <T, R> StateFlow<T>.flatMapLatestState(
        scope: CoroutineScope,
        crossinline transform: (T) -> StateFlow<R>
): StateFlow<R> = mapState(scope) { transform(it) }.flattenLatestState(scope)






//private class FlattenedStateFlow<T>(
//        private val original: StateFlow<StateFlow<T>>
//) : StateFlow<T> {
//    override val replayCache: List<T> get() = original.replayCache.map { it.value }
//    override var value: T = original.value.value
//        private set
//    @InternalCoroutinesApi
//    override suspend fun collect(collector: FlowCollector<T>) {
//        var previous: Job? = null
//        coroutineScope {
//            original.collect { flow ->
//                previous?.cancelAndJoin()
//                emit(collector, flow.value)
//                previous = launch(start = CoroutineStart.UNDISPATCHED) {
//                    flow.collect { emit(collector, it) }
//                }
//            }
//        }
//    }
//
//    private suspend fun emit(collector: FlowCollector<T>, newValue: T) {
//        if (newValue == value) return
//        value = newValue
//        collector.emit(newValue)
//    }
//}
//
//internal class ChannelFlowTransformLatest<T, R>(
//        private val transform: suspend FlowCollector<R>.(value: T) -> Unit,
//        flow: Flow<T>,
//        context: CoroutineContext = EmptyCoroutineContext,
//        capacity: Int = Channel.BUFFERED,
//        onBufferOverflow: BufferOverflow = BufferOverflow.SUSPEND
//) : ChannelFlowOperator<T, R>(flow, context, capacity, onBufferOverflow) {
//    override fun create(context: CoroutineContext, capacity: Int, onBufferOverflow: BufferOverflow): ChannelFlow<R> =
//            ChannelFlowTransformLatest(transform, flow, context, capacity, onBufferOverflow)
//
//    override suspend fun flowCollect(collector: FlowCollector<R>) {
//        assert { collector is SendingCollector } // So cancellation behaviour is not leaking into the downstream
//        flowScope {
//            var previousFlow: Job? = null
//            flow.collect { value ->
//                previousFlow?.apply {
//                    cancel(ChildCancelledException())
//                    join()
//                }
//                // Do not pay for dispatch here, it's never necessary
//                previousFlow = launch(start = CoroutineStart.UNDISPATCHED) {
//                    collector.transform(value)
//                }
//            }
//        }
//    }
//}
//
