package net.novagamestudios.tools.composables

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.vector.ImageVector

@Composable
fun RotatingIcon(
        rotation: Float,
        icon: ImageVector,
        contentDescription: String?,
        modifier: Modifier = Modifier
) {
    val animated by animateFloatAsState(rotation)
    Icon(icon, contentDescription, modifier = modifier.rotate(animated))
}

@Composable
fun ExpandIcon(
        expanded: Boolean,
        contentDescription: String?,
        modifier: Modifier = Modifier
) = RotatingIcon(
        rotation = if (expanded) 180f else 0f,
        icon = Icons.Filled.ExpandMore,
        contentDescription = contentDescription,
        modifier = modifier
)