package net.novagamestudios.tools.collections
/*
import net.novagamestudios.tools.ifNull
import net.novagamestudios.tools.whenChanged

class MutableTreeMapOld<K : Comparable<K>, V> : SortedMap<K, V> {

    private var root: MutableEntry<K, V>? = null



    private fun MutableEntry<K, V>?.insert(key: K, value: V): V? {
        if (this == null) {
            MutableEntry(key, value, root).also { root = it }.apply {
                isRed = false
            }
            return null
        }

        val comparison = key.compareTo(this.key)
        if (comparison == 0) return setValue(value)
        var child = if (comparison < 0) left else right
        if (child != null) return child.insert(key, value)

        child = MutableEntry(key, value, this)

        if (comparison < 0) left = child else right = child

        child.fixRedViolation()

        return null
    }

    private fun MutableEntry<K, V>.delete() {
        val parent = parent
        val isLeftChild = isLeftChild
        if (left != null && right != null) {
            val successor = findSuccessor()!!
            successor.delete()
            swapWith(successor)
            successor.isRed = isRed
        } else {

        }


    }






    private class MutableEntry<K : Comparable<K>, V>(
            override val key: K,
            value: V,
            parent: MutableEntry<K, V>?
    ) : MutableMapEntry<K, V> {
        override var value: V = value
            private set

        override fun setValue(newValue: V): V = value.also { value = newValue }


        // Red Black Stuff

        var isRed = true

        var parent: MutableEntry<K, V>? = parent
            private set(value) {
                field?.let { if (it.left == this) it.left = null else it.right = null }
                field = value
            }
        var left: MutableEntry<K, V>? = null
            set(value) {
                field?.parent = null
                value?.parent = this
                field = value
            }
        var right: MutableEntry<K, V>? = null
            set(value) {
                field?.parent = null
                value?.parent = this
                field = value
            }

        val grandparent get() = parent?.parent
        val uncle get() = grandparent?.let { if (it.left == parent) right else left }

        val isLeftChild get() = parent?.let { it.left == this }


        fun fixRedViolation() {
            if (!isRed) return
            val parent = parent
            if (parent == null) {
                isRed = false
                return
            }
            if (!parent.isRed) return

            val grandparent = grandparent!!
            val uncle = uncle

            if (uncle?.isRed.ifNull { false }) {
                grandparent.isRed = grandparent.parent != null
                parent.isRed = false
                uncle?.isRed = false
                return
            }

            if (parent.isLeftChild!!) {

                if (!isLeftChild!!) {
                    parent.leftRotate()
                    parent.fixRedViolation()
                    return
                }
                grandparent.rightRotate()

                grandparent.isRed = true
                parent.isRed = false
            } else {

                if (isLeftChild!!) {
                    parent.rightRotate()
                    parent.fixRedViolation()
                    return
                }
                grandparent.leftRotate()

                grandparent.isRed = true
                parent.isRed = false
            }
        }


        fun leftRotate() {
            val parent = parent
            val isLeftInParent = isLeftChild
            val right = right!!
            val child = right.left

            this.right = child
            right.left = this
            when (isLeftInParent) {
                true -> parent?.left = right
                false -> parent?.right = right
            }
        }

        fun rightRotate() {
            val parent = parent
            val isLeftInParent = isLeftChild
            val left = left!!
            val child = left.right

            this.left = child
            left.right = this
            when (isLeftInParent) {
                true -> parent?.left = left
                false -> parent?.right = left
            }
        }


        fun findSuccessor(): MutableEntry<K, V>? {
            var current = right
            while (current != null) current = left.also { if (it == null) return current }
            return null
        }

        fun swapWith(other: MutableEntry<K, V>) {
            val parent = parent
            val isLeftInParent = isLeftChild
            val left = left
            val right = right

            val otherParent = other.parent
            val otherIsLeftInParent = other.isLeftChild
            val otherLeft = other.left
            val otherRight = other.right

            if (otherParent != null) {
                if (otherIsLeftInParent!!) otherParent.left = this else otherParent.right = this
            }
            this.left = otherLeft
            this.right = otherRight

            if (parent != null) {
                if (isLeftInParent!!) parent.left = other else parent.right = other
            }
            other.left = left
            other.right = right
        }

    }




}

*/