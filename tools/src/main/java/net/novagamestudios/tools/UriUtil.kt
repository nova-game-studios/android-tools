package net.novagamestudios.tools

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import androidx.annotation.RequiresApi
import androidx.core.net.toUri
import androidx.documentfile.provider.DocumentFile
import java.io.File
import java.net.URI
import java.net.URL
import java.net.URLEncoder
import java.util.*


const val TAG = "UriUtil"

@SuppressLint("ObsoleteSdkInt")
fun Uri.realPath(context: Context?): String? = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> realPathAboveKitKat(context)
    context != null -> realPathBelowKitKat(context.contentResolver, null)
    else -> null
}


@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun Uri.realPathAboveKitKat(context: Context?): String? = when {
    !isContentUri()                                                                              -> path
    authority?.let { AuthorityType.from(it) == AuthorityType.GOOGLE_PHOTOS }.letIfNull { false } -> lastPathSegment
    context == null                                                                              -> realPathFromDocumentOrTreeUri(context)
    else -> {
        val path = realPathFromDocumentOrTreeUri(context)
        when {
            path != null -> path
            else -> realPathBelowKitKat(context.contentResolver, null)
        }
    }
}

@Suppress("DEPRECATION")
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
fun Uri.realPathFromDocumentOrTreeUri(context: Context?): String? {
    val authority = authority ?: return null
    val docId = when {
        isTreeDocumentUri() -> treeDocumentId()
        isDocumentUri(context) -> documentId()
        else -> null
    }
    val idArr = docId?.split(":")?.toTypedArray() ?: return null
    return when (AuthorityType.from(authority)) {
        AuthorityType.MEDIA_DOCUMENTS            -> {
            when {
                context == null -> null
                idArr.size == 2 -> when {
                    "video" == idArr[0] -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" == idArr[0] -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    else                -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                }.realPathBelowKitKat(context.contentResolver, "${MediaStore.Images.Media._ID} = ${idArr[1]}")
                else            -> null
            }
        }
        AuthorityType.DOWNLOAD_DOCUMENTS         -> {
            when (context) {
                null -> null
                else -> ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), docId.toLong())
                        .realPathBelowKitKat(context.contentResolver, null)
            }
        }
        AuthorityType.EXTERNAL_STORAGE_DOCUMENTS -> {
            when {
                idArr[0].equals("primary", ignoreCase = true) -> when {
                    idArr.size > 1 -> "${Environment.getExternalStorageDirectory()}/${idArr[1]}/"
                    else           -> "${Environment.getExternalStorageDirectory()}/"
                }
                else                                          -> "storage/${docId.replace(":", "/")}"
            }
        }
        else                                     -> null
    }
}

@Suppress("DEPRECATION")
fun Uri.realPathBelowKitKat(contentResolver: ContentResolver, whereClause: String?): String? = contentResolver
        .query(this, null, whereClause, null, null)
        .use { cursor ->
            when {
                cursor == null || !cursor.moveToFirst() -> null
                else -> cursor.getString(cursor.getColumnIndex(when {
                                                                   MediaStore.Audio.Media.EXTERNAL_CONTENT_URI == this -> MediaStore.Audio.Media.DATA
                                                                   MediaStore.Video.Media.EXTERNAL_CONTENT_URI == this -> MediaStore.Video.Media.DATA
                                                                   else                                                -> MediaStore.Images.Media.DATA
                                                               }))
            }
        }


/** Check whether this uri represent a document or not.  */
fun Uri.isDocumentUri(context: Context?): Boolean = when {
    context != null -> DocumentsContract.isDocumentUri(context, this)
    !isContentUri() -> false
    else -> {
        pathSegments.let {
            when (it.size) {
                2 -> "document" == it[0]
                4 -> "tree" == it[0] && "document" == it[2]
                else -> false
            }
        }
    }
}

fun isDocumentsProvider(context: Context, authority: String): Boolean =
        context.packageManager.queryIntentContentProviders(Intent(DocumentsContract.PROVIDER_INTERFACE), 0)
                .any { authority == it.providerInfo.authority }

/**
 * Test if the given URI represents a [DocumentsContract.Document] tree.
 */
fun Uri.isTreeDocumentUri(): Boolean = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> DocumentsContract.isTreeUri(this)
    else -> pathSegments.let { it.size >= 2 && "tree" == it[0] }
}

/**
 * Check whether this uri is a content uri or not.
 * content uri like content://media/external/images/media/1302716
 */
fun Uri.isContentUri(): Boolean =
        scheme.equals(ContentResolver.SCHEME_CONTENT, ignoreCase = true)

/**
 * Check whether this uri is a file uri or not.
 * file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
 */
fun Uri.isFileUri(): Boolean = scheme.equals("file", ignoreCase = true)


enum class AuthorityType {
    UNDEFINED,
    /** Document provided by ExternalStorageProvider */
    EXTERNAL_STORAGE_DOCUMENTS,
    /** Document provided by DownloadsProvider */
    DOWNLOAD_DOCUMENTS,
    /** Document provided by MediaProvider */
    MEDIA_DOCUMENTS,
    /** Document provided by google photos */
    GOOGLE_PHOTOS;

    companion object {
        fun from(uriAuthority: String): AuthorityType = when (uriAuthority) {
            "com.android.externalstorage.documents" -> EXTERNAL_STORAGE_DOCUMENTS
            "com.android.providers.downloads.documents" -> DOWNLOAD_DOCUMENTS
            "com.android.providers.media.documents" -> MEDIA_DOCUMENTS
            "com.google.android.apps.photos.content" -> GOOGLE_PHOTOS
            else -> UNDEFINED
        }
    }
}


/**
 * Extract the [DocumentsContract.Document.COLUMN_DOCUMENT_ID] from the given URI.
 */
fun Uri.documentId(): String? {
    return try { DocumentsContract.getDocumentId(this) }
    catch (e: Throwable) {
        Log.e(TAG, "documentId $this", e)
        null
    }
}

/**
 * Extract the via [DocumentsContract.Document.COLUMN_DOCUMENT_ID] from the given URI.
 */
fun Uri.treeDocumentId(): String? {
    return try { DocumentsContract.getTreeDocumentId(this) }
    catch (e: Throwable) {
        Log.e(TAG, "treeDocumentId $this", e)
        null
    }
}


fun Uri.toBreadcrumbs(context: Context?, groupWhenNotWritable: Boolean = true): List<String> {
    val list: MutableList<String> = LinkedList()
    var file: File? = File(realPath(context) ?: return list)
    while (file != null) {
        if (!groupWhenNotWritable || file.canWrite()) list.add(0, file.name)
        else list[0] = file.name + list.getOrNull(0)?.let { File.separator + it }.orEmpty()
        file = file.parentFile
    }
    return list
}

/**
 * @throws RuntimeException
 */
@Suppress("DEPRECATION")
fun File.guessPrimaryTreeUri(): Uri? {
    return """^/?(?>storage/emulated/0/)?(.*?)/?$""".toRegex()
            .matchEntire(path)
            ?.groupValues
            ?.getOrNull(0)
            ?.let { "content://com.android.externalstorage.documents/tree/${URLEncoder.encode("primary:$it")}".toUri() }
}


fun Intent.trySetInitialTreeUri(context: Context, uri: Uri): Boolean = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> try {
        putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri.toTreeDocumentFile(context)!!.uri)
        true
    } catch (e: Throwable) {
        false
    }
    else -> false
}



//fun String.toUri(): Uri = Uri.parse(this)
fun String.toURI(): URI = URI(this)
fun String.toURL(): URL = URL(this)

fun Uri.toURI(): URI = toString().toURI()
fun Uri.toURL(): URL = toString().toURL()

fun URI.toUri(): Uri = toString().toUri()
//fun URI.toURL(): URL = toString().toURL()

fun URL.toUri(): Uri = toString().toUri()
//fun URL.toURI(): URI = toString().toURI()


fun Uri.toFile(): File = File(toString())
fun Uri.toTreeDocumentFile(context: Context): DocumentFile? = DocumentFile.fromTreeUri(context, this)
fun Uri.toSingleDocumentFile(context: Context): DocumentFile? = DocumentFile.fromSingleUri(context, this)
fun Uri.toDocumentFile(context: Context): DocumentFile? =
        if (isTreeDocumentUri()) toTreeDocumentFile(context)
        else toSingleDocumentFile(context)


//fun File.toUri(): Uri = toURI().toUri() // already exists

//fun File.toDocumentFile(): DocumentFile = DocumentFile.fromFile(this) // not compatible
//fun DocumentFile.toFile(): File = uri.toFile()                        // not compatible


fun Uri.mimeType(contentResolver: ContentResolver?): String? =
        if (isContentUri()) contentResolver?.getType(this)
        else path?.let { File(it) }
                ?.toURI()
                ?.toString()
                ?.let { MimeTypeMap.getFileExtensionFromUrl(it) }
                ?.let { MimeTypeMap.getSingleton().getMimeTypeFromExtension(it) }

fun Uri.extension(contentResolver: ContentResolver?): String? =
        mimeType(contentResolver)?.let { MimeTypeMap.getSingleton().getExtensionFromMimeType(it) }


fun String.splitNameAndExtension(): Pair<String, String?> =
        when (val dot = indexOfLast { it == '.' }) {
            -1 -> "" with null
            0 -> this with null
            else -> substring(0 until dot) with substring((dot + 1) until length)
        }







// region FILE OPERATIONS

//@Throws(IOException::class)
//fun Uri.copyFileTo(context: Context, destFile: Uri) {
//    context.contentResolver.openInputStream(this).use { inStream ->
//        context.contentResolver.openOutputStream(destFile).use { outStream ->
//            if (inStream == null) throw IOException("Could not open input stream from $this")
//            if (outStream == null) throw IOException("Could not open output stream to $destFile")
//
//            val buffer = ByteArray(1024)
//            var read: Int
//            while (inStream.read(buffer).also { read = it } != -1) {
//                outStream.write(buffer, 0, read)
//            }
//            outStream.flush()
//        }
//    }
//}

//@Throws(IOException::class)
//fun Uri.deleteFile(context: Context?) {
//    try {
//        if (!(isFileUri() && toFile().delete() || toSingleDocumentFile(context).delete()))
//            throw IOException("File not deleted")
//    } catch (e: Throwable) {
//        throw IOException("Could not delete $this", e)
//    }
//}

//fun Uri.renameFileTo(context: Context, displayName: String) =
//        toSingleDocumentFile(context).renameTo(displayName) // TODO handle file uri (see deleteFile)

//@Throws(IOException::class)
//fun Uri.moveFileTo(context: Context, destFile: Uri) {
//    copyFileTo(context, destFile)
//    deleteFile(context)
//}

//@Throws(IOException::class)
//fun DocumentFile.copyTo(context: Context, destFile: DocumentFile) =
//        uri.copyFileTo(context, destFile.uri)

//@Throws(IOException::class)
//fun DocumentFile.moveTo(context: Context, destFile: DocumentFile) =
//        uri.moveFileTo(context, destFile.uri)

// endregion



//fun File.createChild(name: String): File = File(this, name)







object UriConstants {
//    const val REGEX = "([a-z]([a-z]|\\d|\\+|-|\\.)*):(//(((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:)*@)?((\\[(|(v[\\da-f]{1,}\\.(([a-z]|\\d|-|\\.|_|~)|[!$&'()*+,;=]|:)+))])|((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|(([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=])*)(:\\d*)?)(/(([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)*)*|(/((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)+(/(([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)*)*)?)|((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)+(/(([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)*)*)|((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)){0})(\\?((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)|[\\xE000-\\xF8FF]|/|\\?)*)?(#((([a-z]|\\d|-|\\.|_|~|[\\x00A0-\\xD7FF\\xF900-\\xFDCF\\xFDF0-\\xFFEF])|(%[\\da-f]{2})|[!$&'()*+,;=]|:|@)|/|\\?)*)?"

    const val REGEX = "([a-zA-Z][a-zA-Z0-9+.-]*):(?://((?:(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:]|%[0-9A-F]{2})*))(\\3)@)?(?=(\\[[0-9A-F:.]{2,}\\]|(?:[a-zA-Z0-9-._~!$&'()*+,;=]|%[0-9A-F]{2})*))\\5(?::(?=(\\d*))\\6)?)(/(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/]|%[0-9A-F]{2})*))\\8)?|(/?(?!/)(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/]|%[0-9A-F]{2})*))\\10)?)(?:\\?(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/?]|%[0-9A-F]{2})*))\\11)?(?:#(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/?]|%[0-9A-F]{2})*))\\12)?"
//    const val REGEX_SERVER_AUTHORITY = "(?:(?=((?:[a-z0-9-._~!$&'()*+,;=:]|%[0-9A-F]{2})*))(\\1)@)?(?=(\\[[0-9A-F:.]{2,}\\]|(?:[a-z0-9-._~!$&'()*+,;=]|%[0-9A-F]{2})*))\\3(?::(?=(\\d*))\\4)?"
//    const val REGEX_YOUTUBE = "^(https?://)?(www\\.)?(youtube\\.[a-z]{2,3}|youtu\\.be)/.+$"

    object Schemes {
        const val HTTP   = "http"
        const val HTTPS  = "https"
        const val FTP    = "ftp"
        const val MAILTO = "mailto"
        const val FILE   = "file"
        const val NEWS   = "news"
        const val NNTP   = "nntp"
        const val TELNET = "telnet"
        const val WAIS   = "wais"
        const val GOPHER = "gopher"
    }
}

//data class ServerAuthority(
//        val userInfo: String?,
//        val domainLabels: List<String>,
//        val topLabel: String,
//        val port: Int?
//) {
//    val host: String by lazy { listOf(topLabel, *domainLabels.toTypedArray()).asReversed().joinToString(".") }
//}

//@Suppress("FunctionName")
//fun ServerAuthority(str: String): ServerAuthority? {
//    val match = REGEX_SERVER_AUTHORITY.toRegex().matchEntire(str) ?: return null
//    val labels = match.groupValues.getOrNull(3)?.split(".")?.asReversed() ?: return null
//    return ServerAuthority(
//            match.groupValues.getOrNull(2),
//            labels.slice(1 until labels.size),
//            labels[0],
//            match.groupValues.getOrNull(4)?.toInt())
//}

//fun Uri.serverAuthority(): ServerAuthority? = authority?.let { ServerAuthority(it) }



fun String.findUris(): List<Uri> = UriConstants.REGEX.toRegex()
        .findAll(this)
        .map { it.value.toUri() }
        .toList()



fun Uri.isWebPageUrl(): Boolean =
        scheme in setOf(UriConstants.Schemes.HTTP, UriConstants.Schemes.HTTPS)
                && authority != null
                && path != null
                && tryCatchAll { toURL() } != null


fun Uri.isYoutubeUrl(): Boolean {
    if (!isWebPageUrl()) return false
    val url = toURL()
    if (url.host.endsWith("youtu.be", ignoreCase = true)) return true
    if (url.host.split(".").asReversed().getOrNull(1).equals("youtube", ignoreCase = true)) return true
    return false
//    return isWebPageUrl() && serverAuthority()?.let {
//        it.topLabel.equals("be", true) && it.domainLabels.getOrNull(0).equals("youtu", true)
//    }.ifNull { false }
}

fun toYoutubeUrl(idOrUrl: String): Uri? =
        tryCatchAll { idOrUrl.toUri().takeIf { it.isYoutubeUrl() } }
            ?: if (idOrUrl.matches("\\S+".toRegex())) "http://www.youtube.com/watch?v=$idOrUrl".toUri()
               else null


