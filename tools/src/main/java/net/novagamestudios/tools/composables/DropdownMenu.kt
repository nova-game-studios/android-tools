package net.novagamestudios.tools.composables

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.material.DropdownMenu
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier


@Composable
fun <K> DropdownMenu(
        items: List<Pair<K, @Composable BoxScope.(onClick: () -> Unit) -> Unit>>,
        onSelect: (K) -> Unit,
        modifier: Modifier = Modifier,
        menuModifier: Modifier = Modifier,
        current: @Composable BoxScope.(expanded: Boolean, toggle: () -> Unit) -> Unit
) {
    var expanded by rememberMutableState { false }

    Box(modifier = modifier) {
        current(expanded) { expanded = !expanded }
        DropdownMenu(expanded = expanded,
                     onDismissRequest = { expanded = false },
                     modifier = menuModifier
        ) {
            items.forEach { (item, content) ->
                Box {
                    content {
                        onSelect(item)
                        expanded = false
                    }
                }
            }
        }
    }
}