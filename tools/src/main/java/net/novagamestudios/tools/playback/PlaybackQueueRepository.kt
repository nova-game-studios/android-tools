package net.novagamestudios.tools.playback

import android.support.v4.media.RatingCompat
import androidx.annotation.CallSuper
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import net.novagamestudios.tools.Logger
import net.novagamestudios.tools.flatMapLatestState
import net.novagamestudios.tools.info
import net.novagamestudios.tools.onEachState
import kotlin.reflect.KProperty

@ExperimentalCoroutinesApi
abstract class PlaybackQueueRepository<Item : QueueItem>(
        scope: CoroutineScope
) : CoroutineScope by scope, PlaybackController, QueueController<Item>, Logger {

    protected val playbackController = LazilyProvidedValue({
        providePlaybackController()
    }) { onPlaybackControllerChanged(it) }
    protected val queueController = LazilyProvidedValue({
        provideQueueController()
    }) { onQueueControllerChanged(it) }


    private val playbackControllerFlow = MutableStateFlow<PlaybackController?>(null)
    private val queueControllerFlow = MutableStateFlow<QueueController<Item>?>(null)


    override val playbackState: StateFlow<PlaybackState?> = playbackControllerFlow
            .onEachState(scope) { info { "new PC for PS: $it" } }
            .flatMapLatestState(scope) { it?.playbackState ?: MutableStateFlow(null) }
            .onEachState(scope) { info { "new PS: $it" } }

    override val queueState: StateFlow<QueueState<Item>?> = queueControllerFlow
            .onEachState(scope) { info { "new QC for QS: $it" } }
            .flatMapLatestState(scope) { it?.queueState ?: MutableStateFlow(null) }
            .onEachState(scope) { info { "new QS: $it" } }


    protected abstract suspend fun providePlaybackController(): PlaybackController
    protected abstract suspend fun provideQueueController(): QueueController<Item>

    @CallSuper
    protected open suspend fun onPlaybackControllerChanged(controller: PlaybackController) {
        playbackControllerFlow.value = controller
    }
    @CallSuper
    protected open suspend fun onQueueControllerChanged(controller: QueueController<Item>) {
        queueControllerFlow.value = controller
    }


    private suspend inline fun rpc(crossinline block: suspend PlaybackController.() -> Unit) {
        withContext(Dispatchers.Main) {
            playbackController.value().block()
        }
    }
    private suspend inline fun rqc(crossinline block: suspend QueueController<Item>.() -> Unit) {
        withContext(Dispatchers.Main) {
            queueController.value().block()
        }
    }


    suspend fun loadPlayback() {
        playbackController.value()
    }
    suspend fun loadQueue() {
        queueController.value()
    }



    override suspend fun play() { rpc { play() } }
    override suspend fun pause() { rpc { pause() } }
    override suspend fun togglePlay() { rpc { togglePlay() } }
    override suspend fun stop() { rpc { stop() } }
    override suspend fun seekTo(ms: Long) { rpc { seekTo(ms) } }
    override suspend fun fastForward() { rpc { fastForward() } }
    override suspend fun rewind() { rpc { rewind() } }
    override suspend fun setShuffleMode(mode: ShuffleMode) { rpc { setShuffleMode(mode) } }
    override suspend fun setRepeatMode(mode: RepeatMode) { rpc { setRepeatMode(mode) } }
    override suspend fun setPlaybackParams(params: PlaybackParams) { rpc { setPlaybackParams(params) } }
    override suspend fun setCaptionEnabled(enabled: Boolean) { rpc { setCaptionEnabled(enabled) } }

    override suspend fun skipToIndex(index: Int) { rqc { skipToIndex(index) } }
    override suspend fun skipToNext() { rqc { skipToNext() } }
    override suspend fun skipToPrev() { rqc { skipToPrev() } }
    override suspend fun skipToItem(item: Item) { rqc { skipToItem(item) } }
    override suspend fun set(item: Item) { rqc { set(item) } }
    override suspend fun setAll(items: Collection<Item>) { rqc { setAll(items) } }
    override suspend fun add(item: Item) { rqc { add(item) } }
    override suspend fun addAll(items: Collection<Item>) { rqc { addAll(items) } }
    override suspend fun insert(index: Int, item: Item) { rqc { insert(index, item) } }
    override suspend fun insertAfter(after: Item, item: Item) { rqc { insertAfter(after, item) } }
    override suspend fun insertAll(index: Int, items: Collection<Item>) { rqc { insertAll(index, items) } }
    override suspend fun insertAllAfter(item: Item, items: Collection<Item>) { rqc { insertAllAfter(item, items) } }
    override suspend fun move(fromIndex: Int, toIndex: Int) { rqc { move(fromIndex, toIndex) } }
    override suspend fun remove(index: Int) { rqc { remove(index) } }
    override suspend fun remove(item: Item) { rqc { remove(item) } }
    override suspend fun clear() { rqc { clear() } }
    override suspend fun setRating(rating: RatingCompat) { rqc { setRating(rating) } }


}


//private typealias Operation = suspend () -> Unit





class LazilyProvidedValue<V : Any>(
        private val provider: suspend () -> V,
        private val onNewProvidedValue: suspend (V) -> Unit
) {
    private val mutex = Mutex()

    var valueOrNull: V? = null
        private set

    suspend fun value(): V = valueOrNull ?: mutex.withLock {
        valueOrNull ?: provider()
                .also { valueOrNull = it }
                .also { onNewProvidedValue(it) }
    }

    operator fun getValue(thisRef: Any, property: KProperty<*>): V? = valueOrNull

    fun clear() {
        valueOrNull = null
    }



    suspend fun <R> requiredFor(block: (V) -> R): R = block(value())

}




