package net.novagamestudios.tools

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CompletableDeferred



@Deprecated("See androidx.activity.result.contract.ActivityResultContract<I, O>")
class DeferrableActivityResult private constructor(
        private val activity: Activity?,
        private val fragment: Fragment?
) {

    constructor(activity: Activity) : this(activity, null)
    constructor(fragment: Fragment) : this(null, fragment)
    
    val context: Context = activity ?: fragment!!.requireContext()
    
    private var currentCode : Int = 0
    private val results = mutableMapOf<Int, CompletableDeferred<ActivityResult?>>()

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean =
            results.remove(requestCode)?.also { it.complete(ActivityResult(resultCode, data)) } != null

    suspend fun startActivity(intent: Intent, options: Bundle? = null): ActivityResult? = CompletableDeferred<ActivityResult?>()
            .also {
                try {
                    val requestCode = currentCode++
                    results[requestCode] = it
                    if (activity != null) activity.startActivityForResult(intent, requestCode, options)
                    else fragment!!.startActivityForResult(intent, requestCode, options)
                } catch (e: ActivityNotFoundException) {
                    it.complete(null)
                }
            }.await()
    
    

    data class ActivityResult(
            val resultCode: Int,
            val data: Intent?)
}



