package net.novagamestudios.tools

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock


private val atomicIntCounter = AtomicIntCounter(1)
val Int.Companion.unique get() = runBlocking { atomicIntCounter.get() }
fun Int.Companion.uniqueSequence() = sequence { while (true) yield(Int.unique) }


private val atomicLongCounter = AtomicLongCounter(1)
val Long.Companion.unique get() = runBlocking { atomicLongCounter.get() }
fun Long.Companion.uniqueSequence() = sequence { while (true) yield(Long.unique) }



class AtomicIntCounter(initial: Int) {
    private val mutex = Mutex()
    private var value = initial
    suspend fun get() = mutex.withLock { value++ }
}

class AtomicLongCounter(initial: Long) {
    private val mutex = Mutex()
    private var value = initial
    suspend fun get() = mutex.withLock { value++ }
}














