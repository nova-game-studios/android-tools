package net.novagamestudios.tools.composables

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.primarySurface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.viewinterop.AndroidView
import com.yuyh.jsonviewer.library.JsonRecyclerView
import kotlinx.serialization.json.JsonElement

@Composable
fun JsonViewer(
        json: JsonElement,
        modifier: Modifier = Modifier
) = JsonViewer(json.toString(), modifier)

@Composable
fun JsonViewer(
        json: String,
        modifier: Modifier = Modifier
) {
    Box(modifier
                .fillMaxSize()
                .horizontalScroll(rememberScrollState())
    ) {
        AndroidView(
                factory = { JsonRecyclerView(it) },
                update = { it.bindJson(json) }
        )
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun JsonViewerDialog(
        json: JsonElement,
        onClose: () -> Unit,
        title: @Composable () -> Unit = { Text("Json") },
        topBarColor: Color = MaterialTheme.colors.primarySurface
) = JsonViewerDialog(json.toString(), onClose, title, topBarColor)

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun JsonViewerDialog(
        json: String,
        onClose: () -> Unit,
        title: @Composable () -> Unit = { Text("Json") },
        topBarColor: Color = MaterialTheme.colors.primarySurface
) = FullScreenDialog(
    onClose = onClose,
    title = title,
    topAppBarColor = topBarColor
) {
    JsonViewer(json)
}



