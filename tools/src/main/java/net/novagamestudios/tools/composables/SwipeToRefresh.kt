package net.novagamestudios.tools.composables

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.SwipeableDefaults.resistanceConfig
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import net.novagamestudios.tools.clamp
import net.novagamestudios.tools.ifNull
import net.novagamestudios.tools.map
import net.novagamestudios.tools.composables.SwipeToRefreshDefaults.IndicatorDefaults.Clipped
import net.novagamestudios.tools.composables.SwipeToRefreshDefaults.IndicatorDefaults.Growing
import net.novagamestudios.tools.composables.SwipeToRefreshDefaults.RefreshDistance
import net.novagamestudios.tools.composables.SwipeToRefreshDefaults.SwipeResistance
import net.novagamestudios.tools.composables.SwipeToRefreshState.*
import kotlin.math.roundToInt


@Preview
@ExperimentalMaterialApi
@Composable
fun SwipeToRefreshLayoutPreview() {
    var refreshing by rememberMutableState { false }
    SwipeToRefreshLayout(refreshing,
                         onRefresh = { refreshing = true }
    ) {
        val nestedScrollState = rememberScrollState(0)
        Column(modifier = Modifier
                .width(400.dp)
                .height(300.dp)
                .verticalScroll(nestedScrollState)) {
            Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .background(Color.Blue))
            Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .clickable { refreshing = !refreshing }
                    .background(if (refreshing) Color.Cyan else Color.Red)) {
                Text("Click to toggle", Modifier
                        .align(Alignment.TopCenter)
                        .padding(16.dp))
            }
        }
    }
}


object SwipeToRefreshDefaults {
    val RefreshDistance = 40.dp
    val SwipeResistance = 4f

    object IndicatorDefaults {
        @Composable
        private fun BoxScope.Base(progressToRefresh: Float?, modifier: Modifier = Modifier) {
            val alpha by animateFloatAsState(
                    if (progressToRefresh != null && progressToRefresh < 1f) 0.5f else 1f
            )
            Surface(elevation = 10.dp, shape = CircleShape, modifier = modifier) {
                Box(modifier = Modifier
                        .size(40.dp)
                        .padding(10.dp)
                        .alpha(alpha)
                ) {

                    // TODO instead CircularProgressIndicatorWithArrow
                    if (progressToRefresh != null) CircularProgressIndicator(
                            progressToRefresh.clamp(0f, 1f),
                            modifier = Modifier.rotate(360 * progressToRefresh / 3)
                    ) else CircularProgressIndicator()
                }
            }
        }

        @Composable
        fun BoxScope.Clipped(progressToRefresh: Float?) =
                Base(progressToRefresh)
        @Composable
        fun BoxScope.Growing(progressToRefresh: Float?) =
                Base(progressToRefresh, Modifier.scale(progressToRefresh.ifNull { 1f }.clamp(0f, 1f)))
    }
}

@ExperimentalMaterialApi
@Composable
fun SwipeToRefreshLayout(
        refreshing: Boolean,
        onRefresh: () -> Unit,
        containedIndicator: Boolean = false,
        indicator: @Composable BoxScope.(progressToRefresh: Float?) -> Unit = {
            if (containedIndicator) Growing(it) else Clipped(it)
        },
        content: @Composable () -> Unit
) {
    val hiddenDistance = with(LocalDensity.current) {
        if (containedIndicator) 0.dp.toPx() else -RefreshDistance.toPx()
    }
    val refreshDistance = with(LocalDensity.current) { RefreshDistance.toPx() }

    var state by rememberMutableStateWithPrevious(refreshing, { Idle }) {
        when {
            it == Refresh && !refreshing -> DoneAnimation
            refreshing                   -> Refresh
            else                         -> Idle
        }
    }
    val swipeableState = rememberSwipeableState(refreshing) { new ->
        // is this a workaround?
        if (new && state == Idle) onRefresh() // user triggered refresh
        true
    }
    val anchors = mapOf(hiddenDistance to false, refreshDistance to true)
    val scale = remember { Animatable(1f) }

    Box(modifier = Modifier
            .nestedScroll(
                    if (state == Idle) swipeableState.PreUpPostDownNestedScrollConnection
                    else NoNestedScrollConnection
            )
            .swipeable(state = swipeableState,
                       anchors = anchors,
                       thresholds = { _, _ -> FractionalThreshold(1f) },
                    // TODO overscroll doesn't seem to be implemented yet
                    //  see (https://stackoverflow.com/a/65617078/8489294)
                       resistance = resistanceConfig(anchors.keys, SwipeResistance, SwipeResistance),
                       orientation = Orientation.Vertical)
    ) {
        val offset = swipeableState.offset.value
        content()
        Box(modifier = Modifier
                .align(Alignment.TopCenter)
                .offset { IntOffset(0, offset.roundToInt()) }
                .scale(scale.value)
        ) {
            if (offset > hiddenDistance) {
                indicator(if (refreshing) null else offset.map(
                        hiddenDistance, refreshDistance,
                        0f, 1f
                ))
            }
        }
    }

    LaunchedEffect(state) {
        when (state) {
            Idle          -> { // after DoneAnimation
                swipeableState.snapTo(false)
                scale.snapTo(1f)
            }
            Refresh       -> {
                scale.snapTo(1f)
                swipeableState.animateTo(true)
            }
            DoneAnimation -> {
                swipeableState.snapTo(true)
                scale.animateTo(0f)
                state = Idle
            }
        }
    }

    LaunchedEffect(swipeableState.currentValue) {
        if (swipeableState.currentValue && state == Idle) swipeableState.animateTo(false)
    }
}


private enum class SwipeToRefreshState {
    Idle,
    Refresh,
    DoneAnimation
}

private val NoNestedScrollConnection: NestedScrollConnection
    get() = object : NestedScrollConnection {
        override fun onPreScroll(
                available: Offset,
                source: NestedScrollSource
        ): Offset = Offset.Zero
        override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
        ): Offset = Offset.Zero
        override suspend fun onPreFling(
                available: Velocity
        ): Velocity = Velocity.Zero
        override suspend fun onPostFling(
                consumed: Velocity,
                available: Velocity
        ): Velocity = Velocity.Zero
    }


/**
 * Temporary workaround for nested scrolling behavior. There is no default implementation for
 * pull to refresh yet, this nested scroll connection mimics the behavior.
 */
@ExperimentalMaterialApi
private val <T> SwipeableState<T>.PreUpPostDownNestedScrollConnection: NestedScrollConnection
    get() = object : NestedScrollConnection {
        override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
            val delta = available.toFloat()
            return when {
                source == NestedScrollSource.Drag -> performDrag(delta).toOffset() // delta < 0 &&
                else                                           -> Offset.Zero
            }
        }

        override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
        ): Offset = when (source) {
            NestedScrollSource.Drag -> performDrag(available.toFloat()).toOffset()
            else                    -> Offset.Zero
        }

        override suspend fun onPreFling(available: Velocity): Velocity {
            val toFling = Offset(available.x, available.y).toFloat()
            return if (toFling < 0) {
                performFling(velocity = toFling)
                // since we go to the anchor with tween settling, consume all for the best UX
                available
            } else Velocity.Zero
        }

        override suspend fun onPostFling(
                consumed: Velocity,
                available: Velocity
        ): Velocity {
            performFling(velocity = Offset(available.x, available.y).toFloat())
            return Velocity.Zero
        }

        private fun Float.toOffset(): Offset = Offset(0f, this)

        private fun Offset.toFloat(): Float = this.y
    }